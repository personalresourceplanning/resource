/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestOperations;

@SpringBootApplication
public class ResourceApplication {

	public static void main(final String[] args) {
		SpringApplication.run(ResourceApplication.class, args);
	}

	@Controller
	public class TestController {

		private RestOperations restTemplate;
		private OAuth2Authentication auth;

		@Autowired
		public TestController(final RestOperations restTemplate, final OAuth2Authentication auth) {
			this.restTemplate = restTemplate;
			this.auth = auth;
		}

		@GetMapping("/test")
		@ResponseBody
		public String test() {
			return "WOHAAA baby it's working. Hello " + auth.getName();
		}

	}

}
