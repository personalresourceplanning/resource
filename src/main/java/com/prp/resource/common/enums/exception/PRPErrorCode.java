/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.common.enums.exception;

import com.prp.resource.common.exception.PRPException;

import lombok.Getter;

/**
 * Error code registry for the PRP Resource Service. In this enum, every case
 * used in this Service is documented.
 * <p>
 * The purpose of these codes is to hand the reason for an {@link PRPException}
 * to the client or the calling service. This way, it is possible to create
 * understandable error messages.
 * <p>
 * <strong>Important to know:</strong> Not every error case explained here has
 * to be handed to the client as it is. Most of them, specially DAL errors,
 * indicate internal bugs. In this case, some general information should be
 * displayed to the user with the specific error code. This way, it is possible
 * to track bugs.
 *
 * @see PRPException
 * @author Eric Fischer
 * @since 0.1
 *
 */
public enum PRPErrorCode {

	/******************
	 * General errors *
	 *****************/

	/**
	 * This should be the default case in {@link LiaException}. If this error is
	 * handed to the client, something went completely wrong and should be treated
	 * as a bug.
	 *
	 * @since 0.1
	 */
	UNKNOWN("0"),

	/************************
	 * Email Service Errors *
	 ***********************/

	/**
	 * Indicates, that an email wasn't send because of some transport errors.
	 * <p>
	 * This doesn't indicate bugs directly. Possible reasons for this error are:
	 * <ul>
	 * <li>wrongly configured email account</li>
	 * <li>connection to SMTP server</li>
	 * </ul>
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_SERVICE_MAIL_NOT_SENDABLE("B_E_S_MNS"),
	/**
	 * Thrown if an email account can't be registered because of missing
	 * information. Another case is that not all credentials are given to fetch
	 * E-Mails.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_BEAN_ACCOUNT_CRIDENTIALS_NOT_COMPLETE("B_E_B_ACNC"),
	/**
	 * The email handed to the bean method is {@code null}.
	 * <p>
	 * If this error is thrown, there's probably a bug within the application.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_BEAN_NO_EMAIL_TO_SEND("B_E_B_NETS"),
	/**
	 * The account information for fetching emails isn't complete.
	 * <p>
	 * Indicates a bug, these fields should be filled by the software.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_BEAN_ACCOUNT_INFORMATION_NOT_COMPLETE("B_E_B_AINC"),
	/**
	 * The default fetching method stored within the account isn't supported yet.
	 * <p>
	 * This means, that somewhere in the system some functionality isn't implemented
	 * or a sending protocol is used for receiving. So it indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_BEAN_FETCHING_METHOD_NOT_SUPPORTED("B_E_B_FMNS"),
	/**
	 * The desired account was not found within the database.
	 * <p>
	 * In the most cases, this indicates a bug at the frontend.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_BEAN_ACCOUNT_NOT_FOUND("B_E_B_ANF"),
	/**
	 * Thrown, if the sender email address or name couldn't be set.
	 * <p>
	 * This is caused by invalid formats and could mean, that some checks are
	 * missing.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_SERVICE_SENDER_ADDRESS_AND_NAME_NOT_SETABLE("B_E_S_SAANNS"),
	/**
	 * The provider given with the account isn't reachable.
	 * <p>
	 * This could mean, that the account was not properly configured or the
	 * configuration has changed. Either URL is wrong or the username/password
	 * combination isn't valid. Also, the reading of the folders could have failed.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_SERVICE_PROVIDER_CONNECTION_ERROR("B_E_S_PCE"),
	/**
	 * An error occurred while fetching the folders or email messages.
	 * <p>
	 * This indicates some problems with the transport in common.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_SERVICE_PROVIDER_COMMUNICATION_ERROR("B_E_S_PCOME"),
	/**
	 * Thrown in two cases:
	 * <ul>
	 * <li>The email to store isn't given.</li>
	 * <li>The details of an email address to store are not given.</li>
	 * </ul>
	 * <p>
	 * Definitely indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_DAL_NO_EMAIL_TO_STORE("B_E_D_NETS"),
	/**
	 * Not all parameters are filled to search for a specific host or to store it to
	 * the database.
	 * <p>
	 * This indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_DAL_HOST_DETAILS_MISSING("B_E_D_HDM"),
	/**
	 * Search for host access in database could not be performed because of missing
	 * parameters. Could mean, that no host access information was handed to the
	 * DAL, too.
	 * <p>
	 * Another purpose of this error is to indicate a missing host access
	 * representation in database.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_DAL_INFORMATION_ABOUT_HOSTACCESS_MISSING("B_E_D_IAHM"),
	/**
	 * For one single host multiple access definitions were found. As there should
	 * be only one definition per account and host, this is a violation of the data
	 * model.
	 * <p>
	 * This is a bug. The reason for this error could reside somewhere else in the
	 * application.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_DAL_DATA_INTEGRITY_MULTIPLE_ACCESS_DEFINITIONS("B_E_D_DIMAD"),
	/**
	 * For the given host url multiple entities were found. In this case, the data
	 * model is wrongly configured.
	 * <p>
	 * Is definitely an internal bug.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_DAL_DATA_INTEGRITY_MULTIPLE_HOST_DEFINITIONS("B_E_D_DIMHD"),
	/**
	 * The account id is missing. In this case, the database search cannot be
	 * completed.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_DAL_ACCOUNT_ID_MISSING("B_E_D_AIM"),
	/**
	 * The needed information for searching for an account or for storing it isn't
	 * given.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_DAL_ACCOUNT_INFORMATION_NOT_GIVEN("B_E_D_AING"),
	/**
	 * The user id is missing within the account information.
	 * <p>
	 * As this should be handed to the DAL, this indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_DAL_USER_ID_MISSING("B_E_D_UIM"),
	/**
	 * Parameters missing to perform an operation.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_DAL_PARAMETERS_MISSING("B_E_D_PM"),
	/**
	 * The account couldn't be found for the given search parameters.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_DAL_ACCOUNT_NOT_FOUND("B_E_D_ANF"),
	/**
	 * Thrown, if an email account with the given name for the current user is
	 * already stored. The name has to be unique for the specific user.
	 * <p>
	 * This can be thrown, if the user entered a name that already exists. It
	 * doesn't indicate a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_DAL_ACCOUNT_NAME_ALREADY_TAKEN("B_E_D_ANAT"),
	/**
	 * Parameters missing to search for an email in DB.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_DAL_EMAIL_SEARCH_PARAMETER_MISSING("B_E_D_ESPM"),
	/**
	 * Account and host information were not found in the database.
	 * <p>
	 * This is typically a bug, in future applications this one could reside on
	 * missing information from the client.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_DAL_ACCOUNT_AND_HOST_NOT_FOUND("B_E_D_AAHNF"),
	/**
	 * The content of an email could not be destructed.
	 * <p>
	 * In this case, fetching emails will be aborted, because the user should be
	 * able to get all or nothing.
	 *
	 * @since 0.1
	 */

	BACKEND_EMAILSERVICE_BEAN_EMAIL_NOT_FOUND("B_E_B_ENF"),
	/**
	 * Thrown, if the no email was found for given id.
	 * <p>
	 * Indicated a bug, because email should not be visible in frontend.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_BEAN_EMAIL_ID_MISSING("B_E_B_EIM"),
	/**
	 * Thrown, if the no email id was provided.
	 * <p>
	 * Indicates a bug, this field should be filled by the frontend.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_BEAN_MAIL_CONTENT_NOT_READABLE("B_E_B_MCNR"),
	/**
	 * There was an error fetching the mail details from the provider.
	 * <p>
	 * In this case, the action should be retried.
	 *
	 * @since 0.1
	 */
	BACKEND_EMAILSERVICE_BEAN_PROVIDER_CONNECTION_ERROR("B_E_B_PCE"),

	/***************************
	 * Calendar Service Errors *
	 **************************/

	/**
	 * No event given to store, update or delete in DB.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 *
	 */
	BACKEND_CALENDARSERVICE_DAL_NO_EVENT_GIVEN("B_C_D_NEG"),
	/**
	 * The event handed to the DAL has no start or end date.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_DAL_START_AND_END_NOT_SET("B_C_D_SAENS"),
	/**
	 * The ID of the event is not given. In this case, the update or deletion cannot
	 * be performed, because the DB resource cannot be identified. Furthermore, no
	 * reminders could be added.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_DAL_NO_EVENT_ID_GIVEN("B_C_D_NEIG"),
	/**
	 * No time set for storing into database.
	 * <p>
	 * If thrown, it indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_DAL_NO_REMIND_TIME_GIVEN("B_C_D_NRTG"),
	/**
	 * No event could be found for the given ID.
	 * <p>
	 * Is not a bug, there's just no event for the ID. No further action will be
	 * performed if this error is thrown.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_DAL_NO_EVENT_FOR_ID_FOUND("B_C_D_NEFIDF"),
	/**
	 * No calendar given to store the event in.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_DAL_NO_CALENDAR_GIVEN("B_C_D_NCG"),
	/**
	 * The calendar, an event belongs to, could not be found.
	 * <p>
	 * Indicates a bug in most cases. Sometimes it could happen, that one person is
	 * trying to add an event to an calendar, which was deleted a few seconds ago.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_DAL_NO_CALENDAR_FOR_ID_FOUND("B_C_D_NCFIDF"),
	/**
	 * The calendar to store or update does not contain all necessary information.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_DAL_CALENDAR_DATA_NO_COMPLETE("B_C_D_CDNC"),
	/**
	 * The permission data is not complete.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_DAL_CALENDAR_PERMISSION_DATA_NO_COMPLETE("B_C_D_CPDNC"),
	/**
	 * The calendar or it's ID is missing for storing a calendar permission to the
	 * database.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_DAL_CALENDAR_ID_MISSING("B_C_D_CIDM"),
	/**
	 * No permission ID given to delete or update a calendar permission information.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_DAL_PERMISSION_ID_MISSING("B_C_D_PIDM"),
	/**
	 * The given user already has a permission on the given calendar. No further
	 * permission can be granted. To solve that, the existing Permission has to be
	 * updated.
	 * <p>
	 * This indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_DAL_USER_ALREADY_HAS_PERMISSION("B_C_D_UAHP"),
	/**
	 * Thrown if parameters for an event fetch are not complete.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_DAL_FETCH_DATA_NOT_COMPLETE("B_C_D_FDNC"),
	/**
	 * Parameters for actions on the database are missing.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_DAL_PARAMETERS_MISSING("B_C_D_PM"),
	/**
	 * The permission read from the database isn't convertible into the API
	 * representation. In this case, there's uninterpretable data within the
	 * database.
	 * <p>
	 * This does not definitely indicate a bug. Just make sure that your data is
	 * correct. However, this error should never be thrown.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_BEAN_PERMISSION_NOT_CONVERTABLE("B_C_B_PNC"),
	/**
	 * The user has not the needed permission to update or delete the given
	 * calendar.
	 * <p>
	 * This doesn't indicate a bug. However, the client should avoid this error.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_BEAN_USER_HAS_NO_PERMISSION("B_C_B_UNP"),
	/**
	 * No calendar or calendar ID given for deletion or update.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_BEAN_CALENDAR_NOT_GIVEN("B_C_B_CNG"),
	/**
	 * Parameters missing to perform called method.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_BEAN_PARAMETERS_MISSING("B_C_B_PM"),
	/**
	 * The end date is before the start date of an event.
	 * <p>
	 * This can happen if the user makes a wrong input. The client should avoid that
	 * by checking the data before sending to the server.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_BEAN_END_BEFORE_START("B_C_B_EBS"),
	/**
	 * The user, which permission should be updated, has no permission on the given
	 * calendar.
	 * <p>
	 * This can accidently happen, should be avoided by the calling client.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_BEAN_NO_PERMISSION_FOUND("B_C_B_NPF"),
	/**
	 * No permission has been found when updating a user's permission on a calendar.
	 * <p>
	 * This should be avoided by the client and is usually a client bug.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_BEAN_NO_PERMISSION_TO_UPDATE("B_C_B_NPTU"),
	/**
	 * No calendar found.
	 * <p>
	 * If the client calls update/delete methods while the calendar doesn't exist,
	 * the client has a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_BEAN_CALENDAR_NOT_FOUND("B_C_B_CNF"),
	/**
	 * No calendar found.
	 * <p>
	 * If the client calls update/delete methods while the event doesn't exist, the
	 * client has a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_CALENDARSERVICE_BEAN_NO_EVENT_FOR_ID_FOUND("B_C_B_NEFIF"),

	/***********************
	 * Task service Errors *
	 **********************/

	BACKEND_TASKSERVICE_DAL_PARAMETERS_MISSING("B_T_D_PM"),
	/**
	 * The task list to use isn't existent. This can happen while storing or
	 * updating a task or storing or update a task list.
	 * <p>
	 * It's a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_TASKSERVICE_DAL_TASK_LIST_NOT_EXISTANT("B_T_D_TLNE"),
	/**
	 * The task to update doesn't exist within the database.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_TASKSERVICE_DAL_TASK_NOT_EXISTANT("B_T_D_TNE"),
	/**
	 * The reminder that should be deleted doesn't exist.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_TASKSERVICE_DAL_TASK_REMINDER_NOT_EXISTANT("B_T_D_TRNE"),
	/**
	 * The task relation that should be deleted is not existent.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_TASKSERVICE_DAL_TASK_RELATION_NOT_EXISTANT("B_T_D_TRLNE"),
	/**
	 * Parameters missing to perform the desired action.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_TASKSERVICE_BEAN_PARAMETERS_MISSING("B_T_B_PM"),
	/**
	 * No task for the given ID found.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_TASKSERVICE_BEAN_TASK_NOT_EXISTANT("B_T_B_TNE"),
	/**
	 * No task list exists for the given ID.
	 * <p>
	 * Indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_TASKSERVICE_BEAN_TASK_LIST_NOT_EXISTANT("B_T_B_TLNE"),
	/**
	 * The task to add has a start date after the due date.
	 * <p>
	 * Is the result of a missing check at the client or a wrong user input.
	 *
	 * @since 0.1
	 */
	BACKEND_TASKSERVICE_BEAN_TASK_STARTS_OVERDUE("B_T_B_TSO"),
	/**
	 * The predecessor's due date isn't before the main task's due date.
	 * <p>
	 * This error is a result of a wrong user selection. The client should perform
	 * such checks before the request is sent.
	 *
	 * @since 0.1
	 */
	BACKEND_TASKSERVICE_BEAN_PREDECESSOR_NOT_BEFORE_TASK("B_T_B_PNBT"),
	/**
	 * No slot was found for the task. Tasks, that were planned before stay planned
	 * until a new planning run is performed.
	 * <p>
	 * Isn't a bug. The user should change the inputs, e.g. move some events or due
	 * dates to get the tasks planned.
	 *
	 * @since 0.1
	 */
	BACKEND_TASKSERVICE_BEAN_ALL_SLOTS_FILLED("B_T_B_ASF"),
	/**
	 * The given user ID is not allowed to perform operations on the task list or
	 * task.
	 *
	 * @since 0.1
	 */
	BACKEND_TASKSERVICE_BEAN_WRONG_USER_ID("B_T_B_WUI");

	@Getter
	private final String code;

	PRPErrorCode(final String code) {
		this.code = code;
	}

}
