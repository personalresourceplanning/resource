/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.common.enums.calendar;

import java.util.HashMap;
import java.util.Map;

/**
 * This enum defines the access level an user can have for a calendar.
 * <p>
 * If he created the calendar he is the {@link #OWNER}. In this case, he has all
 * rights for the calendar, even for deletion.
 * <p>
 * As an {@link #ADMIN}, he has the rights to administrate the calendar. This is
 * mostly changing title and description and giving access rights.
 * <p>
 * If there's no need to administrate a calendar but to edit the events,
 * {@link #EDIT} permissions are needed.
 * <p>
 * To view all information, including the dates' information, the user needs the
 * permission {@link #ALL_DETAILS}.
 * <p>
 * If a user has the permission {@link #BLOCKED_ONLY}, only information about
 * the availability of another user is shown. This access right is mostly useful
 * for user groups.
 * <p>
 * All permissions can be revoked for any users or groups with {@link #EXCLUDE}.
 *
 * @author Eric Fischer
 * @since 0.1
 *
 */
public enum CalendarPermissions {

	/**
	 * @see CalendarPermissions
	 * @since 0.1
	 */
	OWNER,
	/**
	 * @see CalendarPermissions
	 * @since 0.1
	 */
	ADMIN,
	/**
	 * @see CalendarPermissions
	 * @since 0.1
	 */
	EDIT,
	/**
	 * @see CalendarPermissions
	 * @since 0.1
	 */
	ALL_DETAILS,
	/**
	 * @see CalendarPermissions
	 * @since 0.1
	 */
	BLOCKED_ONLY,
	/**
	 * Not yet implemented since there are no groups.
	 *
	 * @see CalendarPermissions
	 * @since 0.1
	 */
	EXCLUDE;

	static Map<String, CalendarPermissions> stringMap = new HashMap<>();

	static {
		CalendarPermissions[] values = CalendarPermissions.values();
		for (CalendarPermissions calendarPermissions : values) {
			stringMap.put(calendarPermissions.toString(), calendarPermissions);
		}
	}

	public static CalendarPermissions getEnumCase(String key) {
		return stringMap.get(key);
	}

}
