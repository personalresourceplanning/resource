/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.common.enums.email;

import lombok.Getter;

/**
 * Enum defining the request methods for sending and receiving emails.
 * <p>
 * At the moment, only standard protocols like IMAP, POP and SMTP are supported.
 *
 * @author Eric Fischer
 * @since 0.1
 *
 */
public enum EmailRequestMethod {

	IMAP(Direction.RECEIVE),
	POP3(Direction.RECEIVE),
	SMTP(Direction.SEND);

	@Getter
	private Direction direction;

	private EmailRequestMethod(final Direction direction) {
		this.direction = direction;
	}

	public enum Direction {
		SEND,
		RECEIVE,
		SYNC;
	}

}
