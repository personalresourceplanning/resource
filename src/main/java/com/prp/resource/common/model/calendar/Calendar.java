/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.common.model.calendar;

import java.util.HashMap;
import java.util.Map;

import com.prp.resource.common.enums.calendar.CalendarPermissions;

import lombok.Data;

@Data
public class Calendar {

	private Long id;
	private String name;
	private String description;
	private Map<Long, CalendarPermissions> permissions = new HashMap<>();

}
