/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.common.model.email.account;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Model representing an email address. It contains the name of the sender and his/her address.
 * <p>
 * The {@link #EmailAddress(String)} constructor provides the parsing for the standard email display format
 * <code>Name &lt;name@provider.domain&gt;</code>
 *
 * @author Eric Fischer
 * @since 0.1
 *
 */
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class EmailAddress {

	private static final String EMAIL_REGEX = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
			+ "[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f"
			+ "\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")"
			+ "@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])"
			+ "?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5["
			+ "0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x"
			+ "08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\" + "x0e-\\x7f])+)\\])";

	private static final String SPACE = " ";
	private static final String GREATER_THAN = ">";
	private static final String LOWER_THAN = "<";
	private static final String EMPTY = "";
	private static final String QUOTATION_MARKS = "\"";

	public EmailAddress(String standardFormat) {
		// TODO REGEX anpassen
		String standardFormat1 = "";
		if (!standardFormat.contains(SPACE)) {
			standardFormat1 = standardFormat.toLowerCase();
			standardFormat = standardFormat1;
		}

		if (standardFormat.matches(EMAIL_REGEX)) {
			email = standardFormat;
		} else if (standardFormat.matches(".+<" + EMAIL_REGEX + ">")) {
			String[] splitted = standardFormat.split(LOWER_THAN);

			this.publicName = splitted[0].replaceAll(QUOTATION_MARKS, EMPTY).trim();
			this.email = splitted[1].replaceAll(LOWER_THAN, EMPTY).replaceAll(GREATER_THAN, EMPTY)
					.replaceAll(QUOTATION_MARKS, EMPTY).trim().toLowerCase();
		} else {
			String[] splitted = standardFormat.split(LOWER_THAN);

			this.publicName = splitted[0].replaceAll(QUOTATION_MARKS, EMPTY).trim();
			this.email = splitted[1].replaceAll(LOWER_THAN, EMPTY).replaceAll(GREATER_THAN, EMPTY)
					.replaceAll(QUOTATION_MARKS, EMPTY).trim().toLowerCase();

			// TODO change error handling
			if (this.publicName == null && this.email == null) {
				throw new IllegalArgumentException(
						"The entered format is not parsable. Please use the default constructor.");
			}

		}
	}

	@Getter
	@Setter
	private String email;
	@Getter
	@Setter
	private String publicName;

	@Override
	public String toString() {
		return publicName + SPACE + LOWER_THAN + email + GREATER_THAN;
	}

}
