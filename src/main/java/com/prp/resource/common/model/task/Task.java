/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.common.model.task;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
public class Task {

	private Long id;
	private String title;
	private String description;
	private Date dueDate;
	private Date startDate;
	private Duration duration;
	private Boolean done = Boolean.FALSE;
	private Long taskListId;
	private List<Date> reminders = new ArrayList<>();
	private List<TaskList> subLists = new ArrayList<>();
	/**
	 * Tasks, that have to be finished before this task can be completed.
	 */
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private List<Task> predecessor = new ArrayList<>();

	public Task() {}

	public Task(final Long id) {
		this.id = id;
	}

}
