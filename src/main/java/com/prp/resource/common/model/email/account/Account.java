/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.common.model.email.account;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.prp.resource.common.enums.email.EmailRequestMethod;
import com.prp.resource.common.model.email.account.connection.Connection;

import lombok.Data;

/**
 * Model for standard email accounts. It includes all connection methods for sending and receiving emails and all addresses available from
 * this account.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see EmailRequestMethod
 * @see EmailAddress
 * @see Connection
 */
@Data
public class Account {

	private Long id;
	private String name;
	private Map<EmailRequestMethod, Connection> connectionMethods = new HashMap<>();
	private List<EmailAddress> addresses = new ArrayList<>();
	private EmailRequestMethod primaryFetchingMethod;

}
