/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.common.model.email;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.prp.resource.common.enums.email.EmailRequestMethod;
import com.prp.resource.common.model.email.account.Account;
import com.prp.resource.common.model.email.account.EmailAddress;

import lombok.Data;

/**
 * Model representing an email.
 * <p>
 * An email contains every information to send or receive a message. So information about the sending or receiving account is needed for
 * example.
 *
 * @author Eric Fischer
 *
 */
@Data
public class Email {

	private Long id;
	private List<EmailAddress> to = new ArrayList<>();
	private List<EmailAddress> cc = new ArrayList<>();
	private List<EmailAddress> bcc = new ArrayList<>();
	private String subject;
	private String message;
	private Account sendingAccount;
	private Account receivingAccount;
	private EmailAddress from;
	private List<Appandix> appandicies = new ArrayList<>();
	private Date sentDate;
	private Date receivedDate;
	private EmailRequestMethod method;

	/**
	 * When setting an account, the sending address is resetted if not available in the account.
	 *
	 * @param account - {@link Account}
	 */
	public void setAccount(final Account account) {
		if (EmailRequestMethod.Direction.SEND.equals(method.getDirection())) {
			this.sendingAccount = account;
		} else if (EmailRequestMethod.Direction.RECEIVE.equals(method.getDirection())) {
			this.receivingAccount = account;
		}
		if (this.from != null && ((sendingAccount == null && receivingAccount == null)
				|| !account.getAddresses().contains(this.from))) {
			this.from = null;
		}
	}

	// /**
	// * When setting the from address, checks are performed, if the address is part
	// * of the account set to this email. If not, an {@link
	// IllegalArgumentException}
	// * is thrown.
	// *
	// * @param from
	// * - {@link EmailAddress}
	// * @throws IllegalArgumentException
	// */
	// public void setFrom(EmailAddress from) {
	// if (this.sendingAccount == null && receivingAccount == null) {
	// this.from = null;
	// return;
	// }
	// if (sendingAccount != null
	// && (sendingAccount.getAddresses() == null
	// || !sendingAccount.getAddresses().contains(from))
	// || receivingAccount != null && (receivingAccount.getAddresses() == null
	// || !receivingAccount.getAddresses().contains(from))) {
	// throw new IllegalArgumentException("From address not contained in specified
	// account.");
	// }
	// this.from = from;
	// }
}
