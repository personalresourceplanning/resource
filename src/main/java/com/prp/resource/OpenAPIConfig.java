/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

@Configuration
public class OpenAPIConfig {

	@Bean
	public OpenAPI customOpenAPI() {
		//@formatter:off
		return new OpenAPI().components(new Components())
		.info(new Info()
				.title("PRP Resource service API")
				.description("Access the main functionality of the PRP system with the API of this" +
				" service. Will be devided into more subservices in future.<br>" +
				"All paths documented here rely on the base path /api/0.1/ through the gateway service.")
				.version("0.1")
				.license(new License().name("GNU GPL 3").url("https://www.gnu.org/licenses/"))
				.contact(new Contact().name("Eric Fischer").email("prp@ericfischer.eu")));
		//@formatter:on

	}
}
