/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.calendarservice.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.prp.resource.common.exception.PRPException;
import com.prp.resource.calendarservice.dal.model.EventDB;
import com.prp.resource.calendarservice.dal.model.ReminderDB;
import com.prp.resource.common.model.calendar.Event;

/**
 * Converter between event API and database models.
 *
 * @author Eric Fischer
 * @since 0.1
 *
 */
@Component
public final class EventConverter {

	private CalendarConverter calendarConverter;

	public EventConverter(final CalendarConverter calendarConverter) {
		this.calendarConverter = calendarConverter;
	}

	/**
	 * Complete conversion from {@link EventDB} to {@link Event}.
	 *
	 * @param in - {@link EventDB}
	 * @return {@link Event}
	 * @throws PRPException
	 */
	public Event convertEvent(final EventDB in) throws PRPException {
		Event out = new Event();

		out.setId(in.getId());
		out.setCalendar(calendarConverter.convertCalendar(in.getCalendar()));
		out.setCreatorId(in.getCreatorsUserId());
		out.setTitle(in.getTitle());
		out.setDescription(in.getDescription());
		out.setStartDate(in.getStartDate());
		out.setEndDate(in.getEndDate());
		out.setLocation(in.getLocation());
		List<Date> reminders = new ArrayList<>();
		for (ReminderDB reminder : in.getReminders()) {
			reminders.add(convertReminder(reminder));
		}
		out.setReminders(reminders);

		return out;
	}

	public Date convertReminder(final ReminderDB in) {
		return in.getRemindTime();
	}

}
