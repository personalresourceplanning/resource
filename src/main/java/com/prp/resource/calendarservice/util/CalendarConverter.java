/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.calendarservice.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.prp.resource.calendarservice.dal.model.CalendarDB;
import com.prp.resource.calendarservice.dal.model.CalendarPermissionDB;
import com.prp.resource.common.enums.calendar.CalendarPermissions;
import com.prp.resource.common.enums.exception.PRPErrorCode;
import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.model.calendar.Calendar;

/**
 * Converter between calendar API and database models.
 *
 * @author Eric Fischer
 * @since 0.1
 *
 */
@Component
public final class CalendarConverter {

	private static final Logger log = LoggerFactory.getLogger(CalendarConverter.class);

	/**
	 * Converts a {@link CalendarDB} into a {@link Calendar}.
	 * <p>
	 * {@link Event}s are not converted, because there are different access rights
	 * on the details, which should be managed by the bean.
	 *
	 * @param in
	 *            - {@link CalendarDB} to convert
	 * @return {@link Calendar} - The converted calendar.
	 * @throws PRPException
	 */
	public Calendar convertCalendar(final CalendarDB in) throws PRPException {
		Calendar out = new Calendar();
		out.setId(in.getId());
		out.setName(in.getName());
		out.setDescription(in.getDescription());
		out.setPermissions(convertPermissions(in.getPermissions()));
		return out;
	}

	public Map<Long, CalendarPermissions>
			convertPermissions(final Collection<CalendarPermissionDB> in) throws PRPException {
		Map<Long, CalendarPermissions> out = new HashMap<>();
		for (CalendarPermissionDB permission : in) {
			if (!permission.getDeleted()) {
				CalendarPermissions chosenPermission = determinePermissionEnum(permission);
				out.put(permission.getUserId(), chosenPermission);
			}
		}
		return out;
	}

	public CalendarPermissions determinePermissionEnum(final CalendarPermissionDB permission)
			throws PRPException {
		CalendarPermissions enumValue = null;
		switch (permission.getAccessLevel()) {
			case CalendarPermissionDB.OWNER:
				enumValue = CalendarPermissions.OWNER;
				break;
			case CalendarPermissionDB.ADMIN:
				enumValue = CalendarPermissions.ADMIN;
				break;
			case CalendarPermissionDB.EDIT:
				enumValue = CalendarPermissions.EDIT;
				break;
			case CalendarPermissionDB.ALL_DETAILS:
				enumValue = CalendarPermissions.ALL_DETAILS;
				break;
			case CalendarPermissionDB.BLOCKED_ONLY:
				enumValue = CalendarPermissions.BLOCKED_ONLY;
				break;
			default:
				StringBuilder sb = new StringBuilder("Cannot convert ");
				sb.append(permission.getAccessLevel());
				sb.append(" to any known calendar permission.");
				log.error(sb.toString());
				throw new PRPException(sb.toString(),
						PRPErrorCode.BACKEND_CALENDARSERVICE_BEAN_PERMISSION_NOT_CONVERTABLE);
		}
		return enumValue;
	}

	public CalendarDB convertCalendar(final Calendar in) {
		CalendarDB out = new CalendarDB();
		out.setName(in.getName());
		out.setDescription(in.getDescription());
		out.setPermissions(convertPermissions(in.getPermissions(), out));
		return out;
	}

	public Set<CalendarPermissionDB> convertPermissions(final Map<Long, CalendarPermissions> in,
			final CalendarDB calendar) {
		Set<CalendarPermissionDB> out = new HashSet<>();
		for (Long user : in.keySet()) {
			CalendarPermissionDB db = new CalendarPermissionDB();
			db.setUserId(user);
			db.setCalendar(calendar);
			db.setAccessLevel(determinePermissionString(in.get(user)));
			out.add(db);
		}
		return out;
	}

	public String determinePermissionString(final CalendarPermissions calendarPermission) {
		switch (calendarPermission) { // TODO add EXCLUDE
			case OWNER:
				return CalendarPermissionDB.OWNER;
			case ADMIN:
				return CalendarPermissionDB.ADMIN;
			case EDIT:
				return CalendarPermissionDB.EDIT;
			case ALL_DETAILS:
				return CalendarPermissionDB.ALL_DETAILS;
			case BLOCKED_ONLY:
				return CalendarPermissionDB.BLOCKED_ONLY;
			// no default case, because every enum value should be represented here
		}
		return null;
	}

}
