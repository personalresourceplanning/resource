/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.calendarservice.dal.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.prp.resource.calendarservice.dal.repository.ReminderRepository;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import lombok.ToString;

/**
 * Representing a reminder for an event.
 *
 * @author Eric Fischer
 * @see ReminderRepository
 * @see EventDB
 *
 */
@Entity
@Table(name = "lia_backend_calendarservice_reminder")
@Data
public class ReminderDB implements Comparable<ReminderDB> {

	@Id
	@GeneratedValue
	@Setter(AccessLevel.NONE)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "eventid", nullable = false, unique = false)
	@ToString.Exclude
	private EventDB event;

	@Column(name = "time", nullable = false, unique = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date remindTime;

	@Column(name = "deleted", nullable = false, unique = false)
	private Boolean deleted = Boolean.valueOf(false);

	@Override
	public int compareTo(final ReminderDB o) {
		return remindTime.compareTo(o.getRemindTime());
	}

}
