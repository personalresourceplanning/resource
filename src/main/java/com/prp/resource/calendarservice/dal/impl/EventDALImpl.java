/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.calendarservice.dal.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.prp.resource.calendarservice.dal.EventDAL;
import com.prp.resource.calendarservice.dal.model.CalendarDB;
import com.prp.resource.calendarservice.dal.model.CalendarPermissionDB;
import com.prp.resource.calendarservice.dal.model.EventDB;
import com.prp.resource.calendarservice.dal.model.ReminderDB;
import com.prp.resource.calendarservice.dal.queries.EventDALQueries;
import com.prp.resource.calendarservice.dal.repository.CalendarPermissionsRepository;
import com.prp.resource.calendarservice.dal.repository.CalendarRepository;
import com.prp.resource.calendarservice.dal.repository.EventRepository;
import com.prp.resource.calendarservice.dal.repository.ReminderRepository;
import com.prp.resource.common.enums.exception.PRPErrorCode;
import com.prp.resource.common.exception.PRPException;

@Component
public class EventDALImpl implements EventDAL {

	private static final Logger log = LoggerFactory.getLogger(EventDALImpl.class);

	private EventRepository eventRepository;
	private ReminderRepository reminderRepository;
	private CalendarRepository calendarRepository;
	private CalendarPermissionsRepository calendarPermissionsRepository;
	private EventDALQueries q;

	@Autowired
	public EventDALImpl(final EventRepository eventRepository,
			final ReminderRepository reminderRepository,
			final CalendarRepository calendarRepository,
			final CalendarPermissionsRepository calendarPermissionsRepository,
			final EventDALQueries queries) {
		this.eventRepository = eventRepository;
		this.reminderRepository = reminderRepository;
		this.calendarRepository = calendarRepository;
		this.calendarPermissionsRepository = calendarPermissionsRepository;
		this.q = queries;
	}

	@Override
	public EventDB loadSingleEvent(final Long eventId) throws PRPException {
		if (eventId == null) {
			String message = "No ID given to search for.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_PARAMETERS_MISSING);
		}
		Optional<EventDB> found = eventRepository.findById(eventId);
		if (!found.isPresent()) {
			log.info("No event for ID found.");
			return null;
		}
		return found.get();
	}

	@Override
	public List<EventDB> loadEventsForDay(final Long userID, final Date date) throws PRPException {

		if (userID == null || userID.equals(Long.valueOf(0)) || date == null) {
			String message = "Search parameters for fetch of events not complete.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_FETCH_DATA_NOT_COMPLETE);
		}

		Set<CalendarPermissionDB> permissions = calendarPermissionsRepository.findByUserId(userID);

		if (permissions.isEmpty()) {
			StringBuilder sb = new StringBuilder("User ");
			sb.append(userID);
			sb.append(" has no permissions on any calendar.");
			log.warn(sb.toString());
			return new ArrayList<>();
		}

		GregorianCalendar gregCal = new GregorianCalendar();
		gregCal.setTime(date);
		GregorianCalendar min = new GregorianCalendar();
		GregorianCalendar max = new GregorianCalendar();
		min.set(Calendar.DAY_OF_MONTH, gregCal.get(Calendar.DAY_OF_MONTH));
		min.set(Calendar.MONTH, gregCal.get(Calendar.MONTH));
		min.set(Calendar.YEAR, gregCal.get(Calendar.YEAR));
		min.set(Calendar.HOUR_OF_DAY, 0);
		min.set(Calendar.MINUTE, 0);
		min.set(Calendar.SECOND, 0);
		max.set(Calendar.DAY_OF_MONTH, gregCal.get(Calendar.DAY_OF_MONTH));
		max.set(Calendar.MONTH, gregCal.get(Calendar.MONTH));
		max.set(Calendar.YEAR, gregCal.get(Calendar.YEAR));
		max.set(Calendar.HOUR_OF_DAY, 23);
		max.set(Calendar.MINUTE, 59);
		max.set(Calendar.SECOND, 59);

		StringBuilder debug = new StringBuilder();
		debug.append("Searching for events between ");
		debug.append(min.getTime().toString());
		debug.append(" and ");
		debug.append(max.getTime().toString());
		debug.append(".");
		log.debug(debug.toString());

		Set<EventDB> eventsForDay = new HashSet<>();

		for (CalendarPermissionDB permission : permissions) {
			// filter the correct dates
			// @formatter:off
			List<EventDB> events =
					eventRepository.findAll(Specification
					.where(q.belongsTo(permission.getCalendar()))
					.and(q.startsAfter(min.getTime()))
					.and(q.startsBefore(max.getTime()))
					.and(q.notDeleted())
					);
			// @formatter:on
			eventsForDay.addAll(events);
			// @formatter:off
			events = eventRepository.findAll(
					Specification
					.where(q.belongsTo(permission.getCalendar()))
					.and(q.endsAfter(min.getTime()))
					.and(q.endsBefore(max.getTime()))
					.and(q.notDeleted())
					);
			// @formatter:on
			eventsForDay.addAll(events);
			// @formatter:off
			events = eventRepository.findAll(
					Specification
					.where(q.belongsTo(permission.getCalendar()))
					.and(q.startsBefore(min.getTime()))
					.and(q.endsAfter(max.getTime()))
					.and(q.notDeleted())
					);
			// @formatter:on
			eventsForDay.addAll(events);
		}

		for (EventDB eventDB : eventsForDay) {
			Iterator<ReminderDB> iterator = eventDB.getReminders().iterator();
			while (iterator.hasNext()) {
				ReminderDB reminder = iterator.next();
				if (reminder.getDeleted().equals(Boolean.FALSE)) {
					iterator.remove();
				}
			}
		}

		if (eventsForDay.isEmpty()) {
			log.warn("No events found for day.");
		}

		List<EventDB> retVal = new ArrayList<>(eventsForDay);
		return retVal;
	}

	@Override
	public List<EventDB> loadEventsForUser(final Long userID) throws PRPException {
		if (userID == null) {
			String message = "No user ID given to search the events for.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_PARAMETERS_MISSING);
		}

		Set<CalendarPermissionDB> permissions = calendarPermissionsRepository.findByUserId(userID);

		List<EventDB> events = new ArrayList<>();

		for (CalendarPermissionDB permission : permissions) {
			events.addAll(eventRepository.findAll(Specification
					.where(q.belongsTo(permission.getCalendar())).and(q.notDeleted())));
		}

		if (events.isEmpty()) {
			log.warn("No events found for user.");
		}

		return events;
	}

	@Override
	public EventDB storeEvent(final EventDB event) throws PRPException {
		checkEventData(event);
		eventRepository.save(event);
		storeReminders(event);
		return event;
	}

	@Override
	public EventDB updateEvent(final EventDB event) throws PRPException {
		checkEventData(event);
		checkId(event);
		return eventRepository.save(event);
	}

	@Override
	public void deleteEvent(final Long eventId) throws PRPException {
		if (eventId == null || Long.valueOf(0).equals(eventId)) {
			String message = "The event ID is missing to identify the resource to delete or update.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_NO_EVENT_ID_GIVEN);
		}
		EventDB event = loadEvent(eventId);
		event.setDeleted(Boolean.valueOf(true));
		eventRepository.save(event);
	}

	@Override
	public List<ReminderDB> loadReminders(final Long eventId) throws PRPException {
		if (eventId == null || Long.valueOf(0).equals(eventId)) {
			String message = "The event ID is missing to identify the resource of the reminders.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_NO_EVENT_ID_GIVEN);
		}
		Optional<EventDB> eventWrapper = eventRepository.findById(eventId);
		if (!eventWrapper.isPresent()) {
			return null;
		}
		return reminderRepository.findByEvent(eventWrapper.get());
	}

	@Override
	public void deleteReminder(final Long eventID, final Date reminderTime) throws PRPException {
		checkReminderParams(eventID, reminderTime);
		EventDB event = loadEvent(eventID);
		event.getReminders().clear();
		event.getReminders().addAll(reminderRepository.findByEvent(event));
		for (ReminderDB reminder : event.getReminders()) {
			if (reminder.getRemindTime().equals(reminderTime)) {
				reminder.setDeleted(Boolean.valueOf(true));
				reminderRepository.save(reminder);
			}
		}
	}

	@Override
	public ReminderDB addReminder(final Long eventID, final Date reminderTime) throws PRPException {
		checkReminderParams(eventID, reminderTime);
		EventDB event = loadEvent(eventID);
		ReminderDB reminder = new ReminderDB();
		reminder.setEvent(event);
		reminder.setRemindTime(reminderTime);
		return reminderRepository.save(reminder);
	}

	private void checkReminderParams(final Long eventID, final Date reminderTime)
			throws PRPException {
		if (eventID == null || Long.valueOf(0).equals(eventID)) {
			String message = "No event given to link the reminder to.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_NO_EVENT_ID_GIVEN);
		}
		if (reminderTime == null) {
			String message = "No remind time for the reminder given.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_NO_REMIND_TIME_GIVEN);
		}
	}

	private void checkEventData(final EventDB event) throws PRPException {
		// NULL check
		if (event == null) {
			String message = "There's no event given to store to DB.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_NO_EVENT_GIVEN);
		}

		// start/end check
		if (event.getStartDate() == null || event.getEndDate() == null) {
			String message = "The start and end date are not set.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_START_AND_END_NOT_SET);
		}

		// calendar NULL check
		if (event.getCalendar() == null || event.getCalendar().getId() == null
				|| event.getCalendar().getId().equals(Long.valueOf(0))) {
			throw new PRPException("No calendar given.",
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_NO_CALENDAR_GIVEN);
		}

		// calendar existence check
		Optional<CalendarDB> calendar = calendarRepository.findById(event.getCalendar().getId());
		if (!calendar.isPresent()) {
			String message = "The calendar the event should be stored to could not be found.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_NO_CALENDAR_FOR_ID_FOUND);
		}

	}

	private void checkId(final EventDB event) throws PRPException {
		if (event.getId() == null || Long.valueOf(0).equals(event.getId())) {
			String message = "The event ID is missing to identify the resource to delete or update.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_NO_EVENT_ID_GIVEN);
		}
	}

	private void storeReminders(final EventDB event) throws PRPException {
		for (ReminderDB reminder : event.getReminders()) {
			if (reminder.getRemindTime() == null) {
				String message = "No remind time set.";
				log.error(message);
				throw new PRPException(message,
						PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_NO_REMIND_TIME_GIVEN);
			}
			reminder.setEvent(event);
			reminderRepository.save(reminder);
		}
	}

	/**
	 * Ensures that an event is loaded!
	 *
	 * @param eventID
	 * @return
	 * @throws PRPException
	 */
	private EventDB loadEvent(final Long eventID) throws PRPException {
		Optional<EventDB> DBevent = eventRepository.findById(eventID);
		if (!DBevent.isPresent()) {
			String message = "The event for the given ID was not found.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_NO_EVENT_FOR_ID_FOUND);
		}
		EventDB event = DBevent.get();
		if (event.getDeleted().equals(Boolean.TRUE)) {
			log.warn("The searched event is deleted.");
			return null;
		}
		return event;
	}

}
