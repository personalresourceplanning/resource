/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.calendarservice.dal;

import java.util.Date;
import java.util.List;

import com.prp.resource.common.exception.PRPException;
import com.prp.resource.calendarservice.dal.model.EventDB;
import com.prp.resource.calendarservice.dal.model.ReminderDB;
import com.prp.resource.calendarservice.dal.repository.EventRepository;
import com.prp.resource.calendarservice.dal.repository.ReminderRepository;

/**
 * Database access layer for event and reminder information.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see EventDB
 * @see EventRepository
 * @see ReminderDB
 * @see ReminderRepository
 *
 */
public interface EventDAL {

	/**
	 * Fetches the information for the {@link Event} with the given ID.
	 *
	 * @param eventId - The {@link Long} identifying the {@link Event}.
	 * @return {@link Event}
	 * @throws PRPException
	 * @since 0.1
	 */
	EventDB loadSingleEvent(Long eventId) throws PRPException;

	/**
	 * Loading events for the given {@link Date}. The {@link Event}s are sorted by the start date.
	 *
	 * @param date - The {@link Date} to search the events for. Keep in mind that only the day is important and time will be ignored.
	 * @return {@link List}&lt;{@link EventDB}&gt; - The sorted list of {@link Event}s by start date.
	 * @throws PRPException
	 * @since 0.1
	 */
	List<EventDB> loadEventsForDay(Long userID, Date date) throws PRPException;

	/**
	 * Loads all <strong>future</strong> {@link EventDB}s for the given user.
	 * <p>
	 * The reason for the loading of only future {@link EventDB}s is, that a faster response is possible.
	 *
	 * @param userID - The {@link Long} identifying the user to load the {@link EventDB}s for.
	 * @return {@link List}&lt;{@link EventDB}&gt; - All future events.
	 * @throws PRPException
	 */
	List<EventDB> loadEventsForUser(Long userID) throws PRPException;

	/**
	 * Stores an {@link EventDB} to the database.
	 *
	 * @param event - The {@link EventDB} to store.
	 * @return {@link EventDB} - The stored event.
	 * @throws PRPException
	 * @since 0.1
	 */
	EventDB storeEvent(EventDB event) throws PRPException;

	/**
	 * Updates an {@link EventDB}. The update is performed on the object found for the given ID.
	 * <p>
	 * <strong>Important</strong> <br>
	 * The reminders for the event will <strong>not</strong> be updated. Please use the corresponding reminder functions.
	 *
	 * @param event - The {@link EventDB} to update. Remember to set the ID, otherwise the corresponding DB-representation cannot be found and
	 *              updated.
	 * @return {@link EventDB} - The updated event.
	 * @throws PRPException
	 * @since 0.1
	 */
	EventDB updateEvent(EventDB event) throws PRPException;

	/**
	 * Deletes an {@link EventDB}. The deletion is performed on the object found for the given ID.
	 * <p>
	 * The deletion is done by marking the corresponding database entry as deleted.
	 *
	 * @param event - The {@link EventDB} to delete. Remember to set the ID, otherwise the corresponding DB-representation cannot be found and
	 *              deleted.
	 * @throws PRPException
	 * @since 0.1
	 */
	void deleteEvent(Long event) throws PRPException;

	/**
	 * Loads all reminders for the event.
	 *
	 * @param eventId - The {@link Long} identifying the event.
	 * @return {@link Date}
	 * @throws PRPException
	 * @since 0.1
	 */
	List<ReminderDB> loadReminders(Long eventId) throws PRPException;

	/**
	 * Deletes a reminder for the given {@link EventDB}.
	 *
	 * @param eventID      - The {@link Long} Hibernate event id.
	 * @param reminderTime - The {@link Date} the reminder was set to.
	 * @throws PRPException
	 * @since 0.1
	 */
	void deleteReminder(Long eventID, Date reminderTime) throws PRPException;

	/**
	 * Add a reminder to the DB.
	 *
	 * @param eventID      - The {@link Long} event ID to add the reminder for.
	 * @param reminderTime - The {@link Date} at which the reminder should trigger.
	 * @throws PRPException
	 * @since 0.1
	 */
	ReminderDB addReminder(Long eventID, Date reminderTime) throws PRPException;

}
