/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.calendarservice.dal.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prp.resource.calendarservice.dal.CalendarDAL;
import com.prp.resource.calendarservice.dal.model.CalendarDB;
import com.prp.resource.calendarservice.dal.model.CalendarPermissionDB;
import com.prp.resource.calendarservice.dal.repository.CalendarPermissionsRepository;
import com.prp.resource.calendarservice.dal.repository.CalendarRepository;
import com.prp.resource.common.enums.exception.PRPErrorCode;
import com.prp.resource.common.exception.PRPException;

@Component
public class CalendarDALImpl implements CalendarDAL {

	private static final Logger log = LoggerFactory.getLogger(CalendarDALImpl.class);

	private CalendarRepository calendarRepository;
	private CalendarPermissionsRepository permissionRepository;

	@Autowired
	public CalendarDALImpl(final CalendarRepository calendarRepository,
			final CalendarPermissionsRepository calendarAccessRepository) {
		this.calendarRepository = calendarRepository;
		this.permissionRepository = calendarAccessRepository;
	}

	@Override
	public List<CalendarDB> readCalendars(final Long userId) throws PRPException {
		Set<CalendarPermissionDB> permissions = permissionRepository.findByUserId(userId);
		List<CalendarDB> calendars = new ArrayList<>();
		for (CalendarPermissionDB permission : permissions) {
			Optional<CalendarDB> calendar = calendarRepository
					.findById(permission.getCalendar().getId());
			if (calendar.isPresent()) {
				calendars.add(calendar.get());
			}
		}
		return calendars;
	}

	@Override
	public CalendarDB loadCalendar(final Long calendarId) throws PRPException {
		Optional<CalendarDB> calendar = calendarRepository.findById(calendarId);
		if (!calendar.isPresent()) {
			StringBuilder sb = new StringBuilder("No calendar with ID ");
			sb.append(calendarId);
			sb.append(" found.");
			log.warn(sb.toString());
			return null;
		}
		return calendar.get();
	}

	@Override
	public CalendarDB storeCalendar(final CalendarDB calendar) throws PRPException {
		checkCalendarData(calendar);
		calendarRepository.save(calendar);
		for (CalendarPermissionDB permission : calendar.getPermissions()) {
			permission.setCalendar(calendar);
			addCalendarPermission(permission);
		}
		return calendarRepository.findById(calendar.getId()).get();
	}

	@Override
	public CalendarDB updateCalendar(final CalendarDB calendar) throws PRPException {
		checkCalendarData(calendar);
		checkCalendarID(calendar);

		CalendarDB stored = calendarRepository.save(calendar);
		stored.setPermissions(permissionRepository.findByCalendar(stored));
		return stored;
	}

	@Override
	public void deleteCalendar(final CalendarDB calendar) throws PRPException {
		checkCalendarID(calendar);
		Optional<CalendarDB> found = calendarRepository.findById(calendar.getId());
		if (found.isPresent()) {
			found.get().setDeleted(Boolean.TRUE);
			calendarRepository.save(found.get());
		}
	}

	@Override
	public Set<CalendarPermissionDB> loadCalendarPermissions(final Long userId)
			throws PRPException {
		if (userId == null) {
			String message = "The userId has to be set to fetch the permisions.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_PARAMETERS_MISSING);
		}
		return permissionRepository.findByUserId(userId);
	}

	@Override
	public CalendarPermissionDB addCalendarPermission(final CalendarPermissionDB calendarAccess)
			throws PRPException {
		checkCalendarPermissionData(calendarAccess);
		checkCalendarID(calendarAccess);
		checkIfUserAlreadyHasPermission(calendarAccess);

		if (calendarAccess.getUserId() == null) {
			log.warn("Setting public access at level " + calendarAccess.getAccessLevel()
					+ " for calendar with ID " + calendarAccess.getCalendar().getId());
		}
		permissionRepository.save(calendarAccess);
		return calendarAccess;
	}

	@Override
	public CalendarPermissionDB updateCalendarPermission(final CalendarPermissionDB calendarAccess)
			throws PRPException {
		if (calendarAccess.getId() == null || calendarAccess.getId().equals(Long.valueOf(0))) {
			String message = "No ID given to update calendar permission.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_PERMISSION_ID_MISSING);
		}
		return permissionRepository.save(calendarAccess);
	}

	@Override
	public void deleteCalendarPermission(final CalendarPermissionDB calendarAccess)
			throws PRPException {
		if (calendarAccess.getId() == null || calendarAccess.getId().equals(Long.valueOf(0))) {
			String message = "No ID given to update calendar permission.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_PERMISSION_ID_MISSING);
		}
		permissionRepository.deleteById(calendarAccess.getId());
	}

	private void checkCalendarData(final CalendarDB calendar) throws PRPException {
		if (calendar == null || calendar.getName() == null || calendar.getName().isEmpty()) {
			String message = "The calendar data is not complete.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_CALENDAR_DATA_NO_COMPLETE);
		}
	}

	private void checkCalendarID(final CalendarPermissionDB calendarAccess) throws PRPException {
		if (calendarAccess == null || calendarAccess.getCalendar() == null
				|| calendarAccess.getCalendar().getId() == null
				|| calendarAccess.getCalendar().getId().equals(Long.valueOf(0))) {
			String message = "Calendar permission data not complete - ID missing.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_CALENDAR_ID_MISSING);
		}
	}

	private void checkCalendarID(final CalendarDB calendar) throws PRPException {
		if (calendar == null || calendar.getId() == null
				|| calendar.getId().equals(Long.valueOf(0))) {
			String message = "Calendar ID missing.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_CALENDAR_ID_MISSING);
		}
	}

	private void checkCalendarPermissionData(final CalendarPermissionDB calendarAccess)
			throws PRPException {
		if (calendarAccess == null || calendarAccess.getAccessLevel() == null
				|| calendarAccess.getAccessLevel().isEmpty()) {
			String message = "The permission information is not complete.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_CALENDAR_PERMISSION_DATA_NO_COMPLETE);
		}
	}

	private void checkIfUserAlreadyHasPermission(final CalendarPermissionDB calendarAccess)
			throws PRPException {
		Optional<CalendarPermissionDB> found = permissionRepository
				.findByCalendarAndUserId(calendarAccess.getCalendar(), calendarAccess.getUserId());
		if (found.isPresent()) {
			StringBuilder sb = new StringBuilder("The user ");
			sb.append(calendarAccess.getUserId());
			sb.append(" already has a permission on the calendar ");
			sb.append(calendarAccess.getCalendar().getId());
			sb.append(".");
			log.error(sb.toString());
			throw new PRPException(sb.toString(),
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_USER_ALREADY_HAS_PERMISSION);
		}
	}

}
