/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.calendarservice.dal.queries;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.prp.resource.calendarservice.dal.model.CalendarDB;
import com.prp.resource.calendarservice.dal.model.EventDB;
import com.prp.resource.calendarservice.dal.repository.EventRepository;

/**
 * Queries extending the possibilities on fetching data from the database.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see EventRepository
 *
 */
@Component
public class EventDALQueries {

	public Specification<EventDB> belongsTo(final CalendarDB calendar) {
		return new Specification<EventDB>() {

			private static final long serialVersionUID = -3151125831855787158L;

			@Override
			public Predicate toPredicate(final Root<EventDB> root, final CriteriaQuery<?> query,
					final CriteriaBuilder criteriaBuilder) {
				Predicate pred = criteriaBuilder.equal(root.get("calendar"), calendar);
				return pred;
			}

		};
	}

	public Specification<EventDB> startsBefore(final Date date) {
		return new Specification<EventDB>() {

			private static final long serialVersionUID = -37527103211058646L;

			@Override
			public Predicate toPredicate(final Root<EventDB> root, final CriteriaQuery<?> query,
					final CriteriaBuilder criteriaBuilder) {
				Predicate pred = criteriaBuilder.lessThan(root.get("startDate"), date);
				return pred;
			}

		};
	}

	public Specification<EventDB> startsAfter(final Date date) {
		return new Specification<EventDB>() {

			private static final long serialVersionUID = 6341341425278540439L;

			@Override
			public Predicate toPredicate(final Root<EventDB> root, final CriteriaQuery<?> query,
					final CriteriaBuilder criteriaBuilder) {
				Predicate pred = criteriaBuilder.greaterThan(root.get("startDate"), date);
				return pred;
			}

		};
	}

	public Specification<EventDB> endsBefore(final Date date) {
		return new Specification<EventDB>() {

			private static final long serialVersionUID = 1473407699889968566L;

			@Override
			public Predicate toPredicate(final Root<EventDB> root, final CriteriaQuery<?> query,
					final CriteriaBuilder criteriaBuilder) {
				Predicate pred = criteriaBuilder.lessThan(root.get("endDate"), date);
				return pred;
			}

		};
	}

	public Specification<EventDB> endsAfter(final Date date) {
		return new Specification<EventDB>() {

			private static final long serialVersionUID = 2299365158243889681L;

			@Override
			public Predicate toPredicate(final Root<EventDB> root, final CriteriaQuery<?> query,
					final CriteriaBuilder criteriaBuilder) {
				Predicate pred = criteriaBuilder.greaterThan(root.get("endDate"), date);
				return pred;
			}

		};
	}

	public Specification<EventDB> notDeleted() {
		return new Specification<EventDB>() {

			private static final long serialVersionUID = -7020925549193134142L;

			@Override
			public Predicate toPredicate(final Root<EventDB> root, final CriteriaQuery<?> query,
					final CriteriaBuilder criteriaBuilder) {
				Predicate pred = criteriaBuilder.not(root.get("deleted"));
				return pred;
			}

		};
	}

}
