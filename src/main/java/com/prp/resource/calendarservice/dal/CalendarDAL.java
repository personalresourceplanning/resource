/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.calendarservice.dal;

import java.util.List;
import java.util.Set;

import com.prp.resource.common.exception.PRPException;
import com.prp.resource.calendarservice.dal.model.CalendarDB;
import com.prp.resource.calendarservice.dal.model.CalendarPermissionDB;
import com.prp.resource.calendarservice.dal.repository.CalendarPermissionsRepository;
import com.prp.resource.calendarservice.dal.repository.CalendarRepository;

/**
 * Database access layer for calendar and permission information.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see CalendarDB
 * @see CalendarRepository
 * @see CalendarPermissionDB
 * @see CalendarPermissionsRepository
 *
 */
public interface CalendarDAL {

	/**
	 * Fetches all calendars for the given user.
	 *
	 * @param userId - The {@link Long} representing the user to fetch the calendars for.
	 * @return {@link List}&lt;{@link CalendarDB}&gt; - A list containing all calendars stored for the given user.
	 * @throws PRPException
	 * @since 0.1
	 */
	List<CalendarDB> readCalendars(Long userId) throws PRPException;

	/**
	 * Loads the {@link Calendar} for the given ID.
	 *
	 * @param calendarId
	 * @return {@link Calendar}
	 * @throws PRPException
	 * @since 0.1
	 */
	CalendarDB loadCalendar(Long calendarId) throws PRPException;

	/**
	 * Stores a {@link CalendarDB} into the database. Permissions given with the calendar are stored.
	 *
	 * @param calendar - The {@link CalendarDB} to store.
	 * @return {@link CalendarDB} - The stored calendar.
	 * @throws PRPException
	 * @since 0.1
	 */
	CalendarDB storeCalendar(CalendarDB calendar) throws PRPException;

	/**
	 * Updates calendar details. Use permission functions to update calendar access definitions.
	 *
	 * @param calendar - The {@link CalendarDB} to update.
	 * @return {@link CalendarDB} - The updated calendar representation.
	 * @throws PRPException
	 * @since 0.1
	 */
	CalendarDB updateCalendar(CalendarDB calendar) throws PRPException;

	/**
	 * Marks a {@link CalendarDB} as deleted. Never deletes a calendar physically.
	 *
	 * @param calendar - The {@link CalendarDB} to delete.
	 * @throws PRPException
	 * @since 0.1
	 */
	void deleteCalendar(CalendarDB calendar) throws PRPException;

	/**
	 * Loads all calendar permissions for the given user.
	 *
	 * @param userId - The {@link Long} identifying the user.
	 * @return {@link List}&lt;{@link CalendarPermissionDB}&gt;
	 * @throws PRPException
	 * @since 0.1
	 */
	Set<CalendarPermissionDB> loadCalendarPermissions(Long userId) throws PRPException;

	/**
	 * Adds a {@link CalendarPermissionDB} to the database.
	 *
	 * @param calendarAccess - The {@link CalendarPermissionDB} to store.
	 * @return {@link CalendarPermissionDB} - The stored permission.
	 * @throws PRPException
	 * @since 0.1
	 */
	CalendarPermissionDB addCalendarPermission(CalendarPermissionDB calendarAccess) throws PRPException;

	/**
	 * Updates a {@link CalendarPermissionDB}.
	 *
	 * @param calendarAccess - The {@link CalendarPermissionDB} to update.
	 * @return {@link CalendarPermissionDB} - The stored permission.
	 * @throws PRPException
	 * @since 0.1
	 */
	CalendarPermissionDB updateCalendarPermission(CalendarPermissionDB calendarAccess) throws PRPException;

	/**
	 * Deletes a {@link CalendarPermissionDB} physically.
	 *
	 * @param calendarAccess - The {@link CalendarPermissionDB} to delete.
	 * @throws PRPException
	 * @since 0.1
	 */
	void deleteCalendarPermission(CalendarPermissionDB calendarAccess) throws PRPException;

}
