/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.calendarservice.dal.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.prp.resource.calendarservice.dal.repository.CalendarRepository;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Representing the general calendar information within the database.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see CalendarRepository
 * @see CalendarPermissionDB
 * @see EventDB
 *
 */
@Entity
@Table(name = "lia_backend_calendarservice_calendar")
@EqualsAndHashCode
@Data
public class CalendarDB {

	@Id
	@GeneratedValue
	@EqualsAndHashCode.Include
	@Setter(AccessLevel.NONE)
	private Long id;

	@Getter
	@Setter
	@EqualsAndHashCode.Exclude
	@Column(name = "name", nullable = false, unique = false)
	private String name;

	@Getter
	@Setter
	@EqualsAndHashCode.Exclude
	@Column(name = "description", nullable = true, unique = false)
	private String description;

	@Getter
	@Setter
	@EqualsAndHashCode.Exclude
	@OneToMany(mappedBy = "calendar")
	private List<EventDB> events = new ArrayList<>();

	@Getter
	@Setter
	@EqualsAndHashCode.Exclude
	@OneToMany(mappedBy = "calendar")
	private Set<CalendarPermissionDB> permissions = new HashSet<>();

	@Getter
	@Setter
	@EqualsAndHashCode.Exclude
	@Column(name = "deleted", nullable = false)
	private Boolean deleted = Boolean.FALSE;

}
