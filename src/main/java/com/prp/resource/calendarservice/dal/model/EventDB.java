/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.calendarservice.dal.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.prp.resource.calendarservice.dal.repository.EventRepository;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.ToString;

/**
 * Representing the event information.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see EventRepository
 * @see CalendarDB
 * @see ReminderDB
 *
 */
@Entity
@Table(name = "lia_backend_calendarservice_event")
@Data
public class EventDB implements Comparable<EventDB> {

	@GeneratedValue
	@Id
	@Setter(AccessLevel.NONE)
	private Long id;

	@Column(name = "title", unique = false, nullable = true)
	private String title;

	// future TODO extract time information into new table
	// in order to allow recurring events
	@Column(name = "startdate", unique = false, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;

	@Column(name = "enddate", unique = false, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;

	@Column(name = "description", unique = false, nullable = true)
	private String description;

	@Column(name = "location", unique = false, nullable = true)
	private String location;

	@OneToMany(mappedBy = "event")
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	private Set<ReminderDB> reminders = new HashSet<>();

	@Column(name = "deleted", nullable = false, unique = false)
	private Boolean deleted = Boolean.FALSE;

	@ManyToOne
	@JoinColumn(name = "calendar", nullable = false)
	@ToString.Exclude
	private CalendarDB calendar;

	@Column(name = "creator", unique = false, nullable = false)
	private Long creatorsUserId;

	@Override
	public int compareTo(final EventDB other) {
		return startDate.compareTo(other.getStartDate());
	}

}
