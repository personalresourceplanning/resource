/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.calendarservice.dal.repository;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.prp.resource.calendarservice.dal.model.CalendarDB;
import com.prp.resource.calendarservice.dal.model.CalendarPermissionDB;

/**
 * Repository containing calendar permission information.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see CalendarPermissionDB
 * @see CalendarRepository
 *
 */
public interface CalendarPermissionsRepository extends JpaRepository<CalendarPermissionDB, Long> {

	Set<CalendarPermissionDB> findByCalendar(CalendarDB calendar);

	Optional<CalendarPermissionDB> findByCalendarAndUserId(CalendarDB calendar, Long userId);

	Set<CalendarPermissionDB> findByUserId(Long userId);

}
