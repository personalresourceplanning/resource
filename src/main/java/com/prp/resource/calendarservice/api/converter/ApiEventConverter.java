package com.prp.resource.calendarservice.api.converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.prp.resource.calendarservice.api.model.ApiEvent;
import com.prp.resource.common.model.calendar.Event;

@Component
public class ApiEventConverter {

	private final SimpleDateFormat sdf = new SimpleDateFormat(ApiEvent.URL_DATE_FORMAT_STRING);

	public ApiEvent convert(Event in) {
		ApiEvent out = new ApiEvent();
		out.setId(in.getId());
		out.setCalendarId(in.getCalendar().getId());
		out.setTitle(in.getTitle());
		out.setDescription(in.getDescription());
		out.setLocation(in.getLocation());
		out.setStartDate(sdf.format(in.getStartDate()));
		out.setEndDate(sdf.format(in.getEndDate()));
		return out;
	}

	public Event convert(ApiEvent in) throws ParseException {
		Event out = new Event();
		out.setId(in.getId());
		out.setTitle(in.getTitle());
		out.setDescription(in.getDescription());
		out.setStartDate(sdf.parse(in.getStartDate()));
		out.setEndDate(sdf.parse(in.getEndDate()));
		out.setLocation(in.getLocation());
		return out;
	}

	public List<ApiEvent> convertAll(List<Event> in) {
		List<ApiEvent> out = new ArrayList<>();
		for (Event event : in) {
			out.add(convert(event));
		}
		return out;
	}

}
