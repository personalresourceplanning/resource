/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.calendarservice.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.prp.resource.calendarservice.api.converter.ApiCalendarConverter;
import com.prp.resource.calendarservice.api.converter.ApiEventConverter;
import com.prp.resource.calendarservice.api.model.ApiCalendar;
import com.prp.resource.calendarservice.api.model.ApiEvent;
import com.prp.resource.calendarservice.bean.CalendarBean;
import com.prp.resource.calendarservice.bean.EventBean;
import com.prp.resource.common.enums.calendar.CalendarPermissions;
import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.model.calendar.Calendar;
import com.prp.resource.common.model.calendar.Event;
import com.prp.resource.exception.api.pojo.ApiExceptionResponse;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * Controller for accessing calendar functionality.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see CalendarBean
 * @see EventBean
 *
 */
@RestController
@RequestMapping("/calendar")
public class CalendarserviceController {

	private static final Logger log = LoggerFactory.getLogger(CalendarserviceController.class);

	private CalendarBean calendarBean;
	private EventBean eventBean;
	private ApiCalendarConverter calendarConverter;
	private ApiEventConverter eventConverter;

	@Autowired
	public CalendarserviceController(final CalendarBean calendarBean, final EventBean eventBean,
			ApiCalendarConverter calendarConverter, ApiEventConverter eventConverter) {
		this.calendarBean = calendarBean;
		this.eventBean = eventBean;
		this.calendarConverter = calendarConverter;
		this.eventConverter = eventConverter;
	}

	@Operation(
			summary = "Load a specific calendar.",
			description = "This endpoint loads the calendar details for the specified ID. It does not load any calendar entries.")
	@ApiResponses(
			value = {
					@ApiResponse(
							responseCode = "HTTP 200",
							description = "The calendar was found.",
							content = @Content(
									schema = @Schema(implementation = ApiCalendar.class),
									mediaType = "JSON")),
					@ApiResponse(
							responseCode = "HTTP 404",
							description = "No calendar was found for the given ID.",
							content = @Content()),
					@ApiResponse(
							responseCode = "HTTP 400 / B_C_B_PM",
							description = "No ID parameter present. Should not occur, since there's an extra endpoint without the ID.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = { @ExampleObject(
											value = "{ \n \"status\": 400 [...] \n \"internalError\": \"B_C_B_PM\" \n }") })) })
	@GetMapping("/{id}")
	public ResponseEntity<ApiCalendar>
			loadCalendar(@PathVariable(name = "id", required = true) @Parameter(
					description = "The ID of the calendar to load.",
					example = "1") final Long calendarId) throws PRPException {
		logCall("loadCalendar()", calendarId.toString());
		Calendar calendar = calendarBean.loadCalendar(calendarId);
		if (calendar == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(calendarConverter.convert(calendar), HttpStatus.OK);
	}

	@Operation(
			summary = "Load all calendars the current user can access.",
			description = "Loads all calendars the user has access rights for, even if he/she can only see blocked times.")
	@ApiResponses(
			value = {
					@ApiResponse(
							responseCode = "HTTP 200",
							description = "Calendars were found.",
							content = @Content(
									mediaType = "JSON",
									array = @ArraySchema(
											schema = @Schema(implementation = ApiCalendar.class)))),
					@ApiResponse(
							responseCode = "HTTP 404",
							description = "No calendars were found. The user has no access rights and no own calendars.",
							content = @Content()) })
	@GetMapping
	public ResponseEntity<List<ApiCalendar>> loadAllCalendars() throws PRPException {
		logCall("loadAllCalendars()", StringUtils.EMPTY);
		List<Calendar> foundCalendars = calendarBean.loadAllCalendars();
		List<ApiCalendar> response = new ArrayList<>();
		for (Calendar calendar : foundCalendars) {
			response.add(calendarConverter.convert(calendar));
		}
		if (response.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@Operation(
			summary = "Create a new calendar",
			description = "Creates a new calendar. As the title is no identifier for the calendar, there can be two calendars with the same title.")
	@ApiResponses(
			value = { @ApiResponse(
					responseCode = "HTTP 201",
					description = "The calendar has been created.",
					content = @Content(
							mediaType = "JSON",
							schema = @Schema(implementation = ApiCalendar.class))) })
	@PostMapping("/create")
	public ResponseEntity<ApiCalendar> createCalendar(@io.swagger.v3.oas.annotations.parameters.RequestBody(
			description = "The calendar object to store. "
					+ "The ID will be generated and is not necessary to call this endpoint.") @RequestBody  final ApiCalendar calendar)
			throws PRPException {
		logCall("createCalendar()", calendar.toString());
		Calendar created = calendarBean.createCalendar(calendarConverter.convert(calendar));
		return new ResponseEntity<>(calendarConverter.convert(created), HttpStatus.CREATED);
	}

	@Operation(
			summary = "Update calendar's title and description.",
			description = "Updates the calendar's information like title and desciption. Permissions are not updated in this endpoint.")
	@ApiResponses(
			value = {
					@ApiResponse(
							responseCode = "HTTP 200",
							description = "The calendar has been updated successfully.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiCalendar.class))),
					@ApiResponse(
							responseCode = "HTTP 400 / B_C_B_CNG",
							description = "The calendar data was not set.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 400 [...] \n \"internalError\": \"B_C_B_CNG\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 404 / B_C_B_CNF",
							description = "No calendar found for the given ID.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 404 [...] \n \"internalError\": \"B_C_B_CNF\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 403 / B_C_B_UNP",
							description = "The user is not allowed to change the selected calendar.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 403 [...] \n \"internalError\": \"B_C_B_UNP\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 400 / B_C_B_PM",
							description = "Needed parameters for the permissions check are missing. Should not occur.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = { @ExampleObject(
											value = "{ \n \"status\": 400 [...] \n \"internalError\": \"B_C_B_PM\" \n }") })) })
	@PutMapping("/{id}")
	public ResponseEntity<ApiCalendar> updateCalendar(
			@PathVariable(name = "id") @Parameter(
					description = "The ID of the calendar to update.",
					example = "1") final Long calendarId,
			@io.swagger.v3.oas.annotations.parameters.RequestBody(
					description = "The updated calendar information. "
							+ "The ID will be ignored as it is set in the path.") @RequestBody final ApiCalendar in)
			throws PRPException {
		logCall("updateCalendar()", in.toString());
		in.setId(calendarId);
		Calendar input = calendarConverter.convert(in);
		Calendar updated = calendarBean.updateCalendar(input);
		return new ResponseEntity<>(calendarConverter.convert(updated), HttpStatus.OK);
	}

	@Operation(
			summary = "Delete a specific calendar",
			description = "Delete a calendar identified by it's ID. Only users with the OWNER permission are allowed to delete calendars.")
	@ApiResponses(
			value = {
					@ApiResponse(
							responseCode = "HTTP 204",
							description = "The calendar has been deleted successfully."),
					@ApiResponse(
							responseCode = "HTTP 403 / B_C_B_UNP",
							description = "The user trying to delete the calendar is not an owner of it.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 403 [...] \n \"internalError\": \"B_C_B_UNP\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 404 / B_C_B_CNF",
							description = "No calendar was found for the given ID.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 404 [...] \n \"internalError\": \"B_C_B_CNF\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 400 / B_C_B_PM",
							description = "Needed parameters for the permissions check are missing. Should not occur.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = { @ExampleObject(
											value = "{ \n \"status\": 400 [...] \n \"internalError\": \"B_C_B_PM\" \n }") })) })
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteCalendar(@PathVariable(name = "id", required = true)
	@Parameter(description = "The identifier of the calendar to delete.") final Long calendarId)
			throws PRPException {
		logCall("deleteCalendar()", calendarId.toString());
		calendarBean.deleteCalendar(calendarId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@Operation(
			summary = "Grant a permission on a certain calendar.",
			description = "Grant a permission on a certain calendar. "
					+ "Only OWNER and ADMIN users are allowed to change the permissions on a calendar. "
					+ "While an OWNER is allowed to grant all access rights including the OWNER role, ADMIN "
					+ "users are only allowed to grant access rights lower then the ADMIN role. "
					+ "One user can only hold one access right.")
	@ApiResponses(
			value = {
					@ApiResponse(
							responseCode = "HTTP 200",
							description = "The permission has been set successfuly."),
					@ApiResponse(
							responseCode = "HTTP 400 / B_C_B_CNG",
							description = "Necessary data was not set.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 400 [...] \n \"internalError\": \"B_C_B_CNG\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 400 / B_C_D_UAHP",
							description = "The user who should receive the permission already has a permission on the calendar. Use PUT to update a permission level.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 400 [...] \n \"internalError\": \"B_C_D_UAHP\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 403 / B_C_B_UNP",
							description = "The user is not allowed to change the selected calendar.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 403 [...] \n \"internalError\": \"B_C_B_UNP\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 400 / B_C_B_PM",
							description = "Needed parameters for the permissions check are missing. Should not occur.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = { @ExampleObject(
											value = "{ \n \"status\": 400 [...] \n \"internalError\": \"B_C_B_PM\" \n }") })),
					@ApiResponse(
							responseCode = "HTTP 404 / B_C_D_NCFIDF",
							description = "No calendar was found for the given ID.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 404 [...] \n \"internalError\": \"B_C_D_NCFIDF\" \n }"))) })
	@PostMapping("{id}/{user}/permission")
	public ResponseEntity<Void> addCalendarPermission(@PathVariable("id") @Parameter(
			example = "1",
			description = "The ID of the calendar to set the permission for.") final Long calendarId,
			@PathVariable("user") @Parameter(
					example = "3",
					description = "The user's ID to grant the permission for.") final Long userId,
			@io.swagger.v3.oas.annotations.parameters.RequestBody(
					description = "The permission value. Valid values: OWNER, ADMIN, EDIT, ALL_DETAILS, BLOCKED_ONLY, EXCLUDE") @RequestBody final String permission)
			throws PRPException {
		logCall("addCalendarPermission()", permission, calendarId.toString(), userId.toString());
		calendarBean.addCalendarPermission(CalendarPermissions.getEnumCase(permission), calendarId,
				userId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@Operation(
			summary = "Update a user's permission on the calendar.",
			description = "Update a previously created permission on the given calendar. "
					+ "See the POST endpoint for access level documentation.")
	@ApiResponses(
			value = {
					@ApiResponse(
							responseCode = "HTTP 200",
							description = "The permission has been updated successfuly."),
					@ApiResponse(
							responseCode = "HTTP 400 / B_C_B_CNG",
							description = "Necessary data was not set.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 400 [...] \n \"internalError\": \"B_C_B_CNG\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 400 / B_C_B_NPTU",
							description = "The user who should receive the permission has no permission on the calendar. Use POST to grant a permission.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 400 [...] \n \"internalError\": \"B_C_D_UAHP\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 403 / B_C_B_UNP",
							description = "The user is not allowed to change the selected calendar.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 403 [...] \n \"internalError\": \"B_C_B_UNP\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 400 / B_C_B_PM",
							description = "Needed parameters for the permissions check are missing. Should not occur.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = { @ExampleObject(
											value = "{ \n \"status\": 400 [...] \n \"internalError\": \"B_C_B_PM\" \n }") })),
					@ApiResponse(
							responseCode = "HTTP 404 / B_C_D_NCFIDF",
							description = "No calendar was found for the given ID.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 404 [...] \n \"internalError\": \"B_C_D_NCFIDF\" \n }"))) })
	@PutMapping("{id}/{user}/permission")
	public ResponseEntity<Void> updateCalendarPermission(@PathVariable("id") @Parameter(
			example = "1",
			description = "The ID of the calendar to update the permission of.") final Long calendarId,
			@PathVariable("user") @Parameter(
					example = "3",
					description = "The user's ID whose permission should be updated.") Long userId,
			@io.swagger.v3.oas.annotations.parameters.RequestBody(
					description = "The permission value. Valid values: see POST endpoint") @RequestBody final String permission)
			throws PRPException {
		logCall("updateCalendarPermission()", permission, calendarId.toString(), userId.toString());
		calendarBean.updateCalendarPermission(CalendarPermissions.getEnumCase(permission),
				calendarId, userId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@Operation(
			summary = "Remove a user's permission on a calendar.",
			description = "Removes a user's permission on the given calendar. "
					+ "Be careful: When added a permission to a group, the user will inherit the group's permission level. "
					+ "It is possible, that users will get more permissions when removing their permission entry. "
					+ "To exclude certain users from accessing the calendar, use EXCLUDE permission.")
	@ApiResponses(
			value = { @ApiResponse(
					responseCode = "HTTP 200",
					description = "The deletion was successful. If there was no entry to delete, the deletion is supposed to be successful."),
					@ApiResponse(
							responseCode = "HTTP 403 / B_C_B_UNP",
							description = "The user is not allowed to change the selected calendar.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 403 [...] \n \"internalError\": \"B_C_B_UNP\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 400 / B_C_B_PM",
							description = "Needed parameters for the permissions check are missing. Should not occur.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = { @ExampleObject(
											value = "{ \n \"status\": 400 [...] \n \"internalError\": \"B_C_B_PM\" \n }") })),
					@ApiResponse(
							responseCode = "HTTP 404 / B_C_D_NCFIDF",
							description = "No calendar was found for the given ID.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 404 [...] \n \"internalError\": \"B_C_D_NCFIDF\" \n }"))) })
	@DeleteMapping("{id}/{user}/permission")
	public ResponseEntity<Object> removeCalendarPermission(@PathVariable(
			name = "id",
			required = true)
	@Parameter(
			example = "1",
			description = "The ID of the calendar to remove the permission from.") final Long calendarId,
			@PathVariable(name = "user", required = true) @Parameter(
					example = "1",
					description = "The user ID whose permission should be removed.") final Long userId)
			throws PRPException {
		logCall("removeCalendarPermission()", calendarId.toString(), userId.toString());
		calendarBean.removeCalendarPermission(calendarId, userId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@Operation(
			summary = "Load events for the current user.",
			description = "Loads a list of events either for the specified day or all known events. "
					+ "It's recommended to always use the endpoint per day to avoid extreme loading times.")
	@ApiResponses(
			value = {
					@ApiResponse(
							responseCode = "HTTP 200",
							description = "Events were found and will be returned.",
							content = @Content(
									mediaType = "JSON",
									array = @ArraySchema(
											schema = @Schema(implementation = ApiEvent.class)))),
					@ApiResponse(responseCode = "HTTP 404", description = "No events were found."),
					@ApiResponse(
							responseCode = "HTTP 400",
							description = "The day parameter was malformed."
									+ " Make shure to always hand in event date strings in the format "
									+ ApiEvent.URL_DATE_FORMAT_STRING,
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(
											implementation = ApiExceptionResponse.class))) })
	@GetMapping("/event/list")
	public ResponseEntity<List<ApiEvent>> loadEvents(
			@RequestParam(name = "date", required = false) @Parameter(
					description = "The day in the format " + ApiEvent.URL_DATE_FORMAT_STRING
							+ " to search the events for") final String dayString)
			throws PRPException, ParseException {
		logCall("loadEvents()", dayString != null ? dayString.toString() : "");
		if (dayString != null) {
			Date day = new SimpleDateFormat(ApiEvent.URL_DATE_FORMAT_STRING).parse(dayString);
			List<Event> eventsForDay = eventBean.loadEventsForDay(day);
			if (eventsForDay.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<>(eventConverter.convertAll(eventsForDay), HttpStatus.OK);
		}
		List<Event> eventsForCurrentUser = eventBean.loadEventsForCurrentUser();
		if (eventsForCurrentUser.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(eventConverter.convertAll(eventsForCurrentUser), HttpStatus.OK);
	}

	@Operation(
			summary = "Create a new event.",
			description = "Create a new event in the specified calendar.")
	@ApiResponses(
			value = {
					@ApiResponse(
							responseCode = "HTTP 201",
							description = "The event has been created.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiEvent.class))),
					@ApiResponse(
							responseCode = "HTTP 400",
							description = "If no error code provided, a malformed date has been handed to the endpoint."),
					@ApiResponse(
							responseCode = "HTTP 400 / B_C_B_PM",
							description = "Needed parameters are missing. Should not occur.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = { @ExampleObject(
											value = "{ \n \"status\": 400 [...] \n \"internalError\": \"B_C_B_PM\" \n }") })),
					@ApiResponse(
							responseCode = "HTTP 400 / B_C_B_EBS",
							description = "The end date of the event is before the start date.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = { @ExampleObject(
											value = "{ \n \"status\": 400 [...] \n \"internalError\": \"B_C_B_EBS\" \n }") })),
					@ApiResponse(
							responseCode = "HTTP 403 / B_C_B_UNP",
							description = "No permission to add events to the desired calendar.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = { @ExampleObject(
											value = "{ \n \"status\": 403 [...] \n \"internalError\": \"B_C_B_UNP\" \n }") })),
					@ApiResponse(
							responseCode = "HTTP 404 / B_C_D_NCFIDF",
							description = "No calendar was found for the given ID.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 404 [...] \n \"internalError\": \"B_C_D_NCFIDF\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 400",
							description = "The start or end time was malformed."
									+ " Make shure to always hand in event date strings in the format "
									+ ApiEvent.URL_DATE_FORMAT_STRING,
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(
											implementation = ApiExceptionResponse.class))) })
	@PostMapping("{id}/event")
	public ResponseEntity<ApiEvent>
			addEvent(@PathVariable("id") @Parameter(
					description = "The calendar ID to store the event in.") final Long calendarId,
					@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "The event data to store.") @RequestBody final ApiEvent request)
					throws PRPException, ParseException {
		logCall("addEvent()", calendarId.toString(), request.toString());
		Event event = eventConverter.convert(request);
		event.setCalendar(new Calendar());
		event.getCalendar().setId(calendarId);

		Event added = eventBean.addEvent(event);
		return new ResponseEntity<>(eventConverter.convert(added), HttpStatus.CREATED);
	}

	@Operation(
			summary = "Change an existing event.",
			description = "Update the information on an existing event.")
	@ApiResponses(
			value = {
					@ApiResponse(
							responseCode = "HTTP 201",
							description = "The event has been updated.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiEvent.class))),
					@ApiResponse(
							responseCode = "HTTP 400 / B_C_B_PM",
							description = "Needed parameters are missing. Should not occur.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = { @ExampleObject(
											value = "{ \n \"status\": 400 [...] \n \"internalError\": \"B_C_B_PM\" \n }") })),
					@ApiResponse(
							responseCode = "HTTP 404 / B_C_B_NEFIF",
							description = "No event was found for the given ID.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 404 [...] \n \"internalError\": \"B_C_B_NEFIF\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 403 / B_C_B_UNP",
							description = "No permission to modify events to the desired calendar.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = { @ExampleObject(
											value = "{ \n \"status\": 403 [...] \n \"internalError\": \"B_C_B_UNP\" \n }") })),
					@ApiResponse(
							responseCode = "HTTP 404 / B_C_D_NCFIDF",
							description = "No calendar was found for the given ID.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 404 [...] \n \"internalError\": \"B_C_D_NCFIDF\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 400",
							description = "The start or end time was malformed."
									+ " Make shure to always hand in event date strings in the format "
									+ ApiEvent.URL_DATE_FORMAT_STRING,
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(
											implementation = ApiExceptionResponse.class))) })
	@PutMapping("{cid}/event/{eid}")
	public ResponseEntity<ApiEvent> updateEvent(@PathVariable("cid") final Long calendarId,
			@PathVariable("eid") final Long eventId, @RequestBody final ApiEvent request)
			throws PRPException, ParseException {
		logCall("updateEvent()", calendarId.toString(), eventId.toString(), request.toString());
		Event event = eventConverter.convert(request);
		event.setCalendar(new Calendar());
		event.getCalendar().setId(calendarId);

		return new ResponseEntity<>(eventConverter.convert(eventBean.updateEvent(event)),
				HttpStatus.OK);
	}

	@Operation(
			summary = "Mark a specific event as deleted.",
			description = "Delete an event. When deleted, the event's data is still stored, but it won't appear for the user anymore.")
	@ApiResponses(
			value = {
					@ApiResponse(
							responseCode = "HTTP 204",
							description = "The deletion was successful."),
					@ApiResponse(
							responseCode = "HTTP 403 / B_C_B_UNP",
							description = "No permission to modify events in the desired calendar.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = { @ExampleObject(
											value = "{ \n \"status\": 403 [...] \n \"internalError\": \"B_C_B_UNP\" \n }") })),
					@ApiResponse(
							responseCode = "HTTP 404 / B_C_D_NEFIF",
							description = "No event was found for the given ID.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 404 [...] \n \"internalError\": \"B_C_D_NEFIF\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 404 / B_C_D_NCFIDF",
							description = "No calendar was found for the given ID.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 404 [...] \n \"internalError\": \"B_C_D_NCFIDF\" \n }"))) })
	@DeleteMapping("{cid}/event/{eid}")
	public ResponseEntity<Object> deleteEvent(@PathVariable("cid")
	@Parameter(description = "The ID of the calendar the event belongs to.") final Long calendarId,
			@PathVariable("eid") @Parameter(description = "The event's ID.") final Long eventId)
			throws PRPException {
		logCall("updateEvent()", calendarId.toString(), eventId.toString());
		eventBean.deleteEvent(eventId);

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@Operation(
			summary = "Add a reminder to the specified event.",
			description = "Adds a reminder to an event.")
	@ApiResponses(
			value = {
					@ApiResponse(
							responseCode = "HTTP 200",
							description = "The reminder has been added successfully."),
					@ApiResponse(
							responseCode = "HTTP 403 / B_C_B_UNP",
							description = "No permission to modify events in the desired calendar.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = { @ExampleObject(
											value = "{ \n \"status\": 403 [...] \n \"internalError\": \"B_C_B_UNP\" \n }") })),
					@ApiResponse(
							responseCode = "HTTP 404 / B_C_D_NEFIF",
							description = "No event was found for the given ID.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 404 [...] \n \"internalError\": \"B_C_D_NEFIF\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 404 / B_C_D_NCFIDF",
							description = "No calendar was found for the given ID.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 404 [...] \n \"internalError\": \"B_C_D_NCFIDF\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 400",
							description = "The remind time was malformed."
									+ " Make shure to always hand in event date strings in the format "
									+ ApiEvent.URL_DATE_FORMAT_STRING,
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(
											implementation = ApiExceptionResponse.class))) })
	@PostMapping("{cid}/event/{eid}/reminder")
	public ResponseEntity<Event> addReminder(
			@PathVariable("cid")
			@Parameter(description = "The ID of the event's calendar.") final Long calendarId,
			@PathVariable("eid")
			@Parameter(description = "The ID of the event.") final Long eventId,
			@io.swagger.v3.oas.annotations.parameters.RequestBody(
					description = "The remind time timestamp in the format "
							+ ApiEvent.URL_DATE_FORMAT_STRING) @RequestBody final String remindTime)
			throws PRPException, ParseException {
		logCall("addReminder()", calendarId.toString(), eventId.toString(), remindTime);
		SimpleDateFormat sdf = new SimpleDateFormat(ApiEvent.URL_DATE_FORMAT_STRING);
		return new ResponseEntity<>(eventBean.addReminder(eventId, sdf.parse(remindTime)),
				HttpStatus.OK);
	}

	@ApiResponses(
			value = {
					@ApiResponse(
							responseCode = "HTTP 201",
							description = "The reminder has been deleted successfuly."),
					@ApiResponse(
							responseCode = "HTTP 403 / B_C_B_UNP",
							description = "No permission to modify events in the desired calendar.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = { @ExampleObject(
											value = "{ \n \"status\": 403 [...] \n \"internalError\": \"B_C_B_UNP\" \n }") })),
					@ApiResponse(
							responseCode = "HTTP 404 / B_C_D_NEFIF",
							description = "No event was found for the given ID.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 404 [...] \n \"internalError\": \"B_C_D_NEFIF\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 404 / B_C_D_NCFIDF",
							description = "No calendar was found for the given ID.",
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(implementation = ApiExceptionResponse.class),
									examples = @ExampleObject(
											value = "{ \n \"status\": 404 [...] \n \"internalError\": \"B_C_D_NCFIDF\" \n }"))),
					@ApiResponse(
							responseCode = "HTTP 400",
							description = "The remind time was malformed."
									+ " Make shure to always hand in event date strings in the format "
									+ ApiEvent.URL_DATE_FORMAT_STRING,
							content = @Content(
									mediaType = "JSON",
									schema = @Schema(
											implementation = ApiExceptionResponse.class))) })
	@DeleteMapping("{cid}/event/{eid}/reminder/{time}")
	public ResponseEntity<Object> deleteReminder(
			@PathVariable("cid")
			@Parameter(description = "The ID of the event's calendar.") final Long calendarId,
			@PathVariable("eid")
			@Parameter(description = "The ID of the event.") final Long eventId,
			@PathVariable("time") @Parameter(
					description = "The remind time to remove from the event.") final String remindTime)
			throws PRPException, ParseException {
		logCall("deleteReminder()", calendarId.toString(), eventId.toString(), remindTime);
		SimpleDateFormat sdf = new SimpleDateFormat(ApiEvent.URL_DATE_FORMAT_STRING);
		eventBean.deleteReminder(eventId, sdf.parse(remindTime));

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	private void logCall(final String method, final String... parameters) {
		StringBuilder sb = new StringBuilder(method);
		sb.append(" called with parameters [");
		Iterator<String> iterator = Arrays.asList(parameters).iterator();
		appendParameter(sb, iterator);
		sb.append("]");
		log.debug(sb.toString());
	}

	private void appendParameter(final StringBuilder sb, final Iterator<String> iterator) {
		sb.append(iterator.next());
		if (iterator.hasNext()) {
			sb.append(", ");
			appendParameter(sb, iterator);
		}
	}

}
