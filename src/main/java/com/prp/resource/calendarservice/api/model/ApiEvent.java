/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.calendarservice.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiEvent {

	public static final String URL_DATE_FORMAT_STRING = "dd-MM-yyyy HH:mm";

	@Schema(
			example = "1",
			description = "The generated ID and main identifier. Is usually send by the server")
	private Long id;

	@Schema(
			example = "4",
			description = "The Id of the Calendar the Event belongs to")
	private Long CalendarId;

	@Schema(example = "My Meeting", description = "The title of the event.")
	private String title;

	@Schema(
			example = "Meeting to discuss something.",
			description = "The description providing more information")
	private String description;

	@Schema(
			example = "Street 5, 000000 City",
			description = "The place where the event takes place. Might be a meeting room, too. "
					+ "Does not require a special format.")
	private String location;

	@Schema(
			example = "17-01-2021 12:30",
			description = "Date and time marking the start of the event. " + "Requires the format "
					+ URL_DATE_FORMAT_STRING)
	private String startDate;

	@Schema(
			example = "17-01-2021 14:15",
			description = "Date and time marking the end of the event. " + "Requires the format "
					+ URL_DATE_FORMAT_STRING)
	private String endDate;

}
