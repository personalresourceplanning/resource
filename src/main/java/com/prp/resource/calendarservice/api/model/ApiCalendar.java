package com.prp.resource.calendarservice.api.model;

import java.util.HashMap;
import java.util.Map;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class ApiCalendar {

	@Schema(
			example = "1",
			description = "The unique identifier for the calendar. In requests, "
					+ "this has no effect and should be handed via the URL.")
	private Long id;

	@Schema(example = "My Calendar", description = "The displayed name for the calendar.")
	private String name;

	@Schema(
			example = "Calendar for private events.",
			description = "Some additional info for the calendar.")
	private String description;

	@Schema(
			example = "{363: \\\"ALL_DETAILS\\\"}",
			description = "Permissions for the calendar. If the requesting user"
					+ " has no administrative rights, this object will not contain the full list of permissions. "
					+ "In this case, only the own permission is filled in here.")
	private Map<Long, String> permissions = new HashMap<>();

}
