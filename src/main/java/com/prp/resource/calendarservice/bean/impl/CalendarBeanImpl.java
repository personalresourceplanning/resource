/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.calendarservice.bean.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prp.resource.calendarservice.bean.CalendarBean;
import com.prp.resource.calendarservice.dal.CalendarDAL;
import com.prp.resource.calendarservice.dal.model.CalendarDB;
import com.prp.resource.calendarservice.dal.model.CalendarPermissionDB;
import com.prp.resource.calendarservice.util.CalendarConverter;
import com.prp.resource.common.enums.calendar.CalendarPermissions;
import com.prp.resource.common.enums.exception.PRPErrorCode;
import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.model.User;
import com.prp.resource.common.model.calendar.Calendar;
import com.prp.resource.common.service.UserService;

@Component
public class CalendarBeanImpl implements CalendarBean {

	private static final Logger log = LoggerFactory.getLogger(CalendarBeanImpl.class);

	private UserService userService;
	private CalendarDAL dal;
	private CalendarConverter calendarConverter;

	@Autowired
	public CalendarBeanImpl(final UserService userService, final CalendarDAL dal,
			final CalendarConverter calendarConverter) {
		this.userService = userService;
		this.dal = dal;
		this.calendarConverter = calendarConverter;
	}

	@Override
	public Calendar loadCalendar(final Long calendarId) throws PRPException {
		if (calendarId == null) {
			String message = "Parameters not complete or wrong.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_BEAN_PARAMETERS_MISSING);
		}
		CalendarDB found = dal.loadCalendar(calendarId);
		if (found == null) {
			log.warn("No calendar found for given ID.");
			return null;
		}
		Long currentUserId = userService.loadCurrentUser().getId();
		List<CalendarPermissionDB> permissions = found.getPermissions().stream()
				.filter(p -> p.getUserId().equals(currentUserId)).collect(Collectors.toList());
		if (permissions.isEmpty()) {
			log.warn("User has no permissons for requested calendar.");
			return null;
		} else if (permissions.size() > 1) {
			log.warn(String.format("Multiple permissions found in database for user {}.",
					currentUserId));
		}
		Calendar converted = calendarConverter.convertCalendar(found);

		return converted;
	}

	@Override
	public List<Calendar> loadAllCalendars() throws PRPException {
		User user = userService.loadCurrentUser();
		List<CalendarDB> dbCalendars = dal.readCalendars(user.getId());
		List<Calendar> retVal = new ArrayList<>();
		for (CalendarDB calendarDB : dbCalendars) {
			Calendar converted = calendarConverter.convertCalendar(calendarDB);
			retVal.add(converted);
		}
		return retVal;
	}

	@Override
	public Calendar createCalendar(final Calendar calendar) throws PRPException {
		User user = userService.loadCurrentUser();
		// remove all entries with invalid values
		Iterator<Entry<Long, CalendarPermissions>> iterator = calendar.getPermissions().entrySet()
				.iterator();
		while (iterator.hasNext()) {
			Entry<Long, CalendarPermissions> entry = iterator.next();
			if (entry.getValue() == null) {
				iterator.remove();
			}
		}

		if (calendar.getPermissions().get(user.getId()) == null
				|| !calendar.getPermissions().get(user.getId()).equals(CalendarPermissions.OWNER)) {
			calendar.getPermissions().put(user.getId(), CalendarPermissions.OWNER);
		}

		CalendarDB converted = calendarConverter.convertCalendar(calendar);
		CalendarDB stored = dal.storeCalendar(converted);
		return calendarConverter.convertCalendar(stored);
	}

	@Override
	public Calendar updateCalendar(final Calendar calendar) throws PRPException {
		if (calendar == null) {
			String message = "No calendar given to update.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_BEAN_CALENDAR_NOT_GIVEN);
		}
		CalendarDB found = dal.loadCalendar(calendar.getId());
		if (found == null) {
			StringBuilder sb = new StringBuilder("No calendar found in DB for ID ");
			sb.append(calendar.getId());
			sb.append(".");
			log.debug(sb.toString());
			throw new PRPException(sb.toString(),
					PRPErrorCode.BACKEND_CALENDARSERVICE_BEAN_CALENDAR_NOT_FOUND);
		}
		Calendar convertedFound = calendarConverter.convertCalendar(found);
		User user = userService.loadCurrentUser();
		CalendarPermissions usersPermission = convertedFound.getPermissions().get(user.getId());
		if (usersPermission == null || !usersPermission.equals(CalendarPermissions.ADMIN)
				&& !usersPermission.equals(CalendarPermissions.OWNER)) {
			StringBuilder sb = new StringBuilder("User isn't authorized to change calendar ");
			sb.append(convertedFound.getId());
			log.error(sb.toString());
			throw new PRPException(sb.toString(),
					PRPErrorCode.BACKEND_CALENDARSERVICE_BEAN_USER_HAS_NO_PERMISSION);
		}
		// manual conversion because ID isn't setable
		found.setName(calendar.getName());
		found.setDescription(calendar.getDescription());
		found.setPermissions(
				calendarConverter.convertPermissions(calendar.getPermissions(), found));
		CalendarDB updated = dal.updateCalendar(found);
		return calendarConverter.convertCalendar(updated);
	}

	@Override
	public void deleteCalendar(final Long calendarId) throws PRPException {
		CalendarDB loaded = dal.loadCalendar(calendarId);

		if (loaded == null) {
			StringBuilder sb = new StringBuilder();
			sb.append("No calendar for ID ");
			sb.append(calendarId);
			sb.append(" found. No further action taken.");
			log.error(sb.toString());
			throw new PRPException(sb.toString(),
					PRPErrorCode.BACKEND_CALENDARSERVICE_BEAN_CALENDAR_NOT_FOUND);
		}

		User currentUser = userService.loadCurrentUser();

		Set<CalendarPermissionDB> userPermissions = loaded.getPermissions().stream()
				.filter(p -> p.getUserId().equals(currentUser.getId())).collect(Collectors.toSet())
				.stream().filter(p -> p.getAccessLevel().equals(CalendarPermissionDB.OWNER))
				.collect(Collectors.toSet());

		if (userPermissions.isEmpty()) {
			String message = "The user has no sufficient rights to delete a calendar.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_BEAN_USER_HAS_NO_PERMISSION);
		}

		dal.deleteCalendar(loaded);
		log.debug("Calendar marked as deleted.");
	}

	@Override
	public void addCalendarPermission(final CalendarPermissions level, final Long calendarId,
			final Long userId) throws PRPException {

		CalendarDB calendar = checkUserRights(level, calendarId, userId);

		CalendarPermissionDB permission = new CalendarPermissionDB();
		permission.setAccessLevel(calendarConverter.determinePermissionString(level));
		permission.setCalendar(calendar);
		permission.setUserId(userId);

		dal.addCalendarPermission(permission);
	}

	@Override
	public void updateCalendarPermission(final CalendarPermissions level, final Long calendarId,
			final Long userId) throws PRPException {
		checkUserRights(level, calendarId, userId);

		List<CalendarPermissionDB> permissions = dal.loadCalendarPermissions(userId).stream()
				.filter(p -> p.getCalendar().getId().equals(calendarId))
				.collect(Collectors.toList());
		if (permissions.isEmpty()) {
			String message = "The user needs to be granted a permision first.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_BEAN_NO_PERMISSION_TO_UPDATE);
		}
		CalendarPermissionDB permission = permissions.get(0);
		permission.setAccessLevel(calendarConverter.determinePermissionString(level));

		dal.updateCalendarPermission(permission);
	}

	@Override
	public void removeCalendarPermission(final Long calendarId, final Long userId)
			throws PRPException {

		Set<CalendarPermissionDB> permissions = dal.loadCalendarPermissions(userId);
		List<CalendarPermissionDB> permissionToRemove = permissions.stream()
				.filter(p -> p.getCalendar().getId().equals(calendarId))
				.collect(Collectors.toList());

		if (permissionToRemove.isEmpty()) {
			StringBuilder sb = new StringBuilder("No permission found for user ");
			sb.append(userId);
			sb.append(" and calendar ");
			sb.append(calendarId);
			log.warn(sb.toString());
			return;
		}

		checkUserRights(calendarConverter.determinePermissionEnum(permissionToRemove.get(0)),
				calendarId, userId);

		dal.deleteCalendarPermission(permissionToRemove.get(0));

	}

	private CalendarDB checkUserRights(final CalendarPermissions level, final Long calendarId,
			final Long userId) throws PRPException {
		if (level == null || calendarId == null || calendarId.equals(Long.valueOf(0))) {
			String message = "Parameters not complete or wrong.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_BEAN_PARAMETERS_MISSING);
		}

		CalendarDB calendar = dal.loadCalendar(calendarId);
		User currentUser = userService.loadCurrentUser();

		List<CalendarPermissionDB> currentUserPermissions = calendar.getPermissions().stream()
				.filter(p -> p.getUserId().equals(currentUser.getId()))
				.collect(Collectors.toList());

		if (currentUserPermissions.isEmpty()) {
			String message = "User has no rights to update the calendar permissions.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_BEAN_USER_HAS_NO_PERMISSION);
		}

		Set<CalendarPermissionDB> adminPermissions = currentUserPermissions.stream()
				.filter(p -> p.getAccessLevel().equals(CalendarPermissionDB.ADMIN))
				.collect(Collectors.toSet());
		Set<CalendarPermissionDB> ownerPermissions = currentUserPermissions.stream()
				.filter(p -> p.getAccessLevel().equals(CalendarPermissionDB.OWNER))
				.collect(Collectors.toSet());

		if (adminPermissions.isEmpty() && ownerPermissions.isEmpty()
				|| CalendarPermissionDB.ADMIN.equals(currentUserPermissions.get(0).getAccessLevel())
						&& level.equals(CalendarPermissions.OWNER)) {
			String message = "User has not sufficient rights to add or remove calendar permissions.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_BEAN_USER_HAS_NO_PERMISSION);
		}
		return calendar;
	}

}
