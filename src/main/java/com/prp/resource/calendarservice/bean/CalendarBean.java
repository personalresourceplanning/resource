/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.calendarservice.bean;

import java.util.List;

import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.enums.calendar.CalendarPermissions;
import com.prp.resource.common.model.calendar.Calendar;

/**
 * Bean for managing and loading calendar information and the permissions on the calendars.
 * <p>
 * Some of the permissions only apply on the event level at the {@link EventBean}.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see Calendar
 * @see CalendarPermissions
 * @see EventBean
 *
 */
public interface CalendarBean {

	/**
	 * Loads a specific calendar, identified by it's ID.
	 * <p>
	 * If the user has no access to the calendar, {@code null} will be returned.
	 *
	 * @param calendarId - The {@link Long} identifying the {@link Calendar}.
	 * @return {@link Calendar} - The found calendar, {@code null} if nothing found or user hasn't rights to view this calendar.
	 * @throws PRPException
	 * @since 0.1
	 */
	Calendar loadCalendar(Long calendarId) throws PRPException;

	/**
	 * Loads all {@link Calendar}s for the logged in user. Including {@link Calendar}s with only read permissions.
	 *
	 * @return {@link List}&lt;{@link Calendar}&gt;
	 * @since 0.1
	 */
	List<Calendar> loadAllCalendars() throws PRPException;

	/**
	 * Creates a new {@link Calendar}.
	 * <p>
	 * It's possible to add events that equal another one. If you need to update calendars, consider using
	 * {@link #updateCalendar(Calendar)}.
	 *
	 * @param calendar - The {@link Calendar} to store.
	 * @return {@link Calendar}
	 * @since 0.1
	 */
	Calendar createCalendar(Calendar calendar) throws PRPException;

	/**
	 * Updates the {@link Calendar}'s information.
	 * <p>
	 * In order to be able to identify the {@link Calendar} to update, the ID has to be set.
	 *
	 * @param event - the {@link Calendar} to update.
	 * @return {@link Calendar} - The updated {@link Calendar}. If no entry was updated, <code>null</code> is returned.
	 * @throws PRPException
	 * @since 0.1
	 */
	Calendar updateCalendar(Calendar calendar) throws PRPException;

	/**
	 * Marks a {@link Calendar} as deleted. No physical deletion is done with this method.
	 *
	 * @param calendarId
	 */
	void deleteCalendar(Long calendarId) throws PRPException;

	/**
	 * Adds a {@link CalendarPermissions} for the given {@link User} to the given {@link Calendar}.
	 *
	 * @param level      - The {@link CalendarPermissions} access level to be set.
	 * @param calendarId - The {@link Calendar}'s ID to add the permission for.
	 * @param userId     - The {@link User}'s ID to set the permission for.
	 * @since 0.1
	 */
	void addCalendarPermission(CalendarPermissions level, Long calendarId, Long userId) throws PRPException;

	/**
	 * Updates the {@link CalendarPermissions} for the given {@link User} on the given {@link Calendar}.
	 *
	 * @param level      - The {@link CalendarPermissions} access level to be set.
	 * @param calendarId - The {@link Calendar}'s ID to add the permission for.
	 * @param userId     - The {@link User}'s ID to set the permission for.
	 * @since 0.1
	 */
	void updateCalendarPermission(CalendarPermissions level, Long calendarId, Long userId) throws PRPException;

	/**
	 * Removes the given {@link CalendarPermissions} access level for the given {@link User}.
	 *
	 * @param calendarId - The {@link Calendar}'s ID to remove the {@link CalendarPermissions} for.
	 * @param userId     . The {@link User}'s ID to remove the {@link CalendarPermissions} for.
	 * @since 0.1
	 */
	void removeCalendarPermission(Long calendarId, Long userId) throws PRPException;

}
