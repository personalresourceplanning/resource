/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.calendarservice.bean;

import java.util.Date;
import java.util.List;

import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.enums.calendar.CalendarPermissions;
import com.prp.resource.common.model.calendar.Event;

/**
 * Bean managing {@link Event} actions.
 * <p>
 * Calendar permissions have to be applied on this level, too.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see CalendarPermissions
 * @see Event
 * @see CalendarBean
 *
 */
public interface EventBean {

	/**
	 * Load all {@link Event}s for the given day and the logged in user. This method fetches all {@link Event}s from all calendars.
	 * <p>
	 * The list includes all {@link Event}s starting or ending at the specified day.
	 *
	 * @param day - A {@link Date} representing the day to fetch the {@link Event}s for.
	 * @return {@link List}&lt;{@link Event}&gt; - All {@link Event}s stored for the given day.
	 * @throws PRPException
	 * @since 0.1
	 */
	List<Event> loadEventsForDay(Date day) throws PRPException;

	/**
	 * Loads all {@link Event}s stored for the current user.
	 *
	 * @return {@link List}&lt;{@link Event}&gt; - All found events.
	 * @throws PRPException
	 */
	List<Event> loadEventsForCurrentUser() throws PRPException;

	/**
	 * Adds a new event.
	 * <p>
	 * It's possible to add to add events that equal another one. If you need to update events, consider using {@link #updateEvent(Event)}.
	 *
	 * @param event - The {@link Event} to store.
	 * @return The stored {@link Event}.
	 * @throws PRPException
	 * @since 0.1
	 */
	Event addEvent(Event event) throws PRPException;

	/**
	 * Updates the {@link Event}'s information.
	 * <p>
	 * In order to be able to identify the {@link Event} to update, the ID has to be set. It's currently not possible to move an {@link Event}
	 * from one {@link Calendar} to another like it's not possible to register another creator.
	 *
	 * @param event - the {@link Event} to update.
	 * @return {@link Event} - The updated {@link Event}.
	 * @throws PRPException
	 * @since 0.1
	 */
	Event updateEvent(Event event) throws PRPException;

	/**
	 * Marks an {@link Event} as deleted. No physical deletion is done with this method.
	 *
	 * @param eventId - The {@link Long} ID of the event to delete.
	 * @throws PRPException
	 * @since 0.1
	 */
	void deleteEvent(Long eventId) throws PRPException;

	/**
	 * Adds a reminder to an existing {@link Event}.
	 *
	 * @param eventId      - The {@link Long} ID of the {@link Event}.
	 * @param reminderTime - The {@link Date} to show the reminder.
	 * @return {@link Event} - The updated {@link Event}.
	 * @throws PRPException
	 * @since 0.1
	 */
	Event addReminder(Long eventId, Date reminderTime) throws PRPException;

	/**
	 * Removes the reminder at the given time. If no reminder is planned there, no one will be removed.
	 *
	 * @param eventId      - The {@link Long} ID of the event.
	 * @param reminderTime - The {@link Date} to delete the reminder at.
	 * @throws PRPException
	 * @since 0.1
	 */
	void deleteReminder(Long eventId, Date reminderTime) throws PRPException;

}
