/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.exception.api.pojo;

import java.util.Date;

import com.prp.resource.common.enums.exception.PRPErrorCode;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Setter;

/**
 * POJO used for responding to exceptions in the API
 *
 * Provides information to consumer about the Exception that occured.
 *
 * @author Robin Herder
 * @since 0.1
 * @see com.prp.resource.exception.api.GlobalExceptionController
 */

@Data
@Builder
@AllArgsConstructor
public class ApiExceptionResponse {

	@Builder.Default
	private Date timestamp = new Date();

	@Schema(example = "500", description = "The HTTP status code for this exception.")
	private int status;

	@Schema(example = "XY has to be set.")
	private String error;

	@Schema(example = "")
	private String message;

	@Schema(example = "/api/0.1/foo")
	private String path;

	@Schema(
			example = "B_E_B_PM",
			description = "A shortcode of the occured error. See the ErrorCode documentation for the corresponding service for details.")
	@Setter(value = AccessLevel.NONE)
	private String internalErrorCode;

	public void setInternalErrorCode(PRPErrorCode code) {
		internalErrorCode = code.getCode();
	}

	public static class ApiExceptionResponseBuilder {

		private String internalErrorCode;

		public ApiExceptionResponseBuilder internalErrorCode(PRPErrorCode code) {
			internalErrorCode = code.getCode();
			return this;
		}

	}

}
