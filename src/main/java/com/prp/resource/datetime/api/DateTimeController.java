/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.datetime.api;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.prp.resource.common.exception.PRPException;
import com.prp.resource.datetime.bean.DateTimeBean;
import org.springframework.web.bind.annotation.RestController;

// TODO build API

@RestController
@RequestMapping("/datetime")
public class DateTimeController {

	private DateTimeBean dateTimeBean;

	public DateTimeController(final DateTimeBean dateTimeBean) {
		this.dateTimeBean = dateTimeBean;
	}

	@GetMapping("/current")
	public ResponseEntity<Date> getUsersTime() throws PRPException {
		return new ResponseEntity<>(dateTimeBean.getUsersTime(), HttpStatus.OK);
	}

}
