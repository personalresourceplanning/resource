/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.taskservice.api.converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.prp.resource.common.model.task.Task;
import com.prp.resource.common.model.task.TaskList;
import com.prp.resource.taskservice.api.pojo.ApiTask;
import com.prp.resource.taskservice.api.pojo.ApiTaskList;

/**
 * Converts the JSON representations of the endpoints into work objects for the
 * beans or vice versa.
 *
 * @author Eric Fischer
 * @since 0.1
 *
 */
@Component
public final class ApiTaskConverter {

	private static final String M = "M";
	private static final String H = "H";
	private static final String PT = "PT";
	private static final SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat(
			ApiTask.DATE_FORMAT);

	/**
	 * Converts the {@link TaskList} pojo to the JSON representation to send to
	 * clients.
	 *
	 * @param in
	 *            The {@link TaskList} to convert.
	 * @return The converted {@link ApiTaskList}.
	 */
	public ApiTaskList convertTaskList(final TaskList in) {
		ApiTaskList out = new ApiTaskList();
		out.setId(in.getId());
		out.setTitle(in.getTitle());
		for (Task task : in.getTasks()) {
			out.getTasks().add(task.getId());
		}
		return out;
	}

	/**
	 * Converts the send JSON to the {@link TaskList} pojo.
	 * <p>
	 * <strong>Warning</strong> Does not convert the contained tasks because of
	 * missing information.
	 *
	 * @param in
	 *            - The {@link ApiTaskList} to convert.
	 * @return {@link TaskList} - The converted {@link TaskList}.
	 */
	public TaskList convertTaskList(final ApiTaskList in) {
		TaskList out = new TaskList();
		out.setId(in.getId());
		out.setTitle(in.getTitle());
		return out;
	}

	public ApiTask convertTask(final Task in) {
		ApiTask out = new ApiTask();
		out.setId(in.getId());
		out.setTitle(in.getTitle());
		out.setDescription(in.getDescription());
		if (in.getStartDate() != null) {
			out.setStartDate(DEFAULT_DATE_FORMAT.format(in.getStartDate()));
		}
		if (in.getDueDate() != null) {
			out.setDueDate(DEFAULT_DATE_FORMAT.format(in.getDueDate()));
		}
		if (in.getDuration() != null) {
			out.setDuration(in.getDuration().toString());
		}
		out.setDone(in.getDone());
		out.setTaskListId(in.getTaskListId());
		for (Date reminder : in.getReminders()) {
			out.getReminders().add(DEFAULT_DATE_FORMAT.format(reminder));
		}
		for (Task predecessor : in.getPredecessor()) {
			out.getPredecessor().add(predecessor.getId());
		}
		for (TaskList subList : in.getSubLists()) {
			out.getSublists().add(subList.getId());
		}
		return out;
	}

	public Task convertTask(final ApiTask in) throws ParseException {
		Task out = new Task();
		out.setTitle(in.getTitle());
		out.setDescription(in.getDescription());
		if (StringUtils.isNotEmpty(in.getDueDate()) && StringUtils.isNotBlank(in.getDueDate())) {
			out.setDueDate(toDate(in.getDueDate()));
		}
		if (StringUtils.isNotEmpty(in.getDuration()) && StringUtils.isNotBlank(in.getDuration())) {
			out.setDuration(toDuration(in));
		}
		if (StringUtils.isNotEmpty(in.getStartDate())
				&& StringUtils.isNotBlank(in.getStartDate())) {
			out.setStartDate(toDate(in.getStartDate()));
		}
		out.setId(in.getId());
		out.setDone(in.isDone());
		out.setTaskListId(in.getTaskListId());
		for (String reminder : in.getReminders()) {
			out.getReminders().add(toDate(reminder));
		}
		for (Long predecessor : in.getPredecessor()) {
			out.getPredecessor().add(new Task(predecessor));
		}
		for (Long subList : in.getSublists()) {
			out.getSubLists().add(new TaskList(subList));
		}
		return out;
	}

	private Duration toDuration(final ApiTask in) {
		String duration = in.getDuration().trim();
		String[] elements = duration.split(":");
		StringBuilder sb = new StringBuilder();
		sb.append(PT);
		sb.append(elements[0]);
		sb.append(H);
		sb.append(elements[1]);
		sb.append(M);
		return Duration.parse(sb.toString());
	}

	public Date toDate(final String dateString) throws ParseException {
		return DEFAULT_DATE_FORMAT.parse(dateString.trim());
	}

}
