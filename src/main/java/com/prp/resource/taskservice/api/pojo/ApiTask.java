/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.taskservice.api.pojo;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiTask {

	public static final String DATE_FORMAT = "dd-MM-yyyy HH:mm";

	@Schema(example = "1", description = "The generated ID and main identifier.")
	private Long id;

	@Schema(
			example = "Step 1: Delevop system",
			description = "The title. Not an identifier, two different tasks may have the same title.")
	private String title;

	@Schema(example = "Developing the system with Spring Boot.")
	private String description;

	@Schema(example = "11.11.2020 07:30", description = "The due date in the format " + DATE_FORMAT)
	private String dueDate;

	@Schema(
			example = "10.11.2020 15:00",
			description = "The date, when the user starts working on the task in the format "
					+ DATE_FORMAT)
	private String startDate;

	@Schema(
			example = "2:00",
			description = "The estimated duration of the task in the format hh:mm")
	private String duration;

	@Schema(example = "false")
	private boolean done;

	@Schema(
			example = "2",
			description = "The ID of the TaskList this Task belongs to.")
	private Long taskListId;

	@Schema(
			example = "[11.11.2020 07:30, 10.11.2020 14:45]",
			description = "A list of dates when the user should receive a reminder for the task.")
	private List<String> reminders = new ArrayList<>();

	@Schema(
			example = "[5, 312, 56]",
			description = "An array with IDs of tasks, that have to be done before this task.")
	private List<Long> predecessor = new ArrayList<>();

	@Schema(
			example = "[3, 15]",
			description = "An array with IDs of task lists that contain sub-tasks.")
	private List<Long> sublists = new ArrayList<>();

}
