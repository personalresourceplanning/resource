/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.taskservice.dal.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.ToString;

/**
 * Representing the tasks.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see TaskListDB
 * @see TaskRelationDB
 * @see TaskReminderDB
 *
 */
@Entity
@Table(name = "lia_backend_taskservice_task")
@Data
public class TaskDB {

	@Id
	@GeneratedValue
	@Setter(AccessLevel.NONE)
	private Long id;
	@Column(name = "creator", nullable = false, unique = false)
	private Long creatorId;
	@Column(name = "title", nullable = false, unique = false)
	private String title;
	@Column(name = "description", nullable = true, unique = false)
	private String description;
	@Column(name = "duedate", nullable = true, unique = false)
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date dueDate;
	@Column(name = "eventstart", nullable = true, unique = false)
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date startDate;
	@Column(name = "duration", nullable = true, unique = false)
	@Temporal(value = TemporalType.TIME)
	private Date duration;
	@Column(name = "deleted", nullable = false)
	private Boolean deleted = Boolean.FALSE;
	@Column(name = "done", nullable = false)
	private Boolean done = Boolean.FALSE;
	@ManyToOne
	@JoinColumn(name = "tasklist", nullable = false)
	private TaskListDB taskList;
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	@OneToMany(cascade = { CascadeType.REMOVE })
	private List<TaskListDB> subLists = new ArrayList<>();
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	@OneToMany(mappedBy = "taskFrom", cascade = { CascadeType.REMOVE, CascadeType.REFRESH })
	private List<TaskRelationDB> fromRelations = new ArrayList<>();
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	@OneToMany(mappedBy = "taskTo", cascade = { CascadeType.REMOVE, CascadeType.REFRESH })
	private List<TaskRelationDB> toRelations = new ArrayList<>();
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	@OneToMany(cascade = { CascadeType.ALL })
	private List<TaskReminderDB> reminders = new ArrayList<>();

}
