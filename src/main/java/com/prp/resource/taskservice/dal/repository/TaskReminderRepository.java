/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.taskservice.dal.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.prp.resource.taskservice.dal.model.TaskDB;
import com.prp.resource.taskservice.dal.model.TaskReminderDB;

/**
 * Repository containing the {@link TaskReminderDB}. Reminders belong to a specific {@link TaskDB}.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see TaskDB
 * @see TaskReminderDB
 *
 */
public interface TaskReminderRepository extends JpaRepository<TaskReminderDB, Long> {

	List<TaskReminderDB> findByTask(TaskDB task);

	Optional<TaskReminderDB> findByTaskAndRemindTime(TaskDB task, Date remindTime);

}
