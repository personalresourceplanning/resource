/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.taskservice.dal.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.prp.resource.common.enums.exception.PRPErrorCode;
import com.prp.resource.common.exception.PRPException;
import com.prp.resource.taskservice.dal.TaskListDAL;
import com.prp.resource.taskservice.dal.model.TaskDB;
import com.prp.resource.taskservice.dal.model.TaskListDB;
import com.prp.resource.taskservice.dal.queries.TaskDALQueries;
import com.prp.resource.taskservice.dal.repository.TaskListRepository;
import com.prp.resource.taskservice.dal.repository.TaskRepository;

/**
 *
 * @author Eric Fischer
 * @since 0.1
 * @see TaskListDAL
 * @see TaskListDB
 * @see TaskListRepository
 * @see TaskDB
 * @see TaskRepository
 *
 */
@Component
public class TaskListDALImpl implements TaskListDAL {

	private static final Logger log = LoggerFactory.getLogger(TaskListDALImpl.class);

	private TaskListRepository taskListRepository;
	private TaskRepository taskRepository;
	private TaskDALQueries q;

	@Autowired
	public TaskListDALImpl(final TaskListRepository taskListRepository,
			final TaskRepository taskRepository, final TaskDALQueries q) {
		this.taskListRepository = taskListRepository;
		this.taskRepository = taskRepository;
		this.q = q;
	}

	@Override
	public TaskListDB storeTaskList(final TaskListDB taskList) throws PRPException {
		checkInput(taskList);

		checkSubList(taskList);

		TaskListDB stored = taskListRepository.save(taskList);

		return stored;
	}

	@Override
	public TaskListDB updateTaskList(final TaskListDB taskList) throws PRPException {
		checkInput(taskList);

		Optional<TaskListDB> found = taskListRepository.findById(taskList.getId());

		if (!found.isPresent()) {
			String message = "No task list with that ID stored.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_DAL_TASK_LIST_NOT_EXISTANT);
		}

		TaskDB parentTask = checkSubList(taskList);

		TaskListDB taskListDB = found.get();
		taskListDB.setOwnerId(taskList.getOwnerId());
		taskListDB.setParentTask(parentTask);
		taskListDB.setTitle(taskList.getTitle());

		TaskListDB stored = taskListRepository.save(taskListDB);

		loadTasks(stored);

		return stored;
	}

	@Override
	public TaskListDB loadTasklist(final Long taskListId) throws PRPException {
		if (taskListId == null) {
			String message = "Task list ID missing.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_DAL_PARAMETERS_MISSING);
		}
		Optional<TaskListDB> found = taskListRepository.findById(taskListId);

		if (!found.isPresent()) {
			String message = "No task list with that ID stored.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_DAL_TASK_LIST_NOT_EXISTANT);
		}

		TaskListDB taskList = found.get();

		loadTasks(taskList);

		return taskList;
	}

	@Override
	public void deleteTaskList(final Long taskListID) throws PRPException {
		if (taskListID == null) {
			String message = "Task list ID missing.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_DAL_PARAMETERS_MISSING);
		}

		Optional<TaskListDB> found = taskListRepository.findById(taskListID);

		if (!found.isPresent()) {
			String message = "No task list with that ID stored.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_DAL_TASK_LIST_NOT_EXISTANT);
		}

		TaskListDB toDelete = found.get();
		toDelete.setDeleted(Boolean.TRUE);

		taskListRepository.save(toDelete);
	}

	@Override
	public List<TaskListDB> loadListsForUser(final Long userId) throws PRPException {
		if (userId == null) {
			String message = "User ID missing.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_DAL_PARAMETERS_MISSING);
		}

		List<TaskListDB> taskListsForUser = taskListRepository.findByOwnerIdAndDeleted(userId,
				Boolean.FALSE);

		if (taskListsForUser.isEmpty()) {
			log.warn("No task lists found for user.");
		}

		List<TaskListDB> retVal = taskListsForUser.stream().filter(p -> p.getParentTask() == null)
				.collect(Collectors.toList());

		for (TaskListDB taskListDB : retVal) {
			loadTasks(taskListDB);
		}

		return retVal;
	}

	@Override
	public List<TaskListDB> loadSubLists(final Long taskId) throws PRPException {
		if (taskId == null) {
			String message = "Task ID missing.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_DAL_PARAMETERS_MISSING);
		}

		Optional<TaskDB> foundTask = taskRepository.findById(taskId);

		if (!foundTask.isPresent()) {
			String message = "No task with that ID stored.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_DAL_TASK_NOT_EXISTANT);
		}

		List<TaskListDB> foundLists = taskListRepository.findByParentTaskAndDeleted(foundTask.get(),
				Boolean.FALSE);
		for (TaskListDB taskListDB : foundLists) {
			loadTasks(taskListDB);
		}

		if (foundLists.isEmpty()) {
			log.warn("No sublists found.");
		}

		return foundLists;
	}

	private void checkInput(final TaskListDB taskList) throws PRPException {
		if (taskList == null || taskList.getOwnerId() == null) {
			String message = "Task list or owner not set.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_DAL_PARAMETERS_MISSING);
		}
	}

	private void loadTasks(final TaskListDB taskList) {
		List<TaskDB> tasks = taskRepository
				.findAll(Specification.where(q.belongsTo(taskList)).and(q.notDeleted()));

		if (tasks.isEmpty()) {
			log.warn("Loaded list is empty.");
		}

		for (TaskDB taskDB : tasks) {
			for (TaskListDB subList : taskDB.getSubLists()) {
				loadTasks(subList);
			}
		}

		taskList.setTasks(tasks);
	}

	private TaskDB checkSubList(final TaskListDB taskList) {
		TaskDB parentTask = taskList.getParentTask();
		if (parentTask != null) {
			Optional<TaskDB> foundTask = taskRepository.findById(parentTask.getId());
			if (!foundTask.isPresent()) {
				log.warn(
						"The desired parent task doesn't exist. Assuming this shouldn't be a sub list.");
				taskList.setParentTask(null);
			}
		}
		return parentTask;
	}

}
