/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.taskservice.dal;

import java.util.List;

import com.prp.resource.common.exception.PRPException;
import com.prp.resource.taskservice.dal.model.TaskDB;
import com.prp.resource.taskservice.dal.model.TaskListDB;

/**
 * Database access layer responsible for {@link TaskListDB} operations.
 *
 * @author Eric Fischer
 * @since 0.1
 *
 */
public interface TaskListDAL {

	/**
	 * Persists the given {@link TaskListDB}.
	 *
	 * @param taskList - The {@link TaskListDB} to store.
	 * @return The stored {@link TaskListDB}.
	 * @throws PRPException
	 */
	TaskListDB storeTaskList(TaskListDB taskList) throws PRPException;

	/**
	 * Updates the given {@link TaskListDB}.
	 *
	 * @param taskList - The {@link TaskListDB} to store.
	 * @return The stored {@link TaskListDB}.
	 * @throws PRPException
	 */
	TaskListDB updateTaskList(TaskListDB taskList) throws PRPException;

	/**
	 * Marks the {@link TaskListDB} corresponding to the given ID as deleted.
	 *
	 * @param taskListID - the {@link Long} identifying the {@link TaskListDB} to delete.
	 * @throws PRPException
	 */
	void deleteTaskList(Long taskListID) throws PRPException;

	/**
	 * Loads the {@link TaskListDB} specified by the given ID.
	 * <p>
	 * The contained {@link TaskDB}s are loaded within the {@link TaskListDB}.
	 *
	 * @param taskListId - The {@link Long} identifying the resource to load.
	 * @return {@link TaskListDB} - The found list or <code>null</code> if none found.
	 * @throws PRPException
	 */
	TaskListDB loadTasklist(Long taskListId) throws PRPException;

	/**
	 * Loads all lists and the contained tasks for the given user ID.
	 * <p>
	 * Sub-lists will be ignored.
	 *
	 * @param userId - The {@link Long} identifying the user to load the {@link TaskListDB}s.
	 * @return {@link List}&lt;{@link TaskListDB}&gt;
	 * @throws PRPException
	 */
	List<TaskListDB> loadListsForUser(Long userId) throws PRPException;

	/**
	 * Loads all sublists of the given task.
	 *
	 * @param taskId - The {@link Long} identifying the {@link TaskDB} to load the {@link TaskListDB} for.
	 * @return {@link List}&lt;{@link TaskListDB}&gt; - All found lists for the given task.
	 * @throws PRPException
	 */
	List<TaskListDB> loadSubLists(Long taskId) throws PRPException;
}
