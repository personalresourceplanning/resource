/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.taskservice.dal;

import java.util.Date;
import java.util.List;

import com.prp.resource.common.exception.PRPException;
import com.prp.resource.taskservice.dal.model.TaskDB;
import com.prp.resource.taskservice.dal.model.TaskRelationDB;
import com.prp.resource.taskservice.dal.model.TaskReminderDB;

/**
 * Database access layer responsible for all {@link TaskDB} operations.
 *
 * @author Eric Fischer
 * @since 0.1
 *
 */
public interface TaskDAL {

	/**
	 * Loads all tasks planned for the given {@link Date}. The due date isn't relevant.
	 *
	 * @param day    - The {@link Date} to load the tasks for. The time information will be ignored as only the day information is relevant.
	 * @param userId - The user ID ({@link Long}) to search the tasks for.
	 * @return {@link List}&lt;{@link TaskDB}&gt; - The tasks that are planned on the given day.
	 * @throws PRPException
	 */
	List<TaskDB> loadTasksOnDay(Date day, Long userId) throws PRPException;

	/**
	 * Loads all tasks that are due on the given {@link Date}.
	 *
	 * @param day
	 * @param userId
	 * @return
	 * @throws PRPException
	 */
	List<TaskDB> loadTasksDueOnDay(Date day, Long userId) throws PRPException;

	List<TaskDB> loadTasksInRange(Date start, Date end, Long userId) throws PRPException;

	List<TaskDB> loadTasksDueInRange(Date start, Date end, Long userId) throws PRPException;

	/**
	 * Loads all tasks that are overdue to the given {@link Date}.
	 *
	 * @param day    - The {@link Date} to search the overdue tasks for. Tasks are overdue, if their due date is <strong>before</strong> the
	 *               given date.
	 * @param userId - The {@link Long} ID to search the tasks for.
	 * @return {@link List}&lt;{@link TaskDB}&gt; - The found overdue tasks.
	 * @throws PRPException
	 */
	List<TaskDB> loadOverdueTasks(Date day, Long userId) throws PRPException;

	/**
	 * Loads all tasks, that don't have a due date or start date.
	 *
	 * @param userId - The {@link Long} representing the user's ID to search the tasks for.
	 * @return {@link List}&lt;{@link TaskDB}&gt; - The found tasks.
	 * @throws PRPException
	 */
	List<TaskDB> loadUnplannedTasks(Long userId) throws PRPException;

	/**
	 * Loads a single {@link TaskDB} identified by it's ID.
	 *
	 * @param id - The {@link Long} identifying the resource.
	 * @return {@link TaskDB} - The found task.
	 * @throws PRPException
	 */
	TaskDB loadTask(Long id) throws PRPException;

	/**
	 * Persists the given task to the database.
	 *
	 * @param task - The {@link TaskDB} to store.
	 * @return {@link TaskDB} - The stored task.
	 * @throws PRPException
	 */
	TaskDB storeTask(TaskDB task) throws PRPException;

	/**
	 * Updates a {@link TaskDB}'s information. The task has to exist.
	 *
	 * @param task - The {@link TaskDB} information to update. The {@link TaskDB#getId(Long)} has to be set.
	 * @return {@link TaskDB} - The updated task.
	 * @throws PRPException
	 */
	TaskDB updateTask(TaskDB task) throws PRPException;

	/**
	 * Marks a task as deleted.
	 * <p>
	 * No physical deletion with this method!
	 *
	 * @param taskId - The {@link Long} identifying the resource to delete.
	 * @throws PRPException
	 */
	void deleteTask(Long taskId) throws PRPException;

	/**
	 * Stores a {@link TaskReminderDB}.
	 *
	 * @param reminder - The {@link TaskReminderDB} to store. The {@link TaskDB} has to be set.
	 * @return
	 * @throws PRPException
	 */
	TaskReminderDB storeReminder(TaskReminderDB reminder) throws PRPException;

	/**
	 * Deletes the {@link TaskReminderDB} identified with the given ID.
	 *
	 * @param reminderId - The {@link Long} identifying the {@link TaskReminderDB} to delete.
	 * @throws PRPException
	 */
	void deleteReminder(Long reminderId) throws PRPException;

	/**
	 * Stores a {@link TaskRelationDB} to the database.
	 *
	 * @param relation - The {@link TaskRelationDB} to store. The {@link TaskDB} has to be set with the right ID.
	 * @return {@link TaskRelationDB} - The stored relation.
	 * @throws PRPException
	 */
	TaskRelationDB storeRelation(TaskRelationDB relation) throws PRPException;

	/**
	 * Deletes a {@link TaskRelationDB} identified by it's ID.
	 *
	 * @param relationId - The {@link Long} identifying the resource.
	 * @throws PRPException
	 */
	void deleteRelation(Long relationId) throws PRPException;

}
