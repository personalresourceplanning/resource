/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.taskservice.dal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prp.resource.taskservice.dal.model.TaskDB;
import com.prp.resource.taskservice.dal.model.TaskListDB;
import com.prp.resource.taskservice.dal.queries.TaskDALQueries;

/**
 * Repository containing the {@link TaskDB}s. Queries can be extended with {@link TaskDALQueries}.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see TaskDB
 *
 */
public interface TaskRepository extends JpaRepository<TaskDB, Long>, JpaSpecificationExecutor<TaskDB> {

	List<TaskDB> findByTaskList(TaskListDB taskList);

}
