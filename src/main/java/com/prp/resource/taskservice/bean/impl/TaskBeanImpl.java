/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.taskservice.bean.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import com.prp.resource.common.model.task.TaskList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prp.resource.common.enums.exception.PRPErrorCode;
import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.model.task.Task;
import com.prp.resource.common.service.UserService;
import com.prp.resource.taskservice.bean.TaskBean;
import com.prp.resource.taskservice.bean.util.TaskConverter;
import com.prp.resource.taskservice.dal.TaskDAL;
import com.prp.resource.taskservice.dal.TaskListDAL;
import com.prp.resource.taskservice.dal.model.TaskDB;
import com.prp.resource.taskservice.dal.model.TaskListDB;
import com.prp.resource.taskservice.dal.model.TaskRelationDB;
import com.prp.resource.taskservice.dal.model.TaskReminderDB;

@Component
public class TaskBeanImpl implements TaskBean {

	private static final Logger log = LoggerFactory.getLogger(TaskBeanImpl.class);

	private TaskDAL dal;
	private TaskListDAL listDal;
	private TaskConverter converter;
	private UserService userService;

	@Autowired
	public TaskBeanImpl(final TaskDAL dal, final TaskListDAL listDal, final UserService userService,
			final TaskConverter converter) {
		this.dal = dal;
		this.listDal = listDal;
		this.converter = converter;
		this.userService = userService;
	}

	@Override
	public Task addTask(final Task task, final Long listId) throws PRPException {
		if (task == null || listId == null) {
			String message = "Task or list ID missing.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_PARAMETERS_MISSING);
		}

		if (task.getDueDate() != null && task.getStartDate() != null
				&& task.getStartDate().after(task.getDueDate())) {
			String message = "The task starts after it's due date.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_TASK_STARTS_OVERDUE);
		}

		TaskListDB taskList = listDal.loadTasklist(listId);

		if (taskList == null) {
			String message = "Task list not existant.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_TASK_LIST_NOT_EXISTANT);
		}

		Long userId = userService.loadCurrentUser().getId();
		if (!taskList.getOwnerId().equals(userId)) {
			String message = "Not allowed to add a task in this list.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_BEAN_WRONG_USER_ID);
		}

		TaskDB taskDb = new TaskDB();
		taskDb.setTitle(task.getTitle());
		taskDb.setDescription(task.getDescription());
		taskDb.setStartDate(task.getStartDate());
		if (task.getDuration() != null) {
			taskDb.setDuration(new Date(task.getDuration().toMillis()));
		}
		taskDb.setDueDate(task.getDueDate());
		taskDb.setTaskList(taskList);
		taskDb.setCreatorId(userId);
		for (Date reminder : task.getReminders()) {
			TaskReminderDB reminderDb = new TaskReminderDB();
			reminderDb.setTask(taskDb);
			reminderDb.setRemindTime(reminder);
			taskDb.getReminders().add(reminderDb);
		}

		TaskDB storedTask = dal.storeTask(taskDb);

		if (taskList.getParentTask() != null) {
			addPredecessor(taskList.getParentTask().getId(), storedTask.getId());
		}

		return converter.convertTask(storedTask);
	}

	@Override
	public Task addSubTask(final Task subTask, final Long parentTaskId) throws PRPException {

		if (subTask == null || parentTaskId == null) {
			String message = "Subtask or parent Task ID missing.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_PARAMETERS_MISSING);
		}

		TaskDB parentTask = dal.loadTask(parentTaskId);
		if(parentTask == null) {
			String message = "No parent Task exists for given id " + parentTaskId + ".";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_PARAMETERS_MISSING);
		}

		List<TaskListDB> subLists = listDal.loadSubLists(parentTaskId);
		List<TaskListDB> foundList = subLists.stream().filter(list -> list.getTitle() == null).collect(Collectors.toList());

		TaskListDB storedDefaultList;

		if(foundList.isEmpty()) {
			TaskListDB defaultList = new TaskListDB();
			defaultList.setTitle(null);
			defaultList.setParentTask(parentTask);
			defaultList.setOwnerId(userService.loadCurrentUser().getId());
			storedDefaultList = listDal.storeTaskList(defaultList);
		} else {
			storedDefaultList = foundList.get(0);
		}

		if(!parentTask.getSubLists().contains(storedDefaultList)) {
			parentTask.getSubLists().add(storedDefaultList);
			updateTask(converter.convertTask(parentTask));
		}

		return addTask(subTask, storedDefaultList.getId());
	}

	@Override
	public Task updateTask(final Task task) throws PRPException {
		if (task == null || task.getId() == null) {
			String message = "Task or task ID missing.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_PARAMETERS_MISSING);
		}

		TaskDB loaded = dal.loadTask(task.getId());

		if (loaded == null) {
			String message = "No task for the given ID found.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_TASK_NOT_EXISTANT);
		}

		if (!loaded.getCreatorId().equals(userService.loadCurrentUser().getId())) {
			String message = "Not allowed to change this task.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_BEAN_WRONG_USER_ID);
		}

		loaded.setTitle(task.getTitle());
		loaded.setDescription(task.getDescription());
		loaded.setDueDate(task.getDueDate());
		loaded.setStartDate(task.getStartDate());
		loaded.setDone(task.getDone());
		if(task.getDuration() != null) {
			loaded.setDuration(new Date(task.getDuration().toMillis()));
		}
		List<TaskReminderDB> toAdd = new ArrayList<>();
		for (Date reminder : task.getReminders()) {
			List<TaskReminderDB> found = loaded.getReminders().stream()
					.filter(p -> p.getRemindTime().equals(reminder)).collect(Collectors.toList());
			if (!found.isEmpty()) {
				toAdd.addAll(found);
			} else {
				TaskReminderDB reminderDb = new TaskReminderDB();
				reminderDb.setRemindTime(reminder);
				reminderDb.setTask(loaded);
				toAdd.add(reminderDb);
			}
		}
		loaded.getReminders().clear();
		loaded.getReminders().addAll(toAdd);

		TaskDB updated = dal.updateTask(loaded);

		return converter.convertTask(updated);
	}

	@Override
	public Task markAsDone(final Long taskId) throws PRPException {
		return done(taskId, Boolean.TRUE);
	}

	@Override
	public Task markAsUndone(final Long taskId) throws PRPException {
		return done(taskId, Boolean.FALSE);
	}

	private Task done(final Long taskId, final Boolean value) throws PRPException {
		if (taskId == null) {
			String message = "Task ID missing.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_PARAMETERS_MISSING);
		}

		TaskDB loaded = dal.loadTask(taskId);

		if (loaded == null) {
			String message = "No task for the given ID found.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_TASK_NOT_EXISTANT);
		}

		if (!loaded.getCreatorId().equals(userService.loadCurrentUser().getId())) {
			String message = "Not allowed to change this task.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_BEAN_WRONG_USER_ID);
		}

		loaded.setDone(value);
		return converter.convertTask(dal.updateTask(loaded));
	}

	@Override
	public void deleteTask(final Long taskId) throws PRPException {
		if (taskId == null) {
			String message = "Task ID missing.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_PARAMETERS_MISSING);
		}

		TaskDB task = dal.loadTask(taskId);

		if (task == null) {
			String message = "No task for the given ID found.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_TASK_NOT_EXISTANT);
		}

		if (!task.getCreatorId().equals(userService.loadCurrentUser().getId())) {
			String message = "Not allowed to delete this task.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_BEAN_WRONG_USER_ID);
		}

		dal.deleteTask(taskId);

	}

	@Override
	public void addPredecessor(final Long taskId, final Long predecessorId) throws PRPException {
		if (taskId == null || predecessorId == null) {
			String message = "Task ID missing.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_PARAMETERS_MISSING);
		}

		TaskDB loaded = dal.loadTask(taskId);
		TaskDB predecessor = dal.loadTask(predecessorId);

		if (loaded == null || predecessor == null) {
			String message = "No tasks for the given IDs found.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_TASK_NOT_EXISTANT);
		}

		if (predecessor.getDueDate() != null && predecessor.getStartDate() != null
				&& !predecessor.getDueDate().before(loaded.getDueDate())) {
			String message = "Predecessor's due date not before task's due date.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_PREDECESSOR_NOT_BEFORE_TASK);
		}

		TaskRelationDB relation = new TaskRelationDB();
		relation.setTaskFrom(predecessor);
		relation.setTaskTo(loaded);
		relation.setRelation(TaskRelationDB.BEFORE);

		dal.storeRelation(relation);
	}

	@Override
	public List<Task> loadTasksOnDay(final Date day, final boolean includeOverdue)
			throws PRPException {
		if (day == null) {
			String message = "Day missing.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_PARAMETERS_MISSING);
		}

		Long userId = userService.loadCurrentUser().getId();
		List<TaskDB> loaded = dal.loadTasksOnDay(day, userId);
		loaded.addAll(dal.loadTasksDueOnDay(day, userId));

		if (includeOverdue) {
			loaded.addAll(dal.loadOverdueTasks(day, userId));
		}

		List<Task> retVal = new LinkedList<>();

		for (TaskDB taskDB : loaded) {
			Task task = converter.convertTask(taskDB);
			List<TaskListDB> subLists = listDal.loadSubLists(task.getId());
			for (TaskListDB list : subLists) {
				task.getSubLists().add(converter.convertTaskList(list));
			}
			retVal.add(task);
		}

		return retVal;
	}

	@Override
	public Task loadTask(final Long taskId) throws PRPException {
		if (taskId == null) {
			String message = "Task ID missing.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_PARAMETERS_MISSING);
		}

		TaskDB loaded = dal.loadTask(taskId);

		if(!loaded.getCreatorId().equals(userService.loadCurrentUser().getId())) {
			String message = "User not authorized to access Tasks.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_BEAN_WRONG_USER_ID);
		}

		return converter.convertTask(loaded);
	}

	@Override
	public List<Task> loadTasksInWeek(final Date dateInWeek, final boolean startAtDate)
			throws PRPException {
		if (dateInWeek == null) {
			String message = "Date marker missing.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_PARAMETERS_MISSING);
		}

		// init calendar objects
		GregorianCalendar start = new GregorianCalendar();
		start.setTime(dateInWeek);
		GregorianCalendar end = new GregorianCalendar();
		end.setTime(dateInWeek);

		resetCalendarTime(start);
		resetCalendarTime(end);
		end.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		if (!startAtDate) {
			start.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		}

		Long userId = userService.loadCurrentUser().getId();

		List<TaskDB> result = dal.loadTasksInRange(start.getTime(), end.getTime(), userId);
		result.addAll(dal.loadTasksDueInRange(start.getTime(), end.getTime(), userId));

		List<Task> retVal = new LinkedList<>();

		for (TaskDB taskDB : result) {
			retVal.add(converter.convertTask(taskDB));
		}

		return retVal;
	}

	@Override
	public List<Task> loadUnplannedTasks() throws PRPException {
		List<TaskDB> tasks = dal.loadUnplannedTasks(userService.loadCurrentUser().getId());
		List<Task> retVal = new ArrayList<>();

		for (TaskDB task : tasks) {
			retVal.add(converter.convertTask(task));
		}

		return retVal;
	}

	private void resetCalendarTime(final Calendar cal) {
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
	}

}
