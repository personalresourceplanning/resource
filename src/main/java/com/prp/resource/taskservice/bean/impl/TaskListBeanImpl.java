/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.taskservice.bean.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prp.resource.common.enums.exception.PRPErrorCode;
import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.model.task.TaskList;
import com.prp.resource.common.service.UserService;
import com.prp.resource.taskservice.bean.TaskListBean;
import com.prp.resource.taskservice.bean.util.TaskConverter;
import com.prp.resource.taskservice.dal.TaskDAL;
import com.prp.resource.taskservice.dal.TaskListDAL;
import com.prp.resource.taskservice.dal.model.TaskDB;
import com.prp.resource.taskservice.dal.model.TaskListDB;

/**
 * @since 0.1
 * @author Eric Fischer
 * @see TaskList
 * @see TaskListDAL
 * @see UserService
 * @see TaskDAL
 * @see TaskConverter
 *
 */
@Component
public class TaskListBeanImpl implements TaskListBean {

	private static final Logger log = LoggerFactory.getLogger(TaskListBeanImpl.class);

	private UserService userService;
	private TaskListDAL dal;
	private TaskDAL taskDal;
	private TaskConverter taskConverter;

	@Autowired
	public TaskListBeanImpl(final UserService userService, final TaskListDAL dal,
			final TaskDAL taskDal, final TaskConverter taskConverter) {
		this.userService = userService;
		this.dal = dal;
		this.taskDal = taskDal;
		this.taskConverter = taskConverter;
	}

	@Override
	public TaskList createTaskList(final String title) throws PRPException {
		if (StringUtils.isEmpty(title)) {
			String message = "No task list name given to store.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_PARAMETERS_MISSING);
		}
		TaskListDB listDb = new TaskListDB();
		listDb.setTitle(title);
		listDb.setOwnerId(userService.loadCurrentUser().getId());

		TaskListDB stored = dal.storeTaskList(listDb);

		return taskConverter.convertTaskList(stored);
	}

	@Override
	public TaskList updateTaskList(final TaskList list) throws PRPException {
		if (list == null || list.getId() == null || list.getId().equals(Long.valueOf(0))) {
			String message = "No task list given or ID missing.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_PARAMETERS_MISSING);
		}

		TaskListDB db = dal.loadTasklist(list.getId());

		Long id = userService.loadCurrentUser().getId();
		if (!id.equals(db.getOwnerId())) {
			String msg = "Not allowed to change this task list.";
			log.error(msg);
			throw new PRPException(msg, PRPErrorCode.BACKEND_TASKSERVICE_BEAN_WRONG_USER_ID);
		}

		db.setTitle(list.getTitle());

		TaskListDB updated = dal.updateTaskList(db);

		return taskConverter.convertTaskList(updated);
	}

	@Override
	public TaskList addSubList(final Long taskId, final TaskList list) throws PRPException {
		if (list == null || taskId == null) {
			String message = "No task list given or task ID missing.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_PARAMETERS_MISSING);
		}

		TaskDB foundTask = taskDal.loadTask(taskId);
		if (foundTask == null) {
			String message = "The task to add the sublist for is not existant.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_TASK_NOT_EXISTANT);
		}

		TaskListDB toStore = new TaskListDB();
		toStore.setOwnerId(userService.loadCurrentUser().getId());
		toStore.setTitle(list.getTitle());
		toStore.setParentTask(foundTask);

		TaskListDB stored = dal.storeTaskList(toStore);

		return taskConverter.convertTaskList(stored);
	}

	@Override
	public void deleteTaskList(final Long listId) throws PRPException {
		Long userId = userService.loadCurrentUser().getId();
		TaskListDB taskList = dal.loadTasklist(listId);
		if (!taskList.getOwnerId().equals(userId)) {
			String msg = "Not allowed to delete this task list.";
			log.error(msg);
			throw new PRPException(msg, PRPErrorCode.BACKEND_TASKSERVICE_BEAN_WRONG_USER_ID);
		}
		dal.deleteTaskList(listId);
	}

	@Override
	public List<TaskList> loadListsForUser() throws PRPException {
		List<TaskListDB> lists = dal.loadListsForUser(userService.loadCurrentUser().getId());

		List<TaskList> retVal = new ArrayList<>();

		for (TaskListDB db : lists) {
			TaskList list = new TaskList();
			list.setTitle(db.getTitle());
			list.setId(db.getId());
			for (TaskDB task : db.getTasks()) {
				list.getTasks().add(taskConverter.convertTask(task));
			}
			retVal.add(list);
		}

		return retVal;
	}

	@Override
	public List<TaskList> loadSubLists(final Long taskId) throws PRPException {

		if (taskId == null) {
			String message = "Task ID missing.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_TASKSERVICE_BEAN_PARAMETERS_MISSING);
		}

		List<TaskListDB> subLists = dal.loadSubLists(taskId);
		Boolean subListsInitiallyEmpty = subLists.isEmpty();

		Long userId = userService.loadCurrentUser().getId();

		subLists = subLists.stream().filter(subList -> subList.getOwnerId().equals(userId)).collect(Collectors.toList());

		if(subLists.isEmpty() && !subListsInitiallyEmpty) {
			String message = "User not authorized to access any of the Sub Lists.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_BEAN_WRONG_USER_ID);
		}

		List<TaskList> retVal = new ArrayList<>();
		for (TaskListDB subList : subLists) {
			retVal.add(taskConverter.convertTaskList(subList));
		}
		return retVal;
	}

}
