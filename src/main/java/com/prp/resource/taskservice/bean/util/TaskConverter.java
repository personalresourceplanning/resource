/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.taskservice.bean.util;

import java.time.Duration;

import org.springframework.stereotype.Component;

import com.prp.resource.common.model.task.Task;
import com.prp.resource.common.model.task.TaskList;
import com.prp.resource.taskservice.dal.model.TaskDB;
import com.prp.resource.taskservice.dal.model.TaskListDB;
import com.prp.resource.taskservice.dal.model.TaskRelationDB;
import com.prp.resource.taskservice.dal.model.TaskReminderDB;

/**
 * Converter for {@link Task}, {@link TaskDB}, {@link TaskList} and {@link TaskListDB}.
 *
 * @author Eric Fischer
 * @since 0.1
 *
 */
@Component
public final class TaskConverter {

	public Task convertTask(final TaskDB in) {
		Task out = new Task();
		out.setId(in.getId());
		out.setTitle(in.getTitle());
		out.setDescription(in.getDescription());
		out.setDone(in.getDone());
		out.setStartDate(in.getStartDate());
		out.setDueDate(in.getDueDate());
		out.setTaskListId(in.getTaskList().getId());
		if (in.getDuration() != null) {
			out.setDuration(Duration.ofMillis(in.getDuration().getTime()));
		}
		for (TaskReminderDB reminder : in.getReminders()) {
			out.getReminders().add(reminder.getRemindTime());
		}
		for (TaskRelationDB relation : in.getToRelations()) {
			if (TaskRelationDB.BEFORE.equals(relation.getRelation())) {
				out.getPredecessor().add(convertTask(relation.getTaskFrom()));
			}
		}
		for (TaskListDB subList : in.getSubLists()) {
			out.getSubLists().add(convertTaskList(subList));
		}
		return out;
	}

	public TaskList convertTaskList(final TaskListDB in) {
		TaskList out = new TaskList();
		out.setTitle(in.getTitle());
		out.setId(in.getId());
		for (TaskDB task : in.getTasks()) {
			out.getTasks().add(convertTask(task));
		}
		return out;
	}

}
