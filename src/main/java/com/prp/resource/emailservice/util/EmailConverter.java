/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.util;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.model.User;
import com.prp.resource.common.model.email.Appandix;
import com.prp.resource.common.model.email.Email;
import com.prp.resource.common.model.email.account.EmailAddress;
import com.prp.resource.emailservice.dal.model.AppandixDB;
import com.prp.resource.emailservice.dal.model.EmailAddressDB;
import com.prp.resource.emailservice.dal.model.EmailContentBlockDB;
import com.prp.resource.emailservice.dal.model.EmailDB;

/**
 * Converts emails between DB and API model.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see EmailDB
 * @see Email
 *
 */
@Component
public final class EmailConverter {

	private static final int MAX_BLOCK_SIZE = 255;

	private AccountConverter accountConverter;

	@Autowired
	public EmailConverter(final AccountConverter accountConverter) {
		this.accountConverter = accountConverter;
	}

	/**
	 * Converts the email information to the database model.
	 * <p>
	 * <strong>This method does not convert account information!</strong> This should be loaded from database in order to prevent security
	 * issues.
	 *
	 * @param in   - The {@link Email} to convert.
	 * @param user - The {@link User} that owns this email.
	 * @return {@link EmailDB} - The converted email.
	 */
	public EmailDB convertEmail(final Email in, final User user) {
		EmailDB out = new EmailDB();

		// Addresses
		for (EmailAddress address : in.getTo()) {
			EmailAddressDB db = EmailAddressConverter.convertEmailAddress(address);
			out.getTo().add(db);
		}
		for (EmailAddress address : in.getCc()) {
			EmailAddressDB db = EmailAddressConverter.convertEmailAddress(address);
			out.getCc().add(db);
		}
		for (EmailAddress address : in.getBcc()) {
			EmailAddressDB db = EmailAddressConverter.convertEmailAddress(address);
			out.getBcc().add(db);
		}

		out.getFrom().add(EmailAddressConverter.convertEmailAddress(in.getFrom()));

		// directly setable
		out.setSubject(in.getSubject());
		out.setSent(in.getSentDate());
		out.setReceived(in.getReceivedDate());

		// content
		int contentBlocksNumber = BigDecimal.valueOf(in.getMessage().length())
				.divide(BigDecimal.valueOf(MAX_BLOCK_SIZE), BigDecimal.ROUND_CEILING).intValue();
		for (int i = 0; i < contentBlocksNumber; i++) {
			EmailContentBlockDB contentBlock = new EmailContentBlockDB();
			contentBlock.setMail(out);
			int remainingCharacters = in.getMessage().length() - i * MAX_BLOCK_SIZE;
			if (remainingCharacters >= MAX_BLOCK_SIZE) {
				contentBlock.setContent(in.getMessage().substring(i * MAX_BLOCK_SIZE, (i + 1) * MAX_BLOCK_SIZE));
			} else {
				contentBlock.setContent(in.getMessage().substring(i * MAX_BLOCK_SIZE));
			}
			contentBlock.setOrderNumber(Long.valueOf(i + 1));
			out.getContent().add(contentBlock);
		}

		for (Appandix appandix : in.getAppandicies()) {
			AppandixDB convertedAppandix = AppandixConverter.convertAppandix(appandix);
			convertedAppandix.setMail(out);
			out.getAppandicies().add(convertedAppandix);
		}

		return out;
	}

	public Email convertEmail(final EmailDB in) throws PRPException {
		Email out = new Email();

		out.setId(in.getId());
		out.setSubject(in.getSubject());
		out.setSentDate(in.getSent());

		out.setMethod(in.getHostAccess().getHost().getMethod());

		StringBuilder sb = new StringBuilder();
		for (EmailContentBlockDB contentBlock : in.getContent()) {
			sb.append(contentBlock.getContent());
		}
		out.setMessage(sb.toString());

		for (EmailAddressDB address : in.getTo()) {
			out.getTo().add(EmailAddressConverter.convertEmailAddress(address));
		}
		for (EmailAddressDB address : in.getCc()) {
			out.getCc().add(EmailAddressConverter.convertEmailAddress(address));
		}
		for (EmailAddressDB address : in.getBcc()) {
			out.getBcc().add(EmailAddressConverter.convertEmailAddress(address));
		}

		out.setAccount(accountConverter.convertAccount(in.getHostAccess().getAccount()));

		// future TODO: refactor API model to transport multiple sender addresses
		// TODO fix size error

		if (!in.getFrom().isEmpty()) {
			EmailAddress outFrom = EmailAddressConverter.convertEmailAddress(in.getFrom().get(0));
			out.setFrom(outFrom);

		}

		for (AppandixDB appandix : in.getAppandicies()) {
			out.getAppandicies().add(AppandixConverter.convertAppandix(appandix));
		}

		return out;
	}

}
