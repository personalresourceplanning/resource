/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.util;

import java.math.BigDecimal;

import com.prp.resource.common.model.email.Appandix;
import com.prp.resource.emailservice.dal.model.AppandixDB;
import com.prp.resource.emailservice.dal.model.AppandixDataBlockDB;

/**
 * Converter for appendix models between API and database representation.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see AppandixDB
 * @see Appandix
 *
 */
public class AppandixConverter {

	private AppandixConverter() {}

	public static AppandixDB convertAppandix(final Appandix in) {
		AppandixDB out = new AppandixDB();

		out.setName(in.getTitle());
		out.setType(in.getType());

		int blocks = BigDecimal.valueOf(in.getPayload().length)
				.divide(BigDecimal.valueOf(255), BigDecimal.ROUND_CEILING).intValue();
		for (int i = 0; i < blocks; i++) {
			AppandixDataBlockDB block = new AppandixDataBlockDB();
			block.setOrderNumber(Integer.valueOf(i));
			int bytesIndex = 0; // needed to append the byte to the array
			if (in.getPayload().length - i * 255 > 255) {
				byte[] bytes = new byte[255];
				for (int b = i * 255; b < (i + 1) * 255; b++) {
					bytes[bytesIndex] = in.getPayload()[b];
					bytesIndex++;
				}
				block.setPayload(bytes);
			} else {
				byte[] bytes = new byte[in.getPayload().length - i * 255];
				for (int b = i * 255; b < in.getPayload().length; b++) {
					bytes[bytesIndex] = in.getPayload()[b];
					bytesIndex++;
				}
				block.setPayload(bytes);
			}
			block.setAppandix(out);
			out.getData().add(block);
		}

		return out;
	}

	public static Appandix convertAppandix(final AppandixDB in) {
		Appandix out = new Appandix();

		out.setTitle(in.getName());
		out.setType(in.getType());

		// calc size of byte array
		int blockCount = in.getData().size();
		int lastBlockSize = in.getData().get(in.getData().size() - 1).getPayload().length;
		int fileSize = (blockCount - 1) * 255 + lastBlockSize;

		byte[] payload = new byte[fileSize];
		int payloadIndex = 0;
		for (AppandixDataBlockDB dataBlock : in.getData()) {
			for (int i = 0; i < dataBlock.getPayload().length; i++) {
				payload[payloadIndex] = dataBlock.getPayload()[i];
				payloadIndex++;
			}
		}

		out.setPayload(payload);

		return out;
	}

}
