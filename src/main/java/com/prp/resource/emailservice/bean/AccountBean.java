/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.bean;

import java.util.List;

import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.model.email.account.Account;
import com.prp.resource.emailservice.dal.AccountDal;
import com.prp.resource.emailservice.dal.model.AccountDB;

/**
 * Bean for managing email accounts.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see Account
 * @see AccountDB
 * @see AccountDal
 *
 */
public interface AccountBean {

	/**
	 * Creates a new email account.
	 *
	 * @param account - The {@link Account} to be stored to the system. Remember to fill all fields.
	 * @return {@link Account} - The stored account.
	 * @throws PRPException
	 * @since 0.1
	 */
	Account registerAccount(Account account) throws PRPException;

	/**
	 * Returns all accounts for the current user. This method calls the authentification service to get the current user's ID.
	 *
	 * @return {@link List}<{@link Account}> - All accounts registered for this user. <code>Null</code>, if nothing found.
	 * @throws PRPException
	 * @since 0.1
	 */
	List<Account> findAccountsForCurrentUser() throws PRPException;

	/**
	 * Loads an {@link Account} identified by it's ID.
	 *
	 * @param id - The {@link Long} identifying the resource to load.
	 * @return {@link Account} - The found {@link Account} or {@code null} if nothing found.
	 * @throws PRPException
	 */
	Account loadAccount(Long id) throws PRPException;

	/**
	 * Updates {@link Account} information.
	 *
	 * @param account - The {@link Account} to update.
	 * @return {@link Account} - The updated {@link Account};
	 * @throws PRPException
	 */
	Account updateAccount(Account account) throws PRPException;

}
