/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.bean.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prp.resource.common.enums.exception.PRPErrorCode;
import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.enums.email.EmailRequestMethod;
import com.prp.resource.common.model.User;
import com.prp.resource.common.model.email.Appandix;
import com.prp.resource.common.model.email.Email;
import com.prp.resource.common.model.email.account.Account;
import com.prp.resource.common.model.email.account.EmailAddress;
import com.prp.resource.common.service.UserService;
import com.prp.resource.emailservice.bean.EmailBean;
import com.prp.resource.emailservice.dal.AccountDal;
import com.prp.resource.emailservice.dal.MailDal;
import com.prp.resource.emailservice.dal.model.AccountDB;
import com.prp.resource.emailservice.dal.model.EmailDB;
import com.prp.resource.emailservice.dal.model.HostAccessDB;
import com.prp.resource.emailservice.service.MailService;
import com.prp.resource.emailservice.util.AccountConverter;
import com.prp.resource.emailservice.util.EmailAddressConverter;
import com.prp.resource.emailservice.util.EmailConverter;
import com.sun.mail.util.BASE64DecoderStream;

/**
 *
 * @author Eric Fischer
 * @since 0.1
 * @see EmailBean
 *
 */
@Component
public class EmailBeanImpl implements EmailBean {

	private static final Logger log = LoggerFactory.getLogger(EmailBeanImpl.class);

	private MailService mailService;
	private MailDal mailDal;
	private UserService userService;
	private AccountDal accountDal;
	private AccountConverter accountConverter;
	private EmailConverter emailConverter;

	@Autowired
	public EmailBeanImpl(final MailService mailService, final MailDal mailDal, final UserService userService,
			final AccountDal accountDal, final AccountConverter accountConverter, final EmailConverter emailConverter) {
		this.mailService = mailService;
		this.mailDal = mailDal;
		this.userService = userService;
		this.accountDal = accountDal;
		this.accountConverter = accountConverter;
		this.emailConverter = emailConverter;
	}

	@Override
	public void sendMail(final Email email, final boolean storeToDb) throws PRPException {
		if (email == null) {
			String message = "Null email object handed to the bean to be sent.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_BEAN_NO_EMAIL_TO_SEND);
		}

		if (hostInformationMissing(email)) {
			String message = "Necessary information about host for sending missing.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_BEAN_ACCOUNT_CRIDENTIALS_NOT_COMPLETE);
		}

		if (storeToDb) {
			User user = userService.loadCurrentUser();

			EmailDB emailDB = emailConverter.convertEmail(email, user);
			AccountDB account = accountDal.loadAccount(email.getSendingAccount().getId());
			HostAccessDB hostAccessDB = account.getHosts().stream()
					.filter(p -> p.getHost().getMethod().equals(email.getMethod())).collect(Collectors.toList()).get(0);
			emailDB.setHostAccess(hostAccessDB);
			log.debug("Calling DAL to store email...");
			mailDal.storeEmail(emailDB);
			log.debug("Call to DAL finished successfuly.");

		}

		log.debug("Calling mail service to send mail...");
		mailService.sendMail(email);
		log.info("Mail sent.");
	}

	private boolean hostInformationMissing(final Email email) {
		// @formatter:off
		return
			email.getSendingAccount() == null ||
			email.getSendingAccount().getConnectionMethods() == null ||
			email.getSendingAccount().getConnectionMethods().get(EmailRequestMethod.SMTP) == null ||
			email.getSendingAccount().getConnectionMethods().get(EmailRequestMethod.SMTP).getHost() == null ||
			email.getSendingAccount().getConnectionMethods().get(EmailRequestMethod.SMTP).getHost().isEmpty() ||
			email.getSendingAccount().getConnectionMethods().get(EmailRequestMethod.SMTP).getUsername() == null ||
			email.getSendingAccount().getConnectionMethods().get(EmailRequestMethod.SMTP).getUsername().isEmpty() ||
			email.getSendingAccount().getConnectionMethods().get(EmailRequestMethod.SMTP).getPort() == null ||
			email.getSendingAccount().getConnectionMethods().get(EmailRequestMethod.SMTP).getPort().equals(Integer.valueOf(0));
		// @formatter:on
	}

	@Override
	public List<Email> fetchEmails(final Long accountId, final EmailRequestMethod method, final Integer count,
			final Integer start) throws PRPException {
		if (accountId == null || count == null || start == null || method == null) {
			String message = "Account information missing for fetching emails.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_BEAN_ACCOUNT_INFORMATION_NOT_COMPLETE);
		}

		User currentUser = userService.loadCurrentUser();
		AccountDB loadedAccount = accountDal.loadAccount(accountId);
		Account account = accountConverter.convertAccount(loadedAccount);

		List<Message> fetchedMessages = new ArrayList<>();

		// choose fetch method

		switch (method) {
		case POP3:
			fetchedMessages = mailService.fetchMailsPOP3(account);
			break;
		case IMAP:
			fetchedMessages = mailService.fetchMailsIMAP(account);
			break;
		default:
			String message = "The requestet fetching method isn't supported.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_BEAN_FETCHING_METHOD_NOT_SUPPORTED);
		}

		List<Email> emails = new ArrayList<>();

		int fetchLength = 0;
		if (fetchedMessages.size() < start) {
			log.warn("Start value above email count on server.");
			return new ArrayList<>();
		} else if (fetchedMessages.size() < start + count) {
			log.warn("Not as many mails on server as requested.");
			fetchLength = fetchedMessages.size();
		} else {
			fetchLength = start + count;
		}

		try {
			for (int i = start; i <= fetchLength - 1; i++) {

				// convert messages to API model

				Message message = fetchedMessages.get(i);
				Email converted = new Email();
				converted.setReceivingAccount(account);
				converted.setSubject(message.getSubject());
				converted.setSentDate(message.getSentDate());
				converted.setFrom(new EmailAddress(message.getFrom()[0].toString()));
				converted.setMethod(account.getPrimaryFetchingMethod());

				// check if mail is already stored to DB
				EmailDB found = mailDal.checkIfExistant(converted.getSubject(),
						EmailAddressConverter.convertEmailAddress(converted.getFrom()), converted.getSentDate());
				if (found != null) {
					emails.add(emailConverter.convertEmail(found));
					continue;
				}

				if (message.getRecipients(RecipientType.TO) != null) {
					for (Address address : Arrays.asList(message.getRecipients(RecipientType.TO))) {
						converted.getTo().add(new EmailAddress(address.toString()));
					}
				}
				if (message.getRecipients(RecipientType.CC) != null) {
					for (Address address : message.getRecipients(RecipientType.CC)) {
						converted.getCc().add(new EmailAddress(address.toString()));
					}
				}
				if (message.getRecipients(RecipientType.BCC) != null) {
					for (Address address : message.getRecipients(RecipientType.BCC)) {
						converted.getBcc().add(new EmailAddress(address.toString()));
					}
				}
				try {
					destructMessageContent(message.getContent(), converted);
				} catch (IOException e) {
					String message2 = "Error while reading mail content.";
					log.error(message2);
					throw new PRPException(message2, e,
							PRPErrorCode.BACKEND_EMAILSERVICE_BEAN_MAIL_CONTENT_NOT_READABLE);
				}
				EmailDB toStore = emailConverter.convertEmail(converted, currentUser);
				Long hostId = loadedAccount.getHosts().stream()
						.filter(p -> p.getHost().getMethod().equals(account.getPrimaryFetchingMethod()))
						.collect(Collectors.toList()).get(0).getHost().getId();
				toStore.setHostAccess(accountDal.loadHostAccessInformation(hostId, account.getId()));
				mailDal.storeEmail(toStore);
				emails.add(converted);
			}
		} catch (MessagingException e) {
			String message = "Error while reading mails from the server.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_BEAN_PROVIDER_CONNECTION_ERROR);
		}

		return emails;
	}

	private void destructMessageContent(final Object message, final Email converted)
			throws IOException, MessagingException {
		if (message instanceof MimeMultipart) {
			MimeMultipart part = (MimeMultipart) message;
			for (int i = 0; i < part.getCount(); i++) {
				BodyPart bodyPart = part.getBodyPart(i);
				destructMessageContent(bodyPart.getContent(), converted);
			}
		} else if (message instanceof BASE64DecoderStream) {
			BASE64DecoderStream stream = (BASE64DecoderStream) message;
			List<Byte> appandixPayload = new ArrayList<>();
			do {
				appandixPayload.add((byte) stream.read());
			} while (stream.read() != -1);
			Appandix appandix = new Appandix();
			byte[] byteArray = new byte[appandixPayload.size()];
			for (int i = 0; i < appandixPayload.size(); i++) {
				byteArray[i] = appandixPayload.get(i);
			}
			appandix.setPayload(byteArray);
			converted.getAppandicies().add(appandix);
		}

		else {
			converted.setMessage(message.toString());
		}
	}

	@Override
	public Email loadEmail(final Long id) throws PRPException {
		if (id == null) {
			String message = "Please provide a id to search for an email.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_BEAN_EMAIL_ID_MISSING);
		}

		Email mail = emailConverter.convertEmail(mailDal.loadEmail(id));

		if (mail == null) {
			String message = "No email for given id was found.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_BEAN_EMAIL_NOT_FOUND);
		}

		return mail;
	}
}
