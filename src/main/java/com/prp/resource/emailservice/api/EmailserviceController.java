/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.enums.email.EmailRequestMethod;
import com.prp.resource.common.model.email.Email;
import com.prp.resource.common.model.email.account.Account;
import com.prp.resource.emailservice.bean.AccountBean;
import com.prp.resource.emailservice.bean.EmailBean;

/**
 * Controller receiving request to handle emails and email accounts.
 * <p>
 * At the moment (monolothic architecture), this is just another programming layer. The sense is to be faster at excluding the email service
 * to an own server.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see EmailBean
 * @see AccountBean
 *
 */
@RestController
@RequestMapping("/email")
public class EmailserviceController {

	private EmailBean emailBean;
	private AccountBean accountBean;

	@Autowired
	public EmailserviceController(final EmailBean emailBean, final AccountBean accountBean) {
		this.emailBean = emailBean;
		this.accountBean = accountBean;
	}

	public void sendConfirmationMail(final Email email) throws PRPException {
		emailBean.sendMail(email, false);
	}

	@PostMapping("/send")
	@ResponseBody
	public void sendMail(final Email email) throws PRPException {
		emailBean.sendMail(email, true);
	}

	@PostMapping("/account")
	@ResponseBody
	public Account addAccount(final Account account) throws PRPException {
		return accountBean.registerAccount(account);
	}

	@GetMapping("/account")
	@ResponseBody
	public List<Account> loadAllAccounts() throws PRPException {
		return accountBean.findAccountsForCurrentUser();
	}

	@GetMapping("/account/{id}")
	@ResponseBody
	public Account loadAccount(@PathVariable("id") final Long id) throws PRPException {
		return accountBean.loadAccount(id);
	}

	public List<Email> fetchMails(final Long accountId, final EmailRequestMethod method, final int start,
			final int count) throws PRPException {
		// TODO refactor for API design
		return emailBean.fetchEmails(accountId, method, count, start);
	}

	@PostMapping("/account/update")
	public Account updateAccount(final Account account) throws PRPException {
		return accountBean.updateAccount(account);
	}

	@PostMapping("/{id}")
	public Email loadEmail(@PathVariable("id") final Long id) throws PRPException {
		return emailBean.loadEmail(id);
	}

}
