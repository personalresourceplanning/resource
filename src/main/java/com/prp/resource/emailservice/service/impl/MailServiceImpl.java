/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.prp.resource.common.enums.exception.PRPErrorCode;
import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.enums.email.EmailRequestMethod;
import com.prp.resource.common.model.email.Email;
import com.prp.resource.common.model.email.account.Account;
import com.prp.resource.common.model.email.account.connection.Connection;
import com.prp.resource.emailservice.service.MailService;
import com.sun.mail.imap.IMAPStore;
import com.sun.mail.pop3.POP3Store;

/**
 *
 * @author Eric Fischer
 * @see MailService
 * @since 0.1
 *
 */
@Service
public class MailServiceImpl implements MailService {

	private static final Logger log = LoggerFactory.getLogger(MailServiceImpl.class);

	private static final String MIME_HTML_TYPE = "text/html; charset=utf-8";

	@Override
	public void sendMail(final Email email) throws PRPException {
		Connection smtp = email.getSendingAccount().getConnectionMethods().get(EmailRequestMethod.SMTP);
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", smtp.getHost());
		props.put("mail.smtp.port", smtp.getPort());

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {

			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(smtp.getUsername(), smtp.getPassword());
			}
		});

		try {

			Message mimeMessage = new MimeMessage(session);
			mimeMessage.setFrom(new InternetAddress(email.getFrom().getEmail(), email.getFrom().getPublicName()));
			mimeMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email.getTo().get(0).getEmail()));
			mimeMessage.setSubject(email.getSubject());
			mimeMessage.setContent(email.getMessage(), MIME_HTML_TYPE);

			Transport.send(mimeMessage);

		} catch (MessagingException e) {
			String message = "Could not send mail.";
			log.error(message, e);
			throw new PRPException(message, e, PRPErrorCode.BACKEND_EMAILSERVICE_SERVICE_MAIL_NOT_SENDABLE);
		} catch (UnsupportedEncodingException e) {
			String message = "Could not set sender email address and name.";
			log.error(message, e);
			throw new PRPException(message, e,
					PRPErrorCode.BACKEND_EMAILSERVICE_SERVICE_SENDER_ADDRESS_AND_NAME_NOT_SETABLE);
		}
	}

	@Override
	public List<Message> fetchMailsPOP3(final Account account) throws PRPException {

		if (account == null || account.getConnectionMethods() == null
				|| !account.getConnectionMethods().containsKey(EmailRequestMethod.POP3)
				|| account.getConnectionMethods().get(EmailRequestMethod.POP3) == null) {
			// throwing an IllegalArgumentException here, because bean should have checked
			// the account
			throw new IllegalArgumentException("Account information not complete.");
		}

		log.debug("Fetching mails with POP3 for account " + account.getId() + "...");

		Connection connection = account.getConnectionMethods().get(EmailRequestMethod.POP3);

		Properties props = new Properties();
		props.setProperty("mail.pop3.host", connection.getHost());
		Session session = Session.getDefaultInstance(props);
		POP3Store store = null;
		log.debug("Connecting to host...");

		try {
			store = (POP3Store) session.getStore("pop3");
			store.connect(connection.getUsername(), connection.getPassword());
			List<Message> messageList = new ArrayList<>();
			readEmailFolders(store.getFolder("INBOX"), messageList);
			log.info("Emails fetched.");
			return messageList;
		} catch (NoSuchProviderException nspe) {
			String message = "The provider couldn't be reached. Is your account configured properly?";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_SERVICE_PROVIDER_CONNECTION_ERROR);
		} catch (MessagingException me) {
			String message = "Communication error with email host. Caused by " + me.getClass().toString() + ": "
					+ me.getMessage();
			log.error(message);
			throw new PRPException(message, me, PRPErrorCode.BACKEND_EMAILSERVICE_SERVICE_PROVIDER_COMMUNICATION_ERROR);
		}
	}

	@Override
	public List<Message> fetchMailsIMAP(final Account account) throws PRPException {

		if (account == null || account.getConnectionMethods() == null
				|| !account.getConnectionMethods().containsKey(EmailRequestMethod.IMAP)
				|| account.getConnectionMethods().get(EmailRequestMethod.IMAP) == null) {
			// throwing an IllegalArgumentException here, because bean should have checked
			// the account
			throw new IllegalArgumentException("Account information not complete.");
		}

		log.debug("Fetching mails with IMAP for account " + account.getId() + "...");

		Connection connection = account.getConnectionMethods().get(EmailRequestMethod.IMAP);

		Properties props = new Properties();
		props.setProperty("mail.store.protocol", "imaps");
		Session session = Session.getDefaultInstance(props);
		IMAPStore store = null;
		log.debug("Connecting to host...");

		try {
			store = (IMAPStore) session.getStore("imaps");
			store.connect(connection.getHost(), connection.getUsername(), connection.getPassword());
			List<Message> messageList = new ArrayList<>();
			readEmailFolders(store.getFolder("INBOX"), messageList);
			log.info("Emails fetched.");
			return messageList;
		} catch (NoSuchProviderException nspe) {
			String message = "The provider couldn't be reached. Is your account configured properly?";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_SERVICE_PROVIDER_CONNECTION_ERROR);
		} catch (MessagingException me) {
			String message = "Communication error with email host. Caused by " + me.getClass().toString() + ": "
					+ me.getMessage();
			log.error(message);
			throw new PRPException(message, me, PRPErrorCode.BACKEND_EMAILSERVICE_SERVICE_PROVIDER_COMMUNICATION_ERROR);
		}
	}

	private void readEmailFolders(final Folder folder, final List<Message> messageList) throws MessagingException {
		folder.open(Folder.READ_ONLY);
		Message[] messages = folder.getMessages();
		messageList.addAll(Arrays.asList(messages));
		for (Folder subFolder : folder.list()) {
			readEmailFolders(subFolder, messageList);
		}
	}

}
