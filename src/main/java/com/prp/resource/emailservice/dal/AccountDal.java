/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.dal;

import java.util.List;

import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.enums.email.EmailRequestMethod;
import com.prp.resource.emailservice.dal.model.AccountDB;
import com.prp.resource.emailservice.dal.model.EmailAddressDB;
import com.prp.resource.emailservice.dal.model.HostAccessDB;
import com.prp.resource.emailservice.dal.model.HostDB;

/**
 * Database layer for managing email accounts.
 *
 * @author Eric Fischer
 * @since 0.1
 *
 */
public interface AccountDal {

	/**
	 * Used to store an {@link AccountDB} to the database. Remeber to fill all required fields.
	 *
	 * @param account - The {@link AccountDB} to store.
	 * @return {@link AccountDB} - The stored account representation.
	 * @throws PRPException
	 * @since 0.1
	 */
	AccountDB storeAccount(AccountDB account) throws PRPException;

	/**
	 * Updates account information.
	 *
	 * @param account
	 * @return {@link AccountDB} - The updated account.
	 * @throws PRPException
	 * @since 0.1
	 */
	AccountDB updateAccount(AccountDB account) throws PRPException;

	/**
	 * Deleted account information.
	 *
	 * @param id {@link Long} - The DB ID of the account. Needed to ensure that the right representation is deleted.
	 * @throws PRPException
	 * @since 0.1
	 */
	void deleteAccount(Long id) throws PRPException;

	/**
	 * Loads account information from the database for the given ID. This is returning only one result, because accounts have to be unique.
	 *
	 * @param id - The {@link Long} identifying the account to load.
	 * @return {@link AccountDB} - The found account or {@code null} if nothing found.
	 * @throws PRPException
	 * @since 0.1
	 */
	AccountDB loadAccount(Long id) throws PRPException;

	/**
	 * Lists all account the given user has registered.
	 *
	 * @param userId - The ID of the user the accounts are searched for.
	 * @return {@link List}<{@linkAccountDB}> - List containing all database representations found for the specified user.
	 * @throws PRPException
	 * @since 0.1
	 */
	List<AccountDB> loadAccountsForUser(Long userId) throws PRPException;

	/**
	 * Loads the {@link HostDB} information for a specific host and {@link EmailRequestMethod}.
	 * <p>
	 * The {@link HostDB} is stored seperatly from the user specific {@link HostAccessDB}, because this way it is possible to store the general
	 * information like URL and port centralized. This allows us to provide such information for all users and they don't have to enter all
	 * information.
	 * <p>
	 * As this information is unique for host URL and method, only a single object is returned.
	 *
	 * @param host   - The host URL to search for.
	 * @param method - The {@link EmailRequestMethod} of the host.
	 * @return {@link HostDB} - The representation of the host information or <code>null</code> if nothing found.
	 * @throws PRPException
	 * @since 0.1
	 */
	HostDB loadHostInformation(String host, EmailRequestMethod method) throws PRPException;

	/**
	 * Searches for the user specific credentials for a certain host. The link to the user is given by the specified {@link AccountDB}.
	 *
	 * @param hostId    - The {@link HostDB} the host access information is linked to.
	 * @param accountId - The {@link AccountDB} linked with the user the information is requested by.
	 * @return The {@link HostAccessDB} found for the given information or <code>null</code> if nothing found.
	 * @throws PRPException
	 * @since 0.1
	 */
	HostAccessDB loadHostAccessInformation(Long hostId, Long accountId) throws PRPException;

	/**
	 * Loads the database representation for the provided email address.
	 *
	 * @param senderName - {@link String}
	 * @param email      - {@link String}
	 * @return {@link EmailAddressDB}
	 * @throws PRPException
	 */
	EmailAddressDB loadEmailAddress(String senderName, String email) throws PRPException;

}
