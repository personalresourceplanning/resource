/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.dal.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.prp.resource.common.enums.exception.PRPErrorCode;
import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.enums.email.EmailRequestMethod;
import com.prp.resource.emailservice.dal.AccountDal;
import com.prp.resource.emailservice.dal.model.AccountDB;
import com.prp.resource.emailservice.dal.model.EmailAddressDB;
import com.prp.resource.emailservice.dal.model.HostAccessDB;
import com.prp.resource.emailservice.dal.model.HostDB;
import com.prp.resource.emailservice.dal.repository.AccountsRepository;
import com.prp.resource.emailservice.dal.repository.EmailAddressRepository;
import com.prp.resource.emailservice.dal.repository.HostAccessRepository;
import com.prp.resource.emailservice.dal.repository.HostRepository;

/**
 *
 * @author Eric Fischer
 * @since 0.1
 * @see AccountDal
 *
 */
@Component
public class AccountDalImpl implements AccountDal {

	private static final Logger log = LoggerFactory.getLogger(AccountDalImpl.class);

	private HostRepository hostRepository;
	private HostAccessRepository hostAccessRepository;
	private AccountsRepository accountsRepository;
	private EmailAddressRepository emailAddressRepository;

	@Autowired
	public AccountDalImpl(final HostRepository hostRepository, final HostAccessRepository hostAccessRepository,
			final AccountsRepository accountsRepository, final EmailAddressRepository emailRepository) {
		this.hostRepository = hostRepository;
		this.hostAccessRepository = hostAccessRepository;
		this.accountsRepository = accountsRepository;
		this.emailAddressRepository = emailRepository;
	}

	@Transactional
	@Override
	public AccountDB storeAccount(final AccountDB account) throws PRPException {
		if (account == null || account.getHosts() == null || account.getHosts().isEmpty() || account.getName() == null
				|| account.getName().isEmpty()) {
			String message = "Account information missing. Not possible to store it to the database.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_DAL_ACCOUNT_INFORMATION_NOT_GIVEN);
		}

		for (HostAccessDB hostAccess : account.getHosts()) {

			if (hostInformationMissing(hostAccess)) {
				String message = "Not all parameters given to store host.";
				log.error(message);
				throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_DAL_HOST_DETAILS_MISSING);
			}

			for (EmailAddressDB address : hostAccess.getEmailAddresses()) {
				if (address == null || address.getAddress() == null || address.getAddress().isEmpty()) {
					throw new PRPException("No email address given to link with the host access informaton.",
							PRPErrorCode.BACKEND_EMAILSERVICE_DAL_NO_EMAIL_TO_STORE);
				}
			}
		}

		if (!accountsRepository.findByUserIDAndName(account.getUserID(), account.getName()).isEmpty()) {
			String message = "Account with this name is already stored.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_DAL_ACCOUNT_NAME_ALREADY_TAKEN);
		}

		accountsRepository.save(account);

		for (HostAccessDB hostAccess : account.getHosts()) {

			HostDB host = loadHostInformation(hostAccess.getHost().getHostUrl(), hostAccess.getHost().getMethod());

			if (host == null) {
				host = hostAccess.getHost();
				hostRepository.save(host);
			}

			List<EmailAddressDB> toRemove = new ArrayList<>();
			List<EmailAddressDB> toAdd = new ArrayList<>();

			for (EmailAddressDB address : hostAccess.getEmailAddresses()) {
				EmailAddressDB foundEmailAddress = emailAddressRepository
						.findBySenderNameAndAddress(address.getSenderName(), address.getAddress());
				if (foundEmailAddress == null) {
					emailAddressRepository.save(address);
				} else {
					foundEmailAddress.getHost().add(hostAccess);
					emailAddressRepository.save(foundEmailAddress);
					toRemove.add(address);
					toAdd.add(foundEmailAddress);
				}
			}

			hostAccess.getEmailAddresses().removeAll(toRemove);
			hostAccess.getEmailAddresses().addAll(toAdd);

			hostAccess.setHost(host);
			hostAccess.setAccount(account);
			hostAccessRepository.save(hostAccess);
		}

		return account;

	}

	private boolean hostInformationMissing(final HostAccessDB hostAccess) {
		// @formatter:off
		return hostAccess == null || hostAccess.getEmailAddresses() == null || hostAccess.getEmailAddresses().isEmpty()
				|| hostAccess.getUsername() == null || hostAccess.getUsername().isEmpty()
				|| hostAccess.getPassword() == null || hostAccess.getPassword().isEmpty()
				|| hostAccess.getHost() == null || hostAccess.getHost().getHostUrl() == null
				|| hostAccess.getHost().getHostUrl().isEmpty() || hostAccess.getHost().getPort() == null
				|| hostAccess.getHost().getPort().equals(Integer.valueOf(0))
				|| hostAccess.getHost().getMethod() == null;
		// @formatter:on
	}

	@Override
	public AccountDB updateAccount(final AccountDB account) throws PRPException {
		if (account == null || account.getId() == null || account.getId().equals(Long.valueOf(0))) {
			String message = "Not all parameters given to search for a specific account.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_DAL_PARAMETERS_MISSING);
		}

		Optional<AccountDB> found = accountsRepository.findById(account.getId());

		if (!found.isPresent()) {
			String message = "The account to update wasn't found.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_DAL_ACCOUNT_NOT_FOUND);
		}

		AccountDB toUpdate = found.get();

		toUpdate.setHosts(account.getHosts());
		toUpdate.setName(account.getName());
		toUpdate.setPrimaryFetchingMethod(account.getPrimaryFetchingMethod());
		toUpdate.setUserID(account.getUserID());

		return accountsRepository.save(toUpdate);
	}

	@Override
	public void deleteAccount(final Long id) throws PRPException {
		// TODO Auto-generated method stub

	}

	@Override
	public HostDB loadHostInformation(final String host, final EmailRequestMethod method) throws PRPException {
		if (host == null || host.isEmpty() || method == null) {
			String message = "Not all parameters given to search for a specific host.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_DAL_HOST_DETAILS_MISSING);
		}

		List<HostDB> foundHost = hostRepository.findByHostUrlAndMethod(host, method);

		if (foundHost == null || foundHost.isEmpty()) {
			log.warn("No host found for the given attributes.");
			return null;
		}

		if (foundHost.size() > 1) {
			String message = "Data integrity error: Multiple host definition for single input found.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_EMAILSERVICE_DAL_DATA_INTEGRITY_MULTIPLE_HOST_DEFINITIONS);
		}

		return foundHost.get(0);
	}

	@Override
	public HostAccessDB loadHostAccessInformation(final Long hostId, final Long accountId) throws PRPException {
		if (hostId == null || Long.valueOf(0).equals(hostId) || accountId == null
				|| Long.valueOf(0).equals(accountId)) {
			String message = "Host and account ID missing to search for an access information.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_DAL_INFORMATION_ABOUT_HOSTACCESS_MISSING);
		}

		Optional<HostDB> host = hostRepository.findById(hostId);
		Optional<AccountDB> account = accountsRepository.findById(accountId);
		if (!host.isPresent() || !account.isPresent()) {
			String message = "The host or account information is not stored in the database.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_DAL_ACCOUNT_AND_HOST_NOT_FOUND);
		}

		List<HostAccessDB> hostsAccess = hostAccessRepository.findByHostAndAccount(host.get(), account.get());

		if (hostsAccess == null || hostsAccess.isEmpty()) {
			log.debug("No host access information found.");
			return null;
		}

		if (hostsAccess.size() > 1) {
			String message = "Data integrity error: Multiple access definition for single host found.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_EMAILSERVICE_DAL_DATA_INTEGRITY_MULTIPLE_ACCESS_DEFINITIONS);
		}

		return hostsAccess.get(0);
	}

	@Override
	public AccountDB loadAccount(final Long id) throws PRPException {

		if (id == null || Long.valueOf(0).equals(id)) {
			String message = "Not all parameters given to search for an account.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_DAL_ACCOUNT_INFORMATION_NOT_GIVEN);
		}

		Optional<AccountDB> found = accountsRepository.findById(id);

		if (!found.isPresent()) {
			log.warn("No account for input found.");
			return null;
		}

		return found.get();
	}

	@Override
	public List<AccountDB> loadAccountsForUser(final Long userId) throws PRPException {
		if (userId == null || Long.valueOf(0).equals(userId)) {
			String message = "No user ID given to search for the accounts.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_DAL_ACCOUNT_INFORMATION_NOT_GIVEN);
		}

		List<AccountDB> accounts = accountsRepository.findByUserID(userId);

		if (accounts == null || accounts.isEmpty()) {
			log.warn("No accounts found for user ID.");
			return null;
		}

		return accounts;
	}

	@Override
	public EmailAddressDB loadEmailAddress(final String senderName, final String email) throws PRPException {
		if (senderName == null || email == null) {
			String message = "Sender name or email missing to search for the email address DB representation.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_DAL_PARAMETERS_MISSING);
		}

		return emailAddressRepository.findBySenderNameAndAddress(senderName, email);
	}

}
