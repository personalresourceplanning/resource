/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.dal.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "lia_backend_emailservice_emailaddress", uniqueConstraints = @UniqueConstraint(columnNames = {
		"name", "address" }))
@Data
public class EmailAddressDB {

	@Id @GeneratedValue @Setter(AccessLevel.NONE) private Long id;
	// setup in HostAccessDB
	@ManyToMany(mappedBy = "host") @ToString.Exclude @EqualsAndHashCode.Exclude private Set<HostAccessDB> host = new HashSet<>();
	@Column(name = "name", nullable = false, unique = false) private String senderName;
	@Column(name = "address", nullable = false, unique = false) private String address;

}
