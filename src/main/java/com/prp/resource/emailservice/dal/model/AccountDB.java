/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.dal.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.prp.resource.common.model.email.account.Account;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.ToString;

/**
 * Database representation for {@link Account}.
 * <p>
 *
 * @author Eric Fischer
 * @since 0.1
 *
 */
@Entity
@Table(name = "lia_backend_emailservice_account")
@Data
public class AccountDB {

	public static final String POP3 = "POP3";
	public static final String IMAP = "IMAP";

	@Id
	@GeneratedValue
	@Setter(AccessLevel.NONE)
	@Column(name = "id")
	private Long id;
	@Column(name = "userid", nullable = false, unique = false)
	private Long userID;
	@Column(name = "name", nullable = true, unique = false)
	private String name;
	@OneToMany(mappedBy = "account", fetch = FetchType.EAGER)
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	private List<HostAccessDB> hosts = new ArrayList<>();
	@Column(name = "primaryfetchingmethod", nullable = false, unique = false)
	private String primaryFetchingMethod = IMAP;

}
