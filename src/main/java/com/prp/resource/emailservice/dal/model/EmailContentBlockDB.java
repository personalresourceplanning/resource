/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.dal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "lia_backend_emailservice_emailconetnt")
@Data
public class EmailContentBlockDB {

	@GeneratedValue @Id @Setter(AccessLevel.NONE) private Long id;
	@ManyToOne @JoinColumn(name = "mail", nullable = false, unique = false) @ToString.Exclude private EmailDB mail;
	@Column(name = "ordernumber", nullable = false, unique = false) private Long orderNumber;
	@Column(name = "content", nullable = false, unique = false) private String content;

}
