/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.dal.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.prp.resource.common.enums.email.EmailRequestMethod;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Entity
@Table(name = "lia_backend_emailservice_connection")
@Data
public class HostDB {

	@Id
	@GeneratedValue
	@Setter(AccessLevel.NONE)
	private Long id;
	@Column(name = "host", nullable = false, unique = true)
	private String hostUrl;
	@Column(name = "port", nullable = false, unique = false)
	private Integer port;
	@Column(name = "requestmethod", nullable = false, unique = false)
	// TODO change to String representation
	private EmailRequestMethod method;
	@OneToMany(mappedBy = "host")
	private List<HostAccessDB> accesses = new ArrayList<>();

}
