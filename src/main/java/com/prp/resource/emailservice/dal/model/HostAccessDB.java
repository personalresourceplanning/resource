/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.dal.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "lia_backend_emailservice_hostaccess")
@Data
public class HostAccessDB {

	@Id @GeneratedValue @Setter(AccessLevel.NONE) private Long id;
	@ManyToOne(fetch = FetchType.EAGER) @JoinColumn(name = "hostid", nullable = false, unique = false) private HostDB host;
	@ManyToOne @JoinColumn(name = "accountid", nullable = false) private AccountDB account;
	@Column(name = "username", nullable = false, unique = false) private String username;
	@Column(name = "password", nullable = false, unique = false) private String password;
	// @formatter:off
	@ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	@EqualsAndHashCode.Exclude
    @JoinTable(
        name = "lia_backend_emailservice_hostemailconnection",
        joinColumns = { @JoinColumn(name = "email", nullable=false) },
        inverseJoinColumns = { @JoinColumn(name = "host", nullable=false) }
    )
	private List<EmailAddressDB> emailAddresses = new ArrayList<>();
	// @formatter:on
	@OneToMany(mappedBy = "hostAccess") @ToString.Exclude @EqualsAndHashCode.Exclude private Set<EmailDB> emails = new HashSet<>();
	@Column(name = "leavecopyonserver", nullable = false, unique = false) private boolean leaveCopyOnServer = true;

}
