/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.dal;

import java.util.Date;
import java.util.List;

import com.prp.resource.common.exception.PRPException;
import com.prp.resource.emailservice.dal.model.EmailAddressDB;
import com.prp.resource.emailservice.dal.model.EmailDB;

/**
 * Data access layer for managing the email information. Contains methods for storing and managing emails and drafts.
 *
 * @author Eric Fischer
 * @since 0.1
 *
 */
public interface MailDal {

	/**
	 * Stores an email to the database. Therefore, many integrity checks are done.
	 *
	 * @param email - The {@link EmailDB} to store.
	 * @throws PRPException
	 * @since 0.1
	 */
	void storeEmail(EmailDB email) throws PRPException;

	/**
	 * Marks an email as deleted. For analytics, no email will be really deleted. The only exception for this is a GDPR request by the user, but
	 * therefore, this method shouldn't be used.
	 *
	 * @param id - The ID of the email, which should be marked as deleted.
	 * @throws PRPException
	 * @since 0.1
	 */
	void deleteEmail(Long id) throws PRPException;

	/**
	 * Lists all emails for an user.
	 *
	 * @param userId - The ID of the user the emails should be listed for.
	 * @return {@link List}<{@linkEmailDB}> - The emails found for the user.
	 * @throws PRPException
	 * @since 0.1
	 */
	List<EmailDB> findEmailsForUser(Long userId) throws PRPException;

	/**
	 * Stores an email as a draft. This means, it will not be treated as sent and not so many checks will be performed.
	 *
	 * @param email - The {@link EmailDB} to store as draft.
	 * @throws PRPException
	 * @since 0.1
	 */
	void storeDraft(EmailDB email) throws PRPException;

	/**
	 * Edits a draft. Has a similar behavior as {@link #storeDraft(EmailDB)}.
	 *
	 * @param email - The {@link EmailDB} to update.
	 * @throws PRPException
	 * @since 0.1
	 */
	void editDraft(EmailDB email) throws PRPException;

	/**
	 * Deletes a draft. In difference to {@link #deleteEmail(Long)}, this is a real deletion because no analytics will be performed on deleted
	 * drafts.
	 *
	 * @param id - The ID of the {@link EmailDB} to delete.
	 * @throws PRPException
	 * @since 0.1
	 */
	void deleteDraft(Long id) throws PRPException;

	/**
	 * Checks, if an email with the given parameters exists and returns it if found.
	 *
	 * @param subject  - The {@link String} to look for as subject.
	 * @param sender   - The sender {@link EmailAddressDB} to look for.
	 * @param sentDate - The {@link Date} the email was sent.
	 * @return {@link EmailDB} - The found mail or {@code null} if nothing found.
	 */
	EmailDB checkIfExistant(String subject, EmailAddressDB sender, Date sentDate) throws PRPException;

	EmailDB loadEmail(Long id) throws PRPException;

}
