/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.timeplanningservice.api;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.model.task.Task;
import com.prp.resource.timeplanningservice.bean.TimePlanningBean;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/planning")
public class TimeplanningController {

	private TimePlanningBean timeplanningBean;

	@Autowired
	public TimeplanningController(final TimePlanningBean timeplanningBean) {
		this.timeplanningBean = timeplanningBean;
	}

	public List<Task> reversePlanning(final Date sleepingTimeStart, final Date sleepingTimeEnd) throws PRPException {
		return timeplanningBean.reverseTaskPlanning(sleepingTimeStart, sleepingTimeEnd);
	}

}
