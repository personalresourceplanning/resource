/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.timeplanningservice.bean.util;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.prp.resource.common.model.calendar.Event;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
public class ReversePlanningTimeSlots {

	private static final Logger log = LoggerFactory.getLogger(ReversePlanningTimeSlots.class);

	/**
	 * Map containing the slots free for planning. The first date represents the end of the slot, the second the start. this structure is
	 * important for the reverse planning.
	 */
	@Setter(AccessLevel.NONE)
	private Map<Date, Date> freeSlots = new HashMap<>();

	public ReversePlanningTimeSlots(final List<Event> events, final Calendar sleepingStart, final Calendar sleepingEnd,
			final Date start, final Date end) {
		if (log.isDebugEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("Finding slots between ");
			sb.append(start);
			sb.append(" and ");
			sb.append(end);
			log.debug(sb.toString());

			sb = new StringBuilder();
			sb.append("Sleeping time start: ");
			sb.append(sleepingStart.getTime());
			log.debug(sb.toString());
			sb = new StringBuilder();
			sb.append("Sleeping time end: ");
			sb.append(sleepingEnd.getTime());
			log.debug(sb.toString());
		}

		List<Event> filteredEvents = events.stream().filter(event -> event.getStartDate().before(end))
				.filter(event -> event.getEndDate().after(start)).collect(Collectors.toList());

		List<TimeBlocker> blockers = new ArrayList<>();

		// add events as time blockers
		for (Event event : filteredEvents) {
			blockers.add(new TimeBlocker(event.getStartDate(), event.getEndDate(), BlockerType.EVENT));
		}

		int numberOfEvents = blockers.size();
		if (log.isTraceEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("Added ");
			sb.append(numberOfEvents);
			sb.append(" events as time blockers.");
			log.trace(sb.toString());
		}

		Date searchStart = null;

		Date sleepingStartOnEndDay = buildSleepingDate(sleepingStart, end);
		Date sleepingEndOnEndDay = buildSleepingDate(sleepingEnd, end);
		Calendar dateMarker = Calendar.getInstance();
		if (end.after(sleepingStartOnEndDay)) {
			dateMarker.setTime(sleepingStartOnEndDay);
		} else if (end.before(sleepingEndOnEndDay)) {
			dateMarker.setTime(end);
			dateMarker.set(Calendar.DAY_OF_MONTH, dateMarker.get(Calendar.DAY_OF_MONTH) - 1);
			dateMarker.set(Calendar.HOUR_OF_DAY, sleepingStart.get(Calendar.HOUR_OF_DAY));
			dateMarker.set(Calendar.MINUTE, sleepingStart.get(Calendar.MINUTE));
			dateMarker.set(Calendar.SECOND, sleepingStart.get(Calendar.SECOND));
		} else {
			dateMarker.setTime(end);
		}

		searchStart = dateMarker.getTime();

		if (log.isTraceEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("Found search start date: ");
			sb.append(searchStart);
			log.trace(sb.toString());
		}

		while (dateMarker.getTime().after(start)) {

			Date nextSleepingTimeEnd = buildSleepingDate(sleepingEnd, dateMarker);

			dateMarker.set(Calendar.DAY_OF_MONTH, dateMarker.get(Calendar.DAY_OF_MONTH) - 1);
			Date nextSleepingTimeStart = buildSleepingDate(sleepingStart, dateMarker);

			TimeBlocker timeBlocker = new TimeBlocker(nextSleepingTimeStart, nextSleepingTimeEnd, BlockerType.SLEEPING);
			blockers.add(timeBlocker);

			if (log.isTraceEnabled()) {
				StringBuilder sb = new StringBuilder();
				sb.append("Added blocker ");
				sb.append(timeBlocker);
				log.trace(sb.toString());
			}

			dateMarker.setTime(nextSleepingTimeStart);
		}

		if (log.isTraceEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("Added ");
			sb.append(blockers.size() - numberOfEvents);
			sb.append(" sleeping intervals as time blockers");
			log.trace(sb.toString());
		}

		// if no blockers found, the time interval is added as the only free slot
		if (blockers.isEmpty()) {
			addSlot(start, end);
			log.debug("No blockers found. Added the whole time interval as free slot.");
			return;
		}

		blockers.sort((a, b) -> b.getEnd().compareTo(a.getEnd()));
		dateMarker.setTime(searchStart);

		Iterator<TimeBlocker> iterator = blockers.iterator();

		TimeBlocker firstBlocker = iterator.next();
		addSlot(firstBlocker.getEnd(), dateMarker.getTime());

		if (log.isTraceEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("Marker at ");
			sb.append(firstBlocker.getStart());
			log.trace(sb.toString());
		}
		dateMarker.setTime(firstBlocker.getStart());

		TimeBlocker nextBlocker = null;
		while (iterator.hasNext()) {
			nextBlocker = iterator.next();

			if (log.isTraceEnabled()) {
				StringBuilder sb = new StringBuilder();
				sb.append("Searching for time slot between ");
				sb.append(firstBlocker);
				sb.append(" and ");
				sb.append(nextBlocker);
				log.trace(sb.toString());
			}

			boolean nextEndWithin = nextBlocker.getEnd().after(firstBlocker.getStart())
					&& nextBlocker.getEnd().before(firstBlocker.getEnd());
			boolean nextStartWithin = nextBlocker.getStart().after(firstBlocker.getStart())
					&& nextBlocker.getStart().before(firstBlocker.getEnd());

			boolean nextIsWithin = nextEndWithin && nextStartWithin;

			if (nextIsWithin) {
				nextBlocker = null;
				if (log.isTraceEnabled()) {
					StringBuilder sb = new StringBuilder();
					sb.append("Marker at ");
					sb.append(firstBlocker.getStart());
					log.trace(sb.toString());
				}
				dateMarker.setTime(firstBlocker.getStart());
				if (log.isTraceEnabled()) {
					log.trace("Next blocker is within the first one. Ignoring.");
				}
				continue;
			}

			if (nextEndWithin) {
				firstBlocker = nextBlocker;
				if (log.isTraceEnabled()) {
					StringBuilder sb = new StringBuilder();
					sb.append("Marker at ");
					sb.append(firstBlocker.getStart());
					log.trace(sb.toString());
				}
				dateMarker.setTime(firstBlocker.getStart());
				if (log.isTraceEnabled()) {
					log.trace("Next blocker ends within first one. Ignoring.");
				}
				continue;
			}

			Date slotEnd = dateMarker.getTime();
			Date slotStart = nextBlocker.getEnd();
			if (!slotEnd.equals(slotStart) && !slotStart.before(start)) {
				addSlot(slotStart, slotEnd);
			} else if (slotStart.before(start)) {
				addSlot(start, slotEnd);
			}
			firstBlocker = nextBlocker;
			if (log.isTraceEnabled()) {
				StringBuilder sb = new StringBuilder();
				sb.append("Marker at ");
				sb.append(nextBlocker.getStart());
				log.trace(sb.toString());
			}
			dateMarker.setTime(nextBlocker.getStart());

		}

	}

	private void addSlot(final Date start, final Date end) {
		StringBuilder sb = new StringBuilder();
		sb.append("Adding slot between ");
		sb.append(start);
		sb.append(" and ");
		sb.append(end);
		log.trace(sb.toString());

		freeSlots.put(end, start);
	}

	private Date buildSleepingDate(final Calendar sleeping, final Calendar day) {
		Calendar sleepingEndDate = new GregorianCalendar();
		sleepingEndDate.set(Calendar.DATE, day.get(Calendar.DATE));
		sleepingEndDate.set(Calendar.HOUR_OF_DAY, sleeping.get(Calendar.HOUR_OF_DAY));
		sleepingEndDate.set(Calendar.MINUTE, sleeping.get(Calendar.MINUTE));
		sleepingEndDate.set(Calendar.SECOND, sleeping.get(Calendar.SECOND));
		sleepingEndDate.set(Calendar.MILLISECOND, 0);
		return sleepingEndDate.getTime();
	}

	private Date buildSleepingDate(final Calendar sleeping, final Date day) {
		Calendar dayCalendar = new GregorianCalendar();
		dayCalendar.setTime(day);
		return buildSleepingDate(sleeping, dayCalendar);
	}

	/**
	 * Searches all available slot for the given duration.
	 *
	 * @param duration - The minimal required {@link Duration} of the slot.
	 * @return {@link Map}&lt;{@link Date}, {@link Date}&gt; - The filtered {@link #getFreeSlots()} map.
	 */
	public Map<Date, Date> findSlots(final Duration duration) {
		Map<Date, Date> retVal = new HashMap<>();

		for (Date key : freeSlots.keySet()) {
			Date value = freeSlots.get(key);
			long slotMillis = key.getTime() - value.getTime();
			long durationMillis = duration.toMillis();

			if (slotMillis >= durationMillis) {
				retVal.put(key, value);
			}
		}

		return retVal;
	}

	/**
	 * Marks the given Slot as blocked.
	 *
	 * @param start - The start {@link Date} of the slot.
	 * @param end   - The end {@link Date} of the slot.
	 */
	public void blockSlot(final Date start, final Date end) {
		log.debug("Blocking slot between " + start + " and " + end);

		Date foundStartTime = freeSlots.get(end);

		if (foundStartTime != null) {
			freeSlots.remove(end);
			log.debug("Removed the slot.");
			if (!start.equals(foundStartTime)) {
				freeSlots.put(start, foundStartTime);
				log.debug("New open slot between " + foundStartTime + " and " + start);
			}
			return;
		}

		List<Date> sortedPossibleSlotEnds = freeSlots.keySet().stream().filter(d -> d.after(end))
				.collect(Collectors.toList());
		sortedPossibleSlotEnds.sort(new Comparator<Date>() {

			@Override
			public int compare(final Date o1, final Date o2) {
				return o1.compareTo(o2);
			}
		});
		Date slotEnd = sortedPossibleSlotEnds.get(0);
		Date slotStart = freeSlots.get(slotEnd);
		freeSlots.put(slotEnd, end);
		log.debug("New open slot between " + end + " and " + slotEnd);

		if (!slotStart.equals(start)) {
			freeSlots.put(start, slotStart);
			log.debug("New open slot between " + slotStart + " and " + start);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		for (Date end : freeSlots.keySet()) {
			sb.append("\n");
			sb.append(end);
			sb.append("\n");
			sb.append("||||||||");
			sb.append("\n");
			sb.append(freeSlots.get(end));
			sb.append("\n");
		}

		return sb.toString();
	}

	@Data
	private class TimeBlocker {

		private Date start;
		private Date end;
		private BlockerType type;

		public TimeBlocker(final Date start, final Date end, final BlockerType type) {
			this.start = start;
			this.end = end;
			this.type = type;
		}
	}

	private enum BlockerType {
		EVENT,
		SLEEPING;
	}

}
