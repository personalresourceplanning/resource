/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.timeplanningservice.bean.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prp.resource.common.enums.exception.PRPErrorCode;
import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.model.calendar.Event;
import com.prp.resource.common.model.task.Task;
import com.prp.resource.common.model.task.TaskList;
import com.prp.resource.timeplanningservice.bean.TimePlanningBean;
import com.prp.resource.timeplanningservice.bean.util.ReversePlanningTimeSlots;
import com.prp.resource.timeplanningservice.service.CalendarService;
import com.prp.resource.timeplanningservice.service.DateTimeService;
import com.prp.resource.timeplanningservice.service.TaskService;

@Component
public class TimePlanningBeanImpl implements TimePlanningBean {

	private static final Logger log = LoggerFactory.getLogger(TimePlanningBeanImpl.class);

	private TaskService taskService;
	private CalendarService calendarService;
	private DateTimeService dateTimeService;

	@Autowired
	public TimePlanningBeanImpl(final TaskService taskService, final CalendarService calendarService,
			final DateTimeService dateTimeService) {
		this.taskService = taskService;
		this.calendarService = calendarService;
		this.dateTimeService = dateTimeService;
	}

	@Override
	public List<Task> reverseTaskPlanning(final Date sleepingTimeStart, final Date sleepingTimeEnd)
			throws PRPException {

		List<Task> tasks = taskService.loadTasksForCurrentUser().stream().filter(t -> t.getDone().equals(Boolean.FALSE))
				.collect(Collectors.toList());
		List<Event> events = calendarService.loadEventsForCurrentUser();

		StringBuilder sb = new StringBuilder();
		sb.append("Planning ");
		sb.append(tasks.size());
		sb.append(" tasks between ");
		sb.append(events.size());
		sb.append(" events.");
		log.debug(sb.toString());

		Set<Task> predecessors = new HashSet<>();

		for (Task task : tasks) {
			predecessors.addAll(task.getPredecessor());
		}

		SimpleDateFormat time = new SimpleDateFormat("hh:mm");
		sb = new StringBuilder();
		sb.append("Sleeping time between ");
		sb.append(time.format(sleepingTimeStart));
		sb.append(" and ");
		sb.append(time.format(sleepingTimeEnd));
		log.debug(sb.toString());

		List<Task> finalTasks = tasks.stream().filter(t -> !predecessors.contains(t))
				.filter(t -> t.getDueDate() != null).filter(t -> t.getDuration() != null)
				.filter(t -> t.getDone().equals(Boolean.FALSE)).collect(Collectors.toList());
		Set<Task> planned = new HashSet<>();
		// tasks that are due first should be planned first
		finalTasks.sort(new Comparator<Task>() {

			@Override
			public int compare(final Task o1, final Task o2) {
				return o1.getDueDate().compareTo(o2.getDueDate());
			}
		});

		Date currentTime = dateTimeService.getUsersTime();

		Calendar sleepingStartCalendar = Calendar.getInstance();
		sleepingStartCalendar.setTime(sleepingTimeStart);
		Calendar sleepingEndCalendar = Calendar.getInstance();
		sleepingEndCalendar.setTime(sleepingTimeEnd);

		ReversePlanningTimeSlots slots = new ReversePlanningTimeSlots(
				events.stream().filter(event -> event.getEndDate().after(currentTime)).collect(Collectors.toList()),
				sleepingStartCalendar, sleepingEndCalendar, currentTime,
				finalTasks.get(finalTasks.size() - 1).getDueDate());

		planTasks(finalTasks, slots, planned, currentTime);

		for (Task task : finalTasks) {
			planTasks(task.getPredecessor(), slots, planned, currentTime);
		}

		return tasks;
	}

	private void planTasks(final List<Task> tasks, final ReversePlanningTimeSlots slots, final Set<Task> planned,
			final Date currentTime) throws PRPException {
		for (Task task : tasks) {

			// if task is already planned, then continue to the next
			if (planned.contains(task)) {
				log.warn("Task already planned.");
				continue;
			}

			log.debug("Task duration: " + task.getDuration().toMinutes() + "min");
			log.debug("Due date: " + task.getDueDate());

			Map<Date, Date> availableSlots = slots.findSlots(task.getDuration());
			Map<Date, Date> possibleSlots = availableSlots.entrySet().stream()
					.filter(slot -> slot.getKey().before(task.getDueDate()))
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
			possibleSlots
					.putAll(availableSlots.entrySet().stream().filter(slot -> slot.getKey().equals(task.getDueDate()))
							.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));

			log.debug("Available slots: " + availableSlots);
			log.debug("Possibble slots: " + possibleSlots.toString());

			Date bestSlotKey = null;
			long bestDuration = Long.MAX_VALUE;

			for (Date endDate : possibleSlots.keySet()) {
				long thisDuration = endDate.getTime() - possibleSlots.get(endDate).getTime();
				if (thisDuration < bestDuration) {
					bestDuration = thisDuration;
					bestSlotKey = endDate;
				}
			}

			if (bestSlotKey == null || bestDuration == Long.MAX_VALUE) {
				String message = "No more slots found to plan the task.";
				log.error(message);
				throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_BEAN_ALL_SLOTS_FILLED);
			}

			StringBuilder sb = new StringBuilder();
			sb.append("Found best slot between ");
			sb.append(possibleSlots.get(bestSlotKey));
			sb.append(" and ");
			sb.append(bestSlotKey);
			sb.append(" with duration of ");
			sb.append(bestDuration / 1000 / 60);
			sb.append("min");
			log.debug(sb.toString());

			Date startDate = new Date(bestSlotKey.getTime() - task.getDuration().toMillis());
			slots.blockSlot(startDate, bestSlotKey);

			task.setStartDate(startDate);
			taskService.updateTask(task);
			planned.add(task);

			for (Task predecessor : task.getPredecessor()) {
				predecessor.setDueDate(possibleSlots.get(bestSlotKey));
				taskService.updateTask(predecessor);
			}

			if (!task.getSubLists().isEmpty()) {
				planSubLists(task, slots, planned, currentTime);
			}
		}
	}

	private void planSubLists(final Task task, final ReversePlanningTimeSlots slots, final Set<Task> planned,
			final Date currentTime) throws PRPException {
		for (TaskList subList : task.getSubLists()) {
			planTasks(subList.getTasks(), slots, planned, currentTime);
		}
	}

}
